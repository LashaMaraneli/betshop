﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Betasuchus.Shared
{
    public abstract class DomainEvent
    {
        public DomainEvent(long aggregateRootId, string payload)
        {
            AggregateRootId = aggregateRootId;
            Payload = payload;

            SetMainFields();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; protected set; }

        public long AggregateRootId { get; protected set; }

        public DateTime OccuredOn { get; protected set; }

        public string EventType { get; protected set; }

        public string Payload { get; protected set; }

        private void SetMainFields()
        {
            EventType = this.GetType().Name;
            OccuredOn = DateTime.Now;
        }
    }
}
