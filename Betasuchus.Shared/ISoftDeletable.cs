﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Shared
{
    public interface ISoftDeletable
    {
        void Delete();
    }
}
