﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Betasuchus.Shared
{
    public abstract class AggregateRoot : Entity, IHasDomainEvents
    {
        private IList<DomainEvent> Events { get; } = new List<DomainEvent>();

        IReadOnlyList<DomainEvent> IHasDomainEvents.UncommittedChanges() => new ReadOnlyCollection<DomainEvent>(Events);

        void IHasDomainEvents.MarkChangesAsCommitted() => Events.Clear();

        public void Raise(DomainEvent evnt) => Events.Add(evnt);

        bool IHasDomainEvents.NewlyCreated() => Events.Any(x => x is ICreateEvent);

        public DateTime? DeleteDate { get; set; }

        public string GetJsonObject()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
