﻿using System.Collections.Generic;

namespace Betasuchus.Shared
{
    public interface IHasDomainEvents
    {
        public IReadOnlyList<DomainEvent> UncommittedChanges();

        public void MarkChangesAsCommitted();

        public void Raise(DomainEvent evnt);

        public bool NewlyCreated();
    }
}
