﻿using System.Collections.Generic;

namespace Betasuchus.Shared
{
    public class Error
    {
        public Error()
        {
        }

        public IEnumerable<string> Errors { get; set; }
    }
}
