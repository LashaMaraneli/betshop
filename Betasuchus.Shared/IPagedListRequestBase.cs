﻿namespace Betasuchus.Shared
{
    public interface IPagedListRequestBase
    {
        int PageSize { get; set; }

        int Page { get; set; }

        int Total { get; set; }
    }
}
