﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading;

namespace Betasuchus.Shared
{
    public class MultilanguageString
    {
        public MultilanguageString()
        {
        }

        /// <summary>
        /// Language translation for domain objects.
        /// </summary>
        /// <param name="translations">Dictionary(culture, text)</param>
        public MultilanguageString(string georgian, string english, string turkish, string russian)
        {
            if (string.IsNullOrEmpty(georgian) ||
                string.IsNullOrEmpty(english) ||
                string.IsNullOrEmpty(turkish) ||
                string.IsNullOrEmpty(russian))
            {
                throw new ArgumentNullException();
            }

            Georgian = georgian;
            English = english;
            Turkish = turkish;
            Russian = russian;
        }

        public string Georgian { get; set; }

        public string English { get; set; }

        public string Turkish { get; set; }

        public string Russian { get; set; }

        public MultilanguageString Clone()
        {
            return new MultilanguageString()
            {
                English = this.English,
                Georgian = this.Georgian,
                Russian = this.Russian,
                Turkish = this.Turkish
            };
        }

        [NotMapped]
        public string Text
        {
            get
            {
                var culture = Thread.CurrentThread.CurrentCulture;
                string text = string.Empty;

                switch (culture.Name)
                {
                    case "ka-GE": text = Georgian; break;
                    case "en-US": text = English; break;
                    case "ru-RU": text = Russian; break;
                    case "tr-TR": text = Turkish; break;
                }

                return text;
            }
        }
    }
}