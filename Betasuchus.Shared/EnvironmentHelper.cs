﻿using System;

namespace Betasuchus.Shared
{
    public static class EnvironmentHelper
    {
        public static string GetEnvironment() => Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

        public static bool IsDebug() => GetEnvironment() == "Development";

        public static bool IsStaging() => GetEnvironment() == "Staging";

        public static bool IsProduction() => GetEnvironment() == "Production";
    }
}
