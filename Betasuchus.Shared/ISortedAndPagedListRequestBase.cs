﻿namespace Betasuchus.Shared
{
    public interface ISortedAndPagedListRequestBase : IPagedListRequestBase
    {
        public SortOrder SortOrder { get; set; }

        public string SortBy { get; set; }
    }
}
