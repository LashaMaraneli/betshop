﻿using Betasuchus.Domain.ProviderManagement.Events;
using Betasuchus.Shared;

namespace Betasuchus.Domain.ProviderManagement
{
    public class Provider : AggregateRoot
    {
        public virtual MultilanguageString Name { get; private set; }

        public Provider() { }

        public Provider(long id, MultilanguageString name)
        {
            Id = id;
            Name = name;

            Raise(new ProviderCreated(id, this.GetJsonObject()));
        }

        public void Update(MultilanguageString name)
        {
            Name = name;

            Raise(new ProviderChanged(Id, this.GetJsonObject()));
        }
    }
}