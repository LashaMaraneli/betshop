﻿using Betasuchus.Shared;

namespace Betasuchus.Domain.ProviderManagement.Events
{
    public class ProviderCreated : DomainEvent, ICreateEvent
    {
        public ProviderCreated(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
