﻿using Betasuchus.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.ProviderManagement.Events
{
    public class ProviderChanged : DomainEvent
    {
        public ProviderChanged(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
