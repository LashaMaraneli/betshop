﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OperationManagement
{
    public enum InternalStatus
    {
        Draft = 1, // no operation made
        AwaitingPayment = 2, // requested payment and awaiting status
        AwaitingSending = 3, // awaiting sending to bonus hub
        Sent = 4, // sent to bonus hub
        Completed = 5 // completed processing
    }
}
