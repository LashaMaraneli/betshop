﻿using Betasuchus.Domain.OperationManagement.Events;
using Betasuchus.Shared;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Betasuchus.Domain.OperationManagement
{
    public class Operation : AggregateRoot
    {
        public long OfferId { get; private set; }

        public string Offer { get; private set; }

        public DateTime CreateDate { get; private set; }

        public DateTime StatusDate { get; private set; }

        public DateTime? CompleteDate { get; private set; }

        public long CustomerId { get; private set; }

        public string Customer { get; private set; }

        public InternalStatus InternalStatus { get; private set; }

        public OperationStatus? Status { get; private set; }

        [Column(TypeName = "decimal(9, 4)")]
        public decimal PayedAmount { get; private set; }

        public long OrderId { get; set; }

        public string BonusPoolId { get; private set; }

        public string PaymentTransactionId { get; private set; }

        public PaymentStatus PaymentStatus { get; private set; }

        public Operation()
        {
        }

        public void SetTransactionId(string paymentTransactionId)
        {
            PaymentTransactionId = paymentTransactionId;

            Raise(new OperationProcessingStarted(Id, GetJsonObject()));
        }

        public Operation(long id, long offerId, string offerSlimModel, long customerId, string customer, decimal payedAmount, long orderId)
        {
            Id = id;
            OfferId = offerId;
            Offer = offerSlimModel;
            CustomerId = customerId;
            Customer = customer;
            PayedAmount = payedAmount;
            OrderId = orderId;

            CreateDate = DateTime.Now;
            StatusDate = DateTime.Now;

            InternalStatus = InternalStatus.Draft;
        }

        public void SetBonusPoolId(string bonusPoolId)
        {
            BonusPoolId = bonusPoolId;
            StatusDate = DateTime.Now;

            Raise(new OperationBonusPoolSent(Id, GetJsonObject()));
        }

        public void SetStatus(InternalStatus internalStatus)
        {
            InternalStatus = internalStatus;

            Raise(new OperationStatusChanged(Id, GetJsonObject()));
        }

        public void SetStatus(InternalStatus internalStatus, OperationStatus? operationStatus)
        {
            InternalStatus = internalStatus;
            if (internalStatus == InternalStatus.Completed)
            {
                CompleteDate = DateTime.Now;
                Status = operationStatus;
            }

            Raise(new OperationStatusChanged(Id, GetJsonObject()));
        }
    }
}
