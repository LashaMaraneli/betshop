﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OperationManagement
{
    public enum OperationStatus
    {
        PaymentFailed = 1,
        PaymentSuccess = 2,
        BonusSuccess = 3,
        BonusFail = 4
    }
}
