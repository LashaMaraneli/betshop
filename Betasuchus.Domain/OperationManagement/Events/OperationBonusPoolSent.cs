﻿using Betasuchus.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OperationManagement.Events
{
    public class OperationBonusPoolSent : DomainEvent
    {
        public OperationBonusPoolSent(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
