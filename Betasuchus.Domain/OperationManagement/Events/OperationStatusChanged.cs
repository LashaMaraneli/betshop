﻿using Betasuchus.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OperationManagement.Events
{
    public class OperationStatusChanged : DomainEvent
    {
        public OperationStatusChanged(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
