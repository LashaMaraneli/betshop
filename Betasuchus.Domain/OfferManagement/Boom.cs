﻿using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.OfferManagement.Events;
using Betasuchus.Shared;
using System;
using System.Collections.Generic;

namespace Betasuchus.Domain.OfferManagement
{
    public class Boom : ExtendedOffer
    {
        public int FreebetNominal { get; private set; }

        public Boom() : base() { }

        public Boom(
            long id,
            int amount,
            DateTime expireDate,
            decimal price,
            decimal discountedPrice,
            int expire,
            string thumbnailUrl,
            MultilanguageString offerText,
            GameType gameType,
            long categoryId,
            string category,
            int freebetNominal,
            MultilanguageString gameName)
            : base(id,
                  amount,
                  expireDate,
                  price,
                  discountedPrice,
                  expire,
                  thumbnailUrl,
                  offerText,
                  gameType,
                  OfferManagement.ProviderType.Boom,
                  categoryId,
                  category,
                  gameName)
        {
            FreebetNominal = freebetNominal;

            Raise(new OfferCreated(id, this.GetJsonObject()));
        }

        public override Offer CloneWithNewId(long id, DateTime expireDate)
        {
            return new Boom(
                id,
                Amount,
                expireDate,
                Price,
                DiscountedPrice,
                Expire,
                ThumbnailUrl,
                OfferText.Clone(),
                GameType,
                CategoryId,
                Category,
                FreebetNominal,
                GameName.Clone());
        }

        public override Dictionary<string, object> BonusHubObject()
        {
            var dictionary = new Dictionary<string, object>
            {
                { "provider", (int)ProviderType.Value },
                { "count", Amount },
                { "amount", FreebetNominal },
                { "days", Expire },
                { "PaidAmount", DiscountedPrice }
            };

            return dictionary;
        }
    }
}
