﻿using Betasuchus.Domain.OfferManagement.Events;
using Betasuchus.Shared;
using System;
using System.Collections.Generic;

namespace Betasuchus.Domain.OfferManagement.Abstract
{
    public abstract class Offer : AggregateRoot, ISoftDeletable
    {
        /// <summary>
        /// Freebet/Freespin count.
        /// </summary>
        public int Amount { get; private set; }

        public DateTime ExpireDate { get; private set; }

        public decimal Price { get; private set; }

        public decimal DiscountedPrice { get; private set; }

        public string ThumbnailUrl { get; private set; }

        public MultilanguageString OfferText { get; private set; }

        public GameType GameType { get; private set; }

        public ProviderType? ProviderType { get; private set; }

        public long CategoryId { get; private set; }

        public string Category { get; private set; }

        public MultilanguageString GameName { get; private set; }

        public Offer() { }

        public Offer(
            long id,
            int amount,
            DateTime expireDate,
            decimal price,
            decimal discountedPrice,
            string thumbnailUrl,
            MultilanguageString offerText,
            GameType gameType,
            ProviderType providerType,
            long categoryId,
            string category,
            MultilanguageString gameName)
        {
            Id = id;

            Amount = amount;
            ExpireDate = expireDate;
            Price = price;
            DiscountedPrice = discountedPrice;

            ThumbnailUrl = thumbnailUrl;
            OfferText = offerText;

            GameType = gameType;
            ProviderType = providerType;

            CategoryId = categoryId;
            Category = category;

            GameName = gameName;
        }

        public abstract Offer CloneWithNewId(long id, DateTime expireDate);

        public virtual void Delete()
        {
            DeleteDate = DateTime.Now;

            Raise(new OfferDeleted(Id, GetJsonObject()));
        }

        public abstract Dictionary<string, object> BonusHubObject();

        public virtual OfferSlimModel ToOfferSlimModel()
        {
            return new OfferSlimModel
            {
                Text = OfferText,
                ProviderType = ProviderType,
                ExpireDate = ExpireDate,
                GameType = GameType,
                DiscountedPrice = DiscountedPrice
            };
        }
    }
}
