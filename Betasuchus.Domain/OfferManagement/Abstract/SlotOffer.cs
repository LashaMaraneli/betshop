﻿using Betasuchus.Domain.OfferManagement.Events;
using Betasuchus.Shared;
using System;

namespace Betasuchus.Domain.OfferManagement.Abstract
{
    public abstract class SlotOffer : ExtendedOffer
    {
        public long? ProviderId { get; private set; }

        public string Provider { get; private set; }

        public SlotOffer() : base() { }

        protected SlotOffer(
            long id,
            int amount,
            DateTime expireDate,
            decimal price,
            decimal discountedPrice,
            int expire,
            string thumbnailUrl,
            MultilanguageString offerText,
            GameType gameType,
            ProviderType providerType,
            long categoryId,
            string category,
            long? providerId,
            string provider,
            MultilanguageString gameName)
            : base(id,
                  amount,
                  expireDate,
                  price,
                  discountedPrice,
                  expire,
                  thumbnailUrl,
                  offerText,
                  gameType,
                  providerType,
                  categoryId,
                  category,
                  gameName)
        {
            ProviderId = providerId;
            Provider = provider;
        }

        public virtual void SetProvider(ProviderManagement.Provider provider)
        {
            ProviderId = provider.Id;
            Provider = provider.GetJsonObject();

            Raise(new OfferChanged(Id, GetJsonObject()));
        }
    }
}
