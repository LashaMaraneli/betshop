﻿using Betasuchus.Shared;
using System;

namespace Betasuchus.Domain.OfferManagement.Abstract
{
    public abstract class ExtendedOffer : Offer
    {
        /// <summary>
        /// Freebet/Freespin Expire days
        /// </summary>
        public int Expire { get; private set; }

        public ExtendedOffer() : base() { }

        public ExtendedOffer(
            long id,
            int amount,
            DateTime expireDate,
            decimal price,
            decimal discountedPrice,
            int expire,
            string thumbnailUrl,
            MultilanguageString offerText,
            GameType gameType,
            ProviderType providerType,
            long categoryId,
            string category,
            MultilanguageString gameName)
            : base(id,
                  amount,
                  expireDate,
                  price,
                  discountedPrice,
                  thumbnailUrl,
                  offerText,
                  gameType,
                  providerType,
                  categoryId,
                  category,
                  gameName)
        {
            Expire = expire;
        }
    }
}
