﻿using Betasuchus.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OfferManagement.Events
{
    public class OfferChanged : DomainEvent
    {
        public OfferChanged(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
