﻿using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.OfferManagement.Events;
using Betasuchus.Shared;
using System;
using System.Collections.Generic;

namespace Betasuchus.Domain.OfferManagement
{
    public class Poker : ExtendedOffer
    {
        public string BonusCode { get; private set; }

        public Poker() : base() { }

        public Poker(
            long id,
            int amount,
            DateTime expireDate,
            decimal price,
            decimal discountedPrice,
            int expire,
            string thumbnailUrl,
            MultilanguageString offerText,
            GameType gameType,
            long categoryId,
            string category,
            string bonusCode,
            MultilanguageString gameName)
            : base(id,
                  amount,
                  expireDate,
                  price,
                  discountedPrice,
                  expire,
                  thumbnailUrl,
                  offerText,
                  gameType,
                  OfferManagement.ProviderType.Poker,
                  categoryId,
                  category,
                  gameName)
        {
            BonusCode = bonusCode;

            Raise(new OfferCreated(id, this.GetJsonObject()));
        }

        public override Offer CloneWithNewId(long id, DateTime expireDate)
        {
            return new Poker(
                id,
                Amount,
                expireDate,
                Price,
                DiscountedPrice,
                Expire,
                ThumbnailUrl,
                OfferText.Clone(),
                GameType,
                CategoryId,
                Category,
                BonusCode,
                GameName);
        }

        public override Dictionary<string, object> BonusHubObject()
        {
            var dictionary = new Dictionary<string, object>();

            dictionary.Add("provider", (int)ProviderType.Value);
            dictionary.Add("promoCode", this.BonusCode);
            dictionary.Add("days", Expire);
            dictionary.Add("PaidAmount", DiscountedPrice);

            return dictionary;
        }
    }
}
