﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OfferManagement
{
    public enum ProviderType
    {
        Egt = 1,
        PlayNGo = 2,
        Boom = 3,
        Sport = 4,
        P2P = 5,
        Poker = 6
    }
}
