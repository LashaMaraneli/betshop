﻿using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.OfferManagement.Events;
using Betasuchus.Shared;
using System;
using System.Collections.Generic;

namespace Betasuchus.Domain.OfferManagement
{
    public class TableGame : ExtendedOffer
    {
        public TableGame() : base() { }

        public TableGame(
            long id,
            int amount,
            DateTime expireDate,
            decimal price,
            decimal discountedPrice,
            int expire,
            string thumbnailUrl,
            MultilanguageString offerText,
            GameType gameType,
            long categoryId,
            string category,
            MultilanguageString gameName)
            : base(id,
                  amount,
                  expireDate,
                  price,
                  discountedPrice,
                  expire,
                  thumbnailUrl,
                  offerText,
                  gameType,
                  OfferManagement.ProviderType.P2P,
                  categoryId,
                  category,
                  gameName)
        {
            Raise(new OfferCreated(id, this.GetJsonObject()));
        }

        public override Offer CloneWithNewId(long id, DateTime expireDate)
        {
            return new TableGame(
                id,
                Amount,
                expireDate,
                Price,
                DiscountedPrice,
                Expire,
                ThumbnailUrl,
                OfferText.Clone(),
                GameType,
                CategoryId,
                Category,
                GameName);
        }

        public override Dictionary<string, object> BonusHubObject()
        {
            var dictionary = new Dictionary<string, object>();

            dictionary.Add("provider", (int)ProviderType.Value);
            dictionary.Add("amount", Amount);
            dictionary.Add("days", Expire);
            dictionary.Add("PaidAmount", DiscountedPrice);


            return dictionary;
        }
    }
}
