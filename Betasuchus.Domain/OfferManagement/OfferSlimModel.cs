﻿using Betasuchus.Shared;
using System;

namespace Betasuchus.Domain.OfferManagement
{
    public class OfferSlimModel
    {
        public MultilanguageString Text { get; set; }

        public decimal DiscountedPrice { get; set; }

        public DateTime ExpireDate { get; set; }

        public GameType GameType { get; set; }

        public ProviderType? ProviderType { get; set; }
    }
}
