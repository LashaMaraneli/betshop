﻿using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.OfferManagement.Events;
using Betasuchus.Shared;
using System;
using System.Collections.Generic;

namespace Betasuchus.Domain.OfferManagement
{
    public class PlayNGo : SlotOffer
    {
        public string TriggerId { get; private set; }

        public PlayNGo() : base() { }

        public PlayNGo(
            long id,
            int amount,
            DateTime expireDate,
            decimal price,
            decimal discountedPrice,
            int expire,
            string thumbnailUrl,
            MultilanguageString offerText,
            GameType gameType,
            long categoryId,
            string category,
            string triggerId,
            long? providerId,
            string provider,
            MultilanguageString gameName)
            : base(id,
                  amount,
                  expireDate,
                  price,
                  discountedPrice,
                  expire,
                  thumbnailUrl,
                  offerText,
                  gameType,
                  OfferManagement.ProviderType.PlayNGo,
                  categoryId,
                  category,
                  providerId,
                  provider,
                  gameName)
        {
            TriggerId = triggerId;

            Raise(new OfferCreated(id, this.GetJsonObject()));
        }

        public override Offer CloneWithNewId(long id, DateTime expireDate)
        {
            return new PlayNGo(
                id, 
                Amount, 
                expireDate, 
                Price, 
                DiscountedPrice, 
                Expire, 
                ThumbnailUrl, 
                OfferText.Clone(), 
                GameType, 
                CategoryId, 
                Category, 
                TriggerId, 
                ProviderId, 
                Provider,
                GameName.Clone());
        }

        public override Dictionary<string, object> BonusHubObject()
        {
            var dictionary = new Dictionary<string, object>();

            dictionary.Add("provider", (int)ProviderType.Value);
            dictionary.Add("campaignCode", TriggerId);
            //dictionary.Add("count", Amount);
            //dictionary.Add("days", Expire);
            dictionary.Add("PaidAmount", DiscountedPrice);

            return dictionary;
        }
    }
}
