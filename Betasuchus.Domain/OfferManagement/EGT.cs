﻿using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.OfferManagement.Events;
using Betasuchus.Shared;
using System;
using System.Collections.Generic;

namespace Betasuchus.Domain.OfferManagement
{
    public class EGT : SlotOffer
    {
        public string CampaignId { get; private set; }

        public EGT() : base() { }

        public EGT(
            long id,
            int amount,
            DateTime expireDate,
            decimal price,
            decimal discountedPrice,
            int expire,
            string campaignId,
            string thumbnailUrl,
            MultilanguageString offerText,
            GameType gameType,
            long categoryId,
            string category,
            long? providerId,
            string provider,
            MultilanguageString gameName)
            : base(
                  id,
                  amount,
                  expireDate,
                  price,
                  discountedPrice,
                  expire,
                  thumbnailUrl,
                  offerText,
                  gameType,
                  OfferManagement.ProviderType.Egt,
                  categoryId,
                  category,
                  providerId,
                  provider,
                  gameName)
        {
            CampaignId = campaignId;

            Raise(new OfferCreated(id, this.GetJsonObject()));
        }

        public override Offer CloneWithNewId(long id, DateTime expireDate)
        {
            return new EGT(
                id,
                Amount,
                expireDate,
                Price,
                DiscountedPrice,
                Expire,
                CampaignId,
                ThumbnailUrl,
                OfferText.Clone(),
                GameType,
                CategoryId,
                Category,
                ProviderId,
                Provider,
                GameName.Clone());
        }

        public override Dictionary<string, object> BonusHubObject()
        {
            var dictionary = new Dictionary<string, object>
            {
                { "provider", (int)ProviderType.Value },
                { "campaignCode", CampaignId },
                { "count", Amount },
                { "days", Expire },
                { "PaidAmount", DiscountedPrice }
            };

            return dictionary;
        }
    }
}
