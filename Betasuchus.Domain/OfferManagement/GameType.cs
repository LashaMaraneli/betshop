﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OfferManagement
{
    public enum GameType
    {
        Bonus = 1,
        Freespin = 2,
        Freebet = 3,
        Boom = 4
    }
}
