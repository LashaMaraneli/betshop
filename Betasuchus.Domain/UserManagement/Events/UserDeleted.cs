﻿using Betasuchus.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.UserManagement.Events
{
    public class UserDeleted : DomainEvent
    {
        public UserDeleted(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
