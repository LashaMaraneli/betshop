﻿using Betasuchus.Shared;
using System;

namespace Betasuchus.Domain.UserManagement.Events
{
    public class UserCreated : DomainEvent, ICreateEvent
    {
        public UserCreated(long aggregateRootId, string payload)
            : base(aggregateRootId, payload)
        {
        }
    }
}
