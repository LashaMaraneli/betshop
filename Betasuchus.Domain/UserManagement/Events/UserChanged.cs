﻿using Betasuchus.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.UserManagement.Events
{
    public class UserChanged : DomainEvent
    {
        public UserChanged(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
