﻿using Betasuchus.Domain.UserManagement.Events;
using Betasuchus.Shared;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Betasuchus.Domain.UserManagement
{
    public class User : IdentityUser<long>, IHasDomainEvents, ISoftDeletable
    {
        public User()
        {
        }

        public User(long id, string firstName, string lastName, string email, string phoneNumber, DateTime? birthDate = null)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            PhoneNumber = phoneNumber;
            BirthDate = birthDate;
            UserName = email;
            NormalizedUserName = email.ToUpper();
            NormalizedEmail = email.ToUpper();

            Raise(new UserCreated(id, this.GetJsonObject()));
        }

        public User(string email)
        {
            Email = email;
            UserName = email;
        }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public DateTime? BirthDate { get; private set; }

        public DateTime CreateDate { get; set; }

        private IList<DomainEvent> _events { get; } = new List<DomainEvent>();

        public DateTime? DeleteDate { get; private set; }

        IReadOnlyList<DomainEvent> IHasDomainEvents.UncommittedChanges() => new ReadOnlyCollection<DomainEvent>(_events);

        void IHasDomainEvents.MarkChangesAsCommitted() => _events.Clear();

        public void Raise(DomainEvent evnt) => _events.Add(evnt);

        bool IHasDomainEvents.NewlyCreated() => _events.Any(x => x is ICreateEvent);

        public string GetJsonObject()
        {
            return JsonConvert.SerializeObject(this);
        }

        public void Delete()
        {
            DeleteDate = DateTime.Now;

            Raise(new UserDeleted(Id, GetJsonObject()));
        }

        public void UpdateUser(string firstName, string lastName, string email, string phoneNumber, DateTime? birthDate)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            PhoneNumber = phoneNumber;
            BirthDate = birthDate;
            UserName = email;
            NormalizedUserName = email.ToUpper();
            NormalizedEmail = email.ToUpper();

            Raise(new UserChanged(Id, GetJsonObject()));
        }
    }

    public class UserClaim : IdentityUserClaim<long>
    {
    }

    public class UserLogin : IdentityUserLogin<long>
    {
        public virtual long Id { get; set; }
    }

    public class UserRole : IdentityUserRole<long>
    {
        public virtual Role Role { get; set; }

        public virtual User User { get; set; }
    }

    public class UserToken : IdentityUserToken<long>
    {
        public virtual long Id { get; set; }
    }
}
