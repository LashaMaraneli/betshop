﻿using Microsoft.AspNetCore.Identity;

namespace Betasuchus.Domain.UserManagement
{
    public class Role : IdentityRole<long>
    {
        public Role() { }

        public Role(string roleName)
        {
            Name = roleName;
            NormalizedName = roleName.ToUpper();
        }
    }

    public class RoleClaim : IdentityRoleClaim<long>
    {
    }
}
