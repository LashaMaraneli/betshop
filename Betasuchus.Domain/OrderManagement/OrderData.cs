﻿namespace Betasuchus.Domain.OrderManagement
{
    public class OrderData
    {
        public long OfferId { get; set; }

        public int Count { get; set; }
    }
}
