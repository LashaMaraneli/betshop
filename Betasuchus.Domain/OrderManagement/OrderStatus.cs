﻿namespace Betasuchus.Domain.OrderManagement
{
    public enum OrderStatus
    {
        Draft = 1,
        Processing = 2,
        Success = 2,
        Fail = 3
    }
}
