﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OrderManagement
{
    public enum InternalStatus
    {
        Draft = 1,
        Processing = 2,
        Completed = 3
    }
}