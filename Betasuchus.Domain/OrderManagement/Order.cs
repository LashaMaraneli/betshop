﻿using Betasuchus.Domain.OrderManagement.Events;
using Betasuchus.Shared;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Betasuchus.Domain.OrderManagement
{
    public class Order : AggregateRoot
    {
        [Column(TypeName = "decimal(9, 4)")]
        public decimal TotalAmount { get; private set; }

        public string Card { get; private set; }

        public DateTime CreateDate { get; private set; }

        public DateTime StatusDate { get; private set; }

        public DateTime? CompleteDate { get; private set; }

        public long CustomerId { get; private set; }

        public string Customer { get; private set; }

        public string CustomerPin { get; private set; }

        public InternalStatus InternalStatus { get; private set; }

        public PaymentType PaymentType { get; private set; }

        public string PaymentTransactionId { get; private set; }

        public string StatusDescription { get; private set; }

        public Order()
        {
        }

        public Order(long id, decimal totalAmount, string card, long customerId, string customer, PaymentType paymentType, string customerPin)
        {
            Id = id;
            TotalAmount = totalAmount;
            Card = card;
            CustomerId = customerId;
            Customer = Customer;
            CreateDate = DateTime.Now;
            StatusDate = DateTime.Now;

            PaymentType = paymentType;

            CustomerPin = customerPin;

            InternalStatus = InternalStatus.Draft;

            Raise(new OrderCreated(id, GetJsonObject()));
        }

        public void Complete()
        {
            InternalStatus = InternalStatus.Completed;

            CompleteDate = DateTime.Now;

            Raise(new OrderCompleted(Id, GetJsonObject()));
        }

        public void SetPaymentIdAndBeginProcessing(string paymentId)
        {
            PaymentTransactionId = paymentId;
            InternalStatus = InternalStatus.Processing;

            Raise(new OrderProcessingStarted(Id, GetJsonObject()));
        }
    }
}