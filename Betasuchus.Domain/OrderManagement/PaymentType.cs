﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OrderManagement
{
    public enum PaymentType
    {
        Card = 1,
        NeolletWallet = 2,
        EuropeBetWallet = 3
    }
}
