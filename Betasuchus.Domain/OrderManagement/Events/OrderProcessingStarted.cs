﻿using Betasuchus.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OrderManagement.Events
{
    public class OrderProcessingStarted : DomainEvent
    {
        public OrderProcessingStarted(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
