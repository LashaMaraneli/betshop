﻿using Betasuchus.Shared;

namespace Betasuchus.Domain.OrderManagement.Events
{
    public class OrderUpdated : DomainEvent
    {
        public OrderUpdated(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
