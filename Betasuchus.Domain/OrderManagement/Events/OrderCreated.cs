﻿using Betasuchus.Shared;

namespace Betasuchus.Domain.OrderManagement.Events
{
    public class OrderCreated : DomainEvent, ICreateEvent
    {
        public OrderCreated(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
