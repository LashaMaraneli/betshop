﻿using Betasuchus.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.OrderManagement.Events
{
    public class OrderCompleted : DomainEvent
    {
        public OrderCompleted(long aggregateRootId, string payload) : base(aggregateRootId, payload)
        {
        }
    }
}
