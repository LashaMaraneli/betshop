﻿using Betasuchus.Domain.CategoryManagement.Events;
using Betasuchus.Shared;

namespace Betasuchus.Domain.CategoryManagement
{
    public class Category : AggregateRoot
    {
        public long? ParentCategoryId { get; private set; }

        public virtual MultilanguageString Name { get; private set; }

        public string ThumbnailUrl { get; private set; }

        public Category() { }

        public Category(long id, long? parentCategoryId, MultilanguageString name, string thumbnailUrl)
        {
            Id = id;
            ParentCategoryId = parentCategoryId;
            Name = name;
            ThumbnailUrl = thumbnailUrl;

            Raise(new CategoryCreated(Id, this.GetJsonObject()));
        }

        public void EditCategory(MultilanguageString name, string thumbnailUrl = null)
        {
            Name = name;
            ThumbnailUrl = thumbnailUrl ?? ThumbnailUrl;

            Raise(new CategoryChanged(this.Id, this.GetJsonObject()));
        }
    }
}
