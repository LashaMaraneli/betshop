﻿using Betasuchus.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Domain.CategoryManagement.Events
{
    public class CategoryChanged : DomainEvent
    {
        public CategoryChanged(long id, string payload) : base(id, payload)
        {
        }
    }
}
