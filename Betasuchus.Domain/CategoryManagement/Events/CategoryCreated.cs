﻿using Betasuchus.Shared;

namespace Betasuchus.Domain.CategoryManagement.Events
{
    public class CategoryCreated : DomainEvent, ICreateEvent
    {
        public CategoryCreated(long id, string payload) : base(id, payload)
        {
        }
    }
}