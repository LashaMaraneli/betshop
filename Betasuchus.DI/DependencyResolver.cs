﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.BonusHub;
using Betasuchus.Application.IntegrationServices.FileManagement;
using Betasuchus.Application.IntegrationServices.FileManagement.S3Integration;
using Betasuchus.Application.IntegrationServices.PaymentManagement;
using Betasuchus.Application.IntegrationServices.PaymentManagement.Neollet.Configs;
using Betasuchus.Application.IntegrationServices.SequenceProvider;
using Betasuchus.Domain.UserManagement;
using Betasuchus.Infrastructure.Db;
using Betasuchus.Infrastructure.EventDispatching;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Serilog;
using System.IdentityModel.Tokens.Jwt;

namespace Betasuchus.DI
{
    public static class DependencyResolver
    {
        public static IServiceCollection Resolve(this IServiceCollection services, IWebHostEnvironment env, IConfiguration configuration, bool inMemory = false, bool forApi = true)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Console(Serilog.Events.LogEventLevel.Information).CreateLogger();

            services ??= new ServiceCollection();

            var connectionString = configuration.GetConnectionString("BetasuchusDbContext");

            services.AddDbContext<BetasuchusDbContext>(builder =>
                builder.UseNpgsql(connectionString).UseLazyLoadingProxies());

            services.AddScoped(x => new InternalDomainEventDispatcher(
                services.BuildServiceProvider(),
                typeof(User).Assembly,
                typeof(Command).Assembly));

            services.Configure<BonusHubSettings>(configuration.GetSection(nameof(BonusHubSettings)));
            services.Configure<CardPaymentProviderConfig>(configuration.GetSection(nameof(CardPaymentProviderConfig)));
            services.Configure<NeolletPaymentProviderConfig>(configuration.GetSection(nameof(NeolletPaymentProviderConfig)));
            services.Configure<EuropeBetWalletProviderConfig>(configuration.GetSection(nameof(EuropeBetWalletProviderConfig)));

            //services.AddScoped<AuthHelper>();
            services.AddScoped<CommandExecutor>();
            services.AddScoped<QueryExecutor>();
            services.AddScoped<UnitOfWork>();

            services.AddScoped(typeof(ISequenceProvider<>), typeof(SequenceProvider<>));

            services.AddScoped<IPaymentProviderFactory>(provider =>
            {
                if (env.IsDevelopment())
                {
                    return new MockPaymentProviderFactory();
                }

                return new PaymentProviderFactory(provider);
            });

            services.AddScoped<IBonusHubManager>(provider =>
            {
                if (env.IsDevelopment())
                {
                    return new MockBonusHubManager();
                }

                return new BonusHubManager(
                    provider.GetService<IOptions<BonusHubSettings>>()
                    );
            });

            services.AddScoped<IFileManager>((service) =>
            {
                if (env.IsDevelopment())
                {
                    return new MockFileManager();
                }

                return new FileManager(service.GetService<IOptions<S3Config>>());
            });

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            //services
            //    .AddAuthentication(options =>
            //    {
            //        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //        options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            //        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //    })
            //    .AddJwtBearer(cfg =>
            //    {
            //        cfg.RequireHttpsMetadata = false;
            //        cfg.SaveToken = true;
            //        cfg.TokenValidationParameters = new TokenValidationParameters
            //        {
            //            ValidateIssuer = false,
            //            ValidateAudience = false,
            //            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("&@$NjOHF92=2nasdgknj2 35rdd")),
            //        };
            //    });

            services.AddHttpContextAccessor();

            services.AddScoped<ApplicationContext, ApplicationContext>((ctx) =>
            {
                return new ApplicationContext(ctx.GetService<IHttpContextAccessor>())
                {
                    //AppSetting = new ApplicationContext.AppSettings { JwtSignKey = Encoding.ASCII.GetBytes("$GGT5L@D4-kK6j]m") }
                };
            });

            return services;
        }
    }
}
