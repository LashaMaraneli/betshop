﻿namespace Wandio.Web.Mvc
{
    public class EntityActionsDropdown : EntityActions
    {
        public EntityActionsDropdown(string dropdownTitle)
        {
            DropdownTitle = dropdownTitle;
        }

        public string DropdownTitle { get; set; }
    }
}
