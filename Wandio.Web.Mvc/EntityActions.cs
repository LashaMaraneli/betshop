﻿using System.Collections.Generic;
using Wandio.Web.Mvc.Html;

namespace Wandio.Web.Mvc
{
    public class EntityActions : List<EntityAction>
    {
        public EntityActions Add(bool condition, EntityAction action)
        {
            if (condition)
            {
                Add(action);
            }

            return this;
        }

        public EntityActions Add(
            bool condition,
            string title,
            string href,
            string icon = null,
            BootstrapContext? backgroundContext = null,
            string group = null)
        {
            if (condition)
            {
                Add(new EntityAction(title, href, icon, backgroundContext, group));
            }

            return this;
        }
    }
}