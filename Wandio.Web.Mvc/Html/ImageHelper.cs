﻿// using System;
// using System.Collections.Generic;
// using System.Drawing;
// using System.Drawing.Drawing2D;
// using System.Drawing.Imaging;
// using System.IO;
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;
//
// namespace Wandio.Web.Mvc.Html
// {
//    public class ImageHelper
//    {
//        /// <summary>
//        /// Resizes the image to fit to specified width and height
//        /// </summary>
//        /// <param name="imageContents"></param>
//        /// <param name="width"></param>
//        /// <param name="height"></param>
//        /// <returns></returns>
//        public static byte[] ResizeImage(byte[] imageContents, int width, int height)
//        {
//            if (imageContents == null)
//            {
//                throw new ArgumentNullException("imageFile");
//            }
//            using (Image original = Image.FromStream(new MemoryStream(imageContents)))
//            {
//                int targetHeight = 0;
//                int targetWidth = 0;
//
//                if (original.Height > original.Width)
//                {
//                    targetHeight = height;
//
//                    targetWidth = (int)(original.Width * ((float)targetHeight / (float)original.Height));
//                }
//                else
//                {
//                    targetWidth = width;
//
//                    targetHeight = (int)(original.Height * ((float)targetWidth / (float)original.Width));
//                }
//
//                // Create a new blank canvas.  The resized image will be drawn on this canvas.
//                using (Bitmap bmPhoto = new Bitmap(targetWidth, targetHeight, original.PixelFormat))
//                {
//                    using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
//                    {
//                        grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
//                        grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
//                        grPhoto.CompositingQuality = CompositingQuality.HighQuality;
//                        grPhoto.PixelOffsetMode = PixelOffsetMode.Default;
//                        grPhoto.DrawImage(original, new Rectangle(0, 0, targetWidth, targetHeight), 0, 0, original.Width, original.Height, GraphicsUnit.Pixel);
//
//                        using (MemoryStream resultStream = new MemoryStream())
//                        {
//                            bmPhoto.Save(resultStream, original.RawFormat);
//                            return resultStream.GetBuffer();
//                        }
//                    }
//                }
//            }
//        }
//    }
// }
