﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Wandio.Web.Mvc.Html
{
    public static class TempDataExtensions
    {
        public static string SuccessKey { get; set; } = "success";

        public static string ErrorKey { get; set; } = "danger";

        public static string InformationKey { get; set; } = "info";

        public static void AddSuccess(this ITempDataDictionary tempData, string message)
        {
            AddMessageImpl(tempData, SuccessKey, message);
        }

        public static void AddError(this ITempDataDictionary tempData, string message)
        {
            AddMessageImpl(tempData, ErrorKey, message);
        }

        public static void AddResult(this ITempDataDictionary tempData, bool success, string message)
        {
            if (success)
            {
                tempData.AddSuccess(message);
            }
            else
            {
                tempData.AddError(message);
            }
        }

        public static void AddInformation(this ITempDataDictionary tempData, string message)
        {
            AddMessageImpl(tempData, InformationKey, message);
        }

        public static HtmlString RenderFlash(this IHtmlHelper helper, FlashRenderMode renderMode = FlashRenderMode.Toastr, bool container = true, string containerClass = null)
        {
            if (renderMode == FlashRenderMode.BootstrapAlert)
            {
                return RenderFlashAsBootstrapAlert(helper, container, containerClass);
            }
            else
            {
                return RenderFlasAsToastr(helper);
            }
        }

        private static void AddMessageImpl(ITempDataDictionary tempData, string key, string message)
        {
            tempData[key] = message;
        }

        private static HtmlString RenderFlasAsToastr(IHtmlHelper helper)
        {
            var tempData = helper.ViewContext.TempData;

            StringBuilder toastrScriptBuilder = new StringBuilder();
            var encoder = JavaScriptEncoder.Create(new TextEncoderSettings());

            if (tempData[InformationKey] != null)
            {
                string message = tempData[InformationKey].ToString();
                toastrScriptBuilder.AppendLine("toastr.info('" + encoder.Encode(tempData[InformationKey].ToString()) + "'); ");
                tempData.Remove(InformationKey);
            }

            if (tempData[ErrorKey] != null)
            {
                string message = tempData[ErrorKey].ToString();
                toastrScriptBuilder.AppendLine("toastr.error('" + encoder.Encode(tempData[ErrorKey].ToString()) + "'); ");
                tempData.Remove(ErrorKey);
            }

            if (tempData[SuccessKey] != null)
            {
                string message = tempData[SuccessKey].ToString();
                toastrScriptBuilder.AppendLine("toastr.success('" + encoder.Encode(tempData[SuccessKey].ToString()) + "'); ");
                tempData.Remove(SuccessKey);
            }

            return new HtmlString(toastrScriptBuilder.ToString());
        }

        private static HtmlString RenderFlashAsBootstrapAlert(IHtmlHelper helper, bool container, string containerClass)
        {
            var tempData = helper.ViewContext.TempData;
            List<TagBuilder> alerts = new List<TagBuilder>();

            Dictionary<string, string> messageKeys = new Dictionary<string, string>();

            messageKeys.Add(InformationKey, "info-circle");
            messageKeys.Add(SuccessKey, "check");
            messageKeys.Add(ErrorKey, "danger");

            foreach (var item in messageKeys)
            {
                if (tempData[item.Key] != null)
                {
                    TagBuilder alertContainerBuilder = new TagBuilder("div");
                    if (container)
                    {
                        alertContainerBuilder.AddCssClass("container alert-cont");
                    }

                    if (!string.IsNullOrWhiteSpace(containerClass))
                    {
                        alertContainerBuilder.AddCssClass(containerClass);
                    }

                    var alertBuilder = new TagBuilder("div");

                    alertBuilder.MergeAttribute("data-dismissable", bool.TrueString.ToLower());
                    alertBuilder.AddCssClass("alert alert-dismissable");
                    alertBuilder.AddCssClass(string.Concat("alert-", item.Key));

                    alertBuilder.InnerHtml.Append("<button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button><i class=\"fa fa-" + item.Value + "\"></i> " + tempData[item.Key]);
                    alertBuilder.InnerHtml.Append(alertBuilder.ToString());
                    alerts.Add(alertContainerBuilder);
                    tempData.Remove(item.Key);
                }
            }

            return new HtmlString(string.Concat(alerts.Select(alert => alert.ToString())));
        }
    }
}