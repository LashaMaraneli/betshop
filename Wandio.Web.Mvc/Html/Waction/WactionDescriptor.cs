﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;
// using System.Web.Mvc;
// using System.Web.Routing;
//
// namespace Wandio.Web.Mvc.Html.Waction
// {
//    /// <summary>
//    /// Describes default values to render waction
//    /// </summary>
//    public class WactionDescriptor
//    {
//        /// <summary>
//        /// Specifies the key for an action.
//        /// </summary>
//        public string Key { get; set; }
//
//        /// <summary>
//        /// Specifies default title for an action. This value can be overriden when rendering
//        /// </summary>
//        public string Title { get; set; }
//
//        /// <summary>
//        /// Specifies route values for an action
//        /// </summary>
//        public RouteValueDictionary ActionRouteValues { get; set; }
//
//        /// <summary>
//        /// Specifies what should happen after action is successfully processed. This value can be overriden when rendering
//        /// </summary>
//        public WactionResultProcessingHint DefaultResultProcessingHint { get; set; }
//
//        /// <summary>
//        /// Speifies function that will check whether action is ok or not. In runtime, dynamic parameter will be passed to be able to check condition for specific entity. For example, dynamic parameter may be new { DocumentId = "5", Status = 3" };
//        /// </summary>
//        public Func<dynamic, bool> Condition { get; set; }
//
//        /// <summary>
//        /// Specifies that waction should be rendered despite of condition. This value can be overriden when rendering
//        /// </summary>
//        public bool RenderAnyways { get; set; }
//
//        /// <summary>
//        /// Specifies group for waction. This can be used to list all the actions related to specific group, for example, "Employees"
//        /// </summary>
//        public string Group { get; set; }
//
//        /// <summary>
//        /// Number of items this waction affects.
//        /// </summary>
//        public WactionItemScope ItemScope { get; set; }
//
//
//        /// <summary>
//        /// Prepends icon into generated link like <i class=fa fa-Icon></i>
//        /// </summary>
//        public string Icon { get; set; }
//
//
//        /// <summary>
//        /// Checks condition. Null condition means that condition is not checked, therefore action is always rendered
//        /// </summary>
//        /// <param name="parameters"></param>
//        /// <returns></returns>
//        public bool CheckCondition(dynamic parameters)
//        {
//            if (Condition == null)
//            {
//                return true;
//            }
//
//            return Condition(parameters);
//        }
//    }
// }
