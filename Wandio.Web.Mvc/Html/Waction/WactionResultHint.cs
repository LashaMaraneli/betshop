﻿namespace Wandio.Web.Mvc.Html.Waction
{
    public enum WactionResultProcessingHint
    {
        RefreshWindow = 0,
        Redirect = 1,
        RefreshRow = 2,
        RefreshContainer = 3,
        DoNothing = 4,
    }
}
