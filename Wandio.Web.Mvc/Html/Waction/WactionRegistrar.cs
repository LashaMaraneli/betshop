﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;
// using System.Web.Routing;
//
// namespace Wandio.Web.Mvc.Html.Waction
// {
//    /// <summary>
//    /// Acts as registry for application wide wactions
//    /// </summary>
//    public static class WactionRegistrar
//    {
//        internal static Dictionary<string, WactionDescriptor> Actions { get; set; }
//
//        static WactionRegistrar()
//        {
//            Actions = new Dictionary<string, WactionDescriptor>();
//        }
//
//        /// <summary>
//        /// Add waction to registry
//        /// </summary>
//        /// <param name="wactionDescriptor"></param>
//        public static void Waction(WactionDescriptor wactionDescriptor)
//        {
//            Actions.Add(wactionDescriptor.Key, wactionDescriptor);
//        }
//
//        /// <summary>
//        /// add waction to regsitry
//        /// </summary>
//        /// <param name="key">Key for action</param>
//        /// <param name="title">Default title for waction</param>
//        /// <param name="routeValues">Route values for waction</param>
//        /// <param name="condition">Condition for waction</param>
//        /// <param name="group">Group for action</param>
//        public static void Waction(string key, string title, RouteValueDictionary routeValues, Func<dynamic, bool> condition = null, string group = null, string icon = null)
//        {
//            Waction(new WactionDescriptor()
//            {
//                Key = key,
//                Title = title,
//                ActionRouteValues = routeValues,
//                Condition = condition,
//                Icon = icon,
//                Group = group,
//            });
//        }
//    }
// }
