﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;
// using System.Web.Mvc;
// using System.Web.Mvc.Html;
// using System.Web.Routing;
// using System.Web.WebPages;
//
// namespace Wandio.Web.Mvc.Html.Waction
// {
//    /// <summary>
//    /// Helpers for use with waction html element generation
//    /// </summary>
//    public static class WactionHelpers
//    {
//        /// <summary>
//        /// Checks whether action is available
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="key"></param>
//        /// <param name="parameters">Null if no additional parameters required or condition is always true</param>
//        public static bool WactionCheck(this HtmlHelper helper, string key, dynamic parameters = null)
//        {
//            return GetDescriptor(key).CheckCondition(parameters);
//        }
//
//        /// <summary>
//        /// Checks whether action is available
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="keys"></param>
//        /// <param name="parameters">Null if no additional parameters required or condition is always true</param>
//        public static bool WactionCheckAny(this HtmlHelper helper, dynamic parameters = null, params string[] keys)
//        {
//            return keys.Any(key => GetDescriptor(key).CheckCondition(parameters));
//        }
//
//        /// <summary>
//        /// Generates waction anchor based on waction key provided
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="key"></param>
//        /// <param name="parameters">Required parameters that will be used to render waction for specific entity. Generally should contan Ids or other route information</param>
//        /// <param name="htmlAttributes">Html attributes for generated anchor. if contains key named icon, <i class=fa fa-icon></i> will be prepended to the generated anchor inner text</param>
//        /// <returns></returns>
//        public static MvcHtmlString WactionLink(this HtmlHelper helper, string key, dynamic parameters = null, string title = null, bool? renderAlwaysOverride = null, object htmlAttributes = null)
//        {
//            var descriptor = GetDescriptor(key);
//
//            bool render = renderAlwaysOverride == true || descriptor.CheckCondition(parameters);
//
//            if (render)
//            {
//                var attributes = new RouteValueDictionary(GetWactionDataAttributes(helper, descriptor, parameters));
//
//                if (htmlAttributes != null)
//                {
//                    var htmlAttributesDicitonary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
//                    foreach (var item in attributes)
//                    {
//                        attributes.Add(item.Key, item.Value);
//                    }
//                }
//
//                TagBuilder anchorBuilder = new TagBuilder("a");
//                anchorBuilder.MergeAttributes<string, object>(attributes);
//                if (!string.IsNullOrWhiteSpace(descriptor.Icon))
//                {
//                    anchorBuilder.InnerHtml += string.Format("<i class=\"fa {0}\"></i> ", descriptor.Icon);
//                }
//
//                anchorBuilder.InnerHtml += title ?? descriptor.Title;
//
//                return MvcHtmlString.Create(anchorBuilder.ToString());
//            }
//
//            return MvcHtmlString.Empty;
//        }
//
//        /// <summary>
//        /// Gives ability to write waction html element
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="wactionName"></param>
//        /// <param name="parameters"></param>
//        /// <param name="template">Template cannot really be null</param>
//        /// <returns></returns>
//        public static HelperResult Waction(this HtmlHelper helper, string wactionName, dynamic parameters = null, Func<WactionRenderParams, HelperResult> template = null)
//        {
//            var descriptor = GetDescriptor(wactionName);
//            var renderParams = GetWactionRenderParams(helper, descriptor, parameters);
//
//            return new HelperResult(writer =>
//            {
//                template(renderParams).WriteTo(writer);
//            });
//        }
//
//        private static WactionDescriptor GetDescriptor(string key)
//        {
//            return WactionRegistrar.Actions[key];
//        }
//
//        /// <summary>
//        /// Renders all the waction attributes to the stream. Especially useful when attaching attributes to custom html elements
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="wactionKey"></param>
//        /// <param name="parameters"></param>
//        /// <returns></returns>
//        public static MvcHtmlString RenderWactionDataAttributes(this HtmlHelper helper, string key, dynamic parameters = null)
//        {
//            return RenderHelpers.RenderAttributes(helper, GetWactionDataAttributes(helper, key, parameters));
//        }
//
//        /// <summary>
//        /// Gets calculated parameters that may be used to render waction
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="wactionKey"></param>
//        /// <param name="parameters"></param>
//        /// <returns></returns>
//        public static WactionRenderParams GetWactionRenderParams(this HtmlHelper helper, WactionDescriptor waction, dynamic parameters = null)
//        {
//            var result = new WactionRenderParams
//            {
//                Key = waction.Key,
//                Url = GetWactionUrl(helper, waction, parameters),
//                Condition = waction.CheckCondition(parameters),
//                Descriptor = waction
//            };
//
//            return result;
//        }
//
//        /// <summary>
//        /// Gets calculated parameters that may be used to render waction
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="wactionKey"></param>
//        /// <param name="parameters"></param>
//        /// <returns></returns>
//        public static WactionRenderParams GetWactionRenderParams(this HtmlHelper helper, string key, dynamic parameters = null)
//        {
//            var descriptor = GetDescriptor(key);
//
//            var result = new WactionRenderParams
//            {
//                Key = descriptor.Key,
//                Url = GetWactionUrl(helper, descriptor, parameters),
//                Condition = descriptor.CheckCondition(parameters),
//                Descriptor = descriptor
//            };
//
//            return result;
//        }
//
//        /// <summary>
//        /// Get data-* attributes that can be used to make any html element waction
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="wactionKey"></param>
//        /// <param name="parameters"></param>
//        /// <returns></returns>
//        public static IDictionary<string, object> GetWactionDataAttributes(this HtmlHelper helper, string key, dynamic parameters = null)
//        {
//            var descriptor = GetDescriptor(key);
//
//            return GetWactionDataAttributes(helper, descriptor, parameters);
//        }
//
//        /// <summary>
//        /// Get data-* attributes that can be used to make any html element waction
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="waction"></param>
//        /// <param name="parameters"></param>
//        /// <returns></returns>
//        public static IDictionary<string, object> GetWactionDataAttributes(this HtmlHelper helper, WactionDescriptor waction, dynamic parameters = null)
//        {
//            var renderParams = (WactionRenderParams)GetWactionRenderParams(helper, waction, parameters);
//            Dictionary<string, object> result = new Dictionary<string, object>();
//
//            result.Add("data-waction", bool.TrueString.ToLower());
//            result.Add("data-waction-key", renderParams.Key);
//            result.Add("data-waction-url", renderParams.Url);
//
//            return result;
//        }
//
//        /// <summary>
//        /// Generates waction url for specific entity by parameters
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="waction"></param>
//        /// <param name="parameters"></param>
//        /// <returns></returns>
//        public static string GetWactionUrl(this HtmlHelper helper, WactionDescriptor waction, dynamic parameters)
//        {
//            return new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection).RouteUrl(GetWactionRouteValues(helper, waction, parameters));
//        }
//
//        /// <summary>
//        /// Generates waction url for specific entity by parameters
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="waction"></param>
//        /// <param name="parameters"></param>
//        /// <returns></returns>
//        public static RouteValueDictionary GetWactionRouteValues(this HtmlHelper helper, WactionDescriptor waction, dynamic parameters)
//        {
//            var newRouteValues = new RouteValueDictionary(waction.ActionRouteValues);
//            if (parameters != null)
//            {
//                var newParameters = HtmlHelper.AnonymousObjectToHtmlAttributes((object)parameters);
//                foreach (var item in newParameters)
//                {
//                    newRouteValues.Add(item.Key, item.Value);
//                }
//            }
//
//
//            return newRouteValues;
//        }
//    }
// }
