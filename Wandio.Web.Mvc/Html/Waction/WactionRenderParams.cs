﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;
//
// namespace Wandio.Web.Mvc.Html.Waction
// {
//    /// <summary>
//    /// Describes generated, computed parameters, like url, condition
//    /// </summary>
//    public class WactionRenderParams
//    {
//        /// <summary>
//        /// Key of a waction
//        /// </summary>
//        public string Key { get; set; }
//
//        /// <summary>
//        /// Generated final url
//        /// </summary>
//        public string Url { get; set; }
//
//        /// <summary>
//        /// Calculated condition
//        /// </summary>
//        public bool Condition { get; set; }
//
//
//        /// <summary>
//        /// Original descriptor
//        /// </summary>
//        public WactionDescriptor Descriptor { get; set; }
//    }
// }
