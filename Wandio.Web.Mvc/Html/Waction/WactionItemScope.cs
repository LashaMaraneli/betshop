﻿namespace Wandio.Web.Mvc.Html.Waction
{
    public enum WactionItemScope
    {
        /// <summary>
        /// Default value. For Add-type operations, AddDocument, AddEmployee that require no existing item
        /// </summary>
        None = 0,

        /// <summary>
        /// Waction affects single entity
        /// </summary>
        Single = 1,

        /// <summary>
        /// Waction can be used on multiple entities
        /// </summary>
        Multiple = 2,
    }
}
