﻿// using Microsoft.AspNetCore.Html;
// using Microsoft.AspNetCore.Mvc.ViewFeatures;
// using System;
// using System.Collections.Generic;
// using System.Text;
// using System.Web;

// namespace Wandio.Web.Mvc.Html
// {
//    /// <summary>
//    /// General helpers to use within razor for rendering process
//    /// </summary>
//    public static class RenderHelpers
//    {
//        /// <summary>
//        /// Renders text to stream if value is true
//        /// </summary>
//        /// <typeparam name="TModel"></typeparam>
//        /// <param name="helper"></param>
//        /// <param name="value"></param>
//        /// <param name="text"></param>
//        /// <returns></returns>
//        public static HtmlString WriteIf<TModel>(this HtmlHelper<TModel> helper, bool value, string text)
//        {
//            if (value)
//            {
//                return HtmlString.Create(text);
//            }

// return HtmlString.Empty;
//        }

// /// <summary>
//        /// Renders text to stream if both side of the operands are equal
//        /// </summary>
//        /// <typeparam name="TModel"></typeparam>
//        /// <param name="helper"></param>
//        /// <param name="left"></param>
//        /// <param name="right"></param>
//        /// <param name="text"></param>
//        /// <returns></returns>
//        public static HtmlString WriteIfEquals<TModel>(this HtmlHelper<TModel> helper, bool left, bool right, string text)
//        {
//            return helper.WriteIf(left == right, text);
//        }

// /// <summary>
//        /// Renders specified attributes to stream
//        /// </summary>
//        /// <param name="helper"></param>
//        /// <param name="attributes"></param>
//        /// <returns></returns>
//        public static MvcHtmlString RenderAttributes(this HtmlHelper helper, IDictionary<string, object> attributes)
//        {
//            StringBuilder sb = new StringBuilder();

// foreach (var current in attributes)
//            {
//                string key = current.Key;
//                string currentValue = Convert.ToString(current.Value);
//                if (!string.Equals(key, "id", StringComparison.Ordinal) || !string.IsNullOrEmpty(currentValue))
//                {
//                    string value = HttpUtility.HtmlAttributeEncode(currentValue);
//                    sb.Append(' ').Append(key).Append("=\"").Append(value).Append('"');
//                }
//            }

// return MvcHtmlString.Create(sb.ToString());
//        }
//    }
// }
