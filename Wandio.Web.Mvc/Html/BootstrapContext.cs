﻿namespace Wandio.Web.Mvc.Html
{
    public enum BootstrapContext
    {
        Default,
        Primary,
        Success,
        Info,
        Warning,
        Danger,
    }
}
