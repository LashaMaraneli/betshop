﻿namespace Wandio.Web.Mvc.Html
{
    public enum FlashRenderMode
    {
        BootstrapAlert = 1,
        Toastr = 2,
    }
}
