﻿var entityActions = function () {
    var confirmOnlyAction = 'confirm-only';

    function getFolderIds() {
        var folderIds = [];
        $('.folder-checkbox:checked').each(function () {
            folderIds.push($(this).attr('id'));
        });
        return folderIds.join(",").toString();
    }

    var handleModalActions = function () {
        $(document).on('click', 'a.prevent-middle', function (event) {

        });
        $(document).on('click', '[data-waction]:not(.custom)', function (event) {
            event.preventDefault();
            var $this = $(this);
            var currentHref = $this.data('waction-url');
            var newHref = currentHref;
            if ($this.hasClass('depend-on-checkbox')) {
                newHref = currentHref + "&ids=" + getFolderIds();
            }
            handleEntityAction(newHref);
        });
    };



    var handleEntityAction = function (href, successCallback, cancelCallbak) {
        $.blockUI();
        $.get(href, function (result) {
            if (result.redirect) {
                window.location.href = result.redirect;
            }
            else {
                setupModalActionModal(result, successCallback, cancelCallbak);
            }
            //var modal = $(result).filter('.modal').modal();
            //modal.on('hidden.bs.modal', function () {
            //    $(this).remove();
            //});
        });
    };

    var setupModalActionModal = function (data, successCallback, cancelCallbak) {
        $('.modal-dialog').unblock();
        var $existing = $('#js-entity-action-modal');
        var parsedHtml = $.parseHTML(data);
        var $modal = $(parsedHtml).filter('.modal');
        var $form;
        if ($existing.length) {
            $existing.html($modal.html());
            $form = $existing.find('form');
        }
        else {
            $('body').append($modal);
            $modal.filter('.modal').modal();
            $modal.on('hidden.bs.modal', function (e) {
                if (cancelCallbak) {
                    cancelCallbak();
                }
                $(this).remove();
            })
            $form = $modal.find('form');
        }
        if ($form.length) {
            $.validator.unobtrusive.reParse($form);
            $form.on('submit', function (event) {
                if ($form.valid()) {
                    event.preventDefault();
                    $('.modal-dialog').block();
                    $form.ajaxSubmit(
                        {
                            success: function (result) {
                                if (result.success) {
                                    var message = result.message;
                                    var flashMessages = Array();
                                    if (message) {
                                        flashMessages.push(message);
                                    }

                                    if (successCallback) {
                                        successCallback();
                                    }
                                    else {
                                        sessionStorage.setItem('messages', JSON.stringify(flashMessages));
                                        if (result.redirect) {
                                            window.location.href = result.redirect;
                                        } else {
                                            window.location.reload();
                                        }
                                    }
                                }
                                else {
                                    setupModalActionModal(result);
                                }
                            }
                        });
                }
            });


            function appendHidenInput($form, name, value) {
                var inputTag = $('<input />');
                inputTag.hide();
                inputTag.prop('value', value);
                inputTag.prop('name', name);
                $form.append(inputTag);
            }

            if ($('.dropzone').length > 0) {
                var dictDefaultMessage = $('.dropzone').data('dropzone-dictdefaultmessage');
                var myDropzone = new Dropzone(".dropzone", { dictDefaultMessage: dictDefaultMessage, addRemoveLinks: true, maxFiles: 1 });

                var responseDate;
                myDropzone.on("success", function (data) {
                    var responseObj = jQuery.parseJSON(data.xhr.response);
                    var iTag = $('<i></i>');
                    iTag.css({ "margin-left": "25px", "margin-top": "20px" });

                    iTag.addClass(responseObj.data.className);
                    $('.dz-image').html(iTag);
                    $('.dz-image img').hide();
                    $('#Title').val(data.name);

                    $('form input[name="FileNameInDisk"]').val(responseObj.data.fileNameInDisk);
                    $('form input[name="ContentType"]').val(responseObj.data.contentType);

                    responseDate = responseObj.data;
                });

                myDropzone.on("removedfile", function (data) {
                    $('form input[name="FileNameInDisk"]').val('');
                    $('form input[name="ContentType"]').val('');
                });

                var fileNameInDisk = $('form input[name="FileNameInDisk"]').val();
                var fileName = $('form input[name="FileName"]').val();
                var fileSize = $('form input[name="FileSize"]').val();
                var $fileIconTag = $($('form input[name="iconTag"]').val());

                if (fileNameInDisk) {
                    var mockFile = {
                        name: fileName, size: fileSize
                    };


                    // Call the default addedfile event handler
                    myDropzone.emit("addedfile", mockFile);
                    myDropzone.emit("thumbnail", mockFile, fileSize);

                    // Make sure that there is no progress bar, etc...
                    myDropzone.emit("complete", mockFile);

                    $('.dz-image').html($fileIconTag);
                }
            }
        }
    }


    return {
        initialize: function () {
            handleModalActions();
        },
        handleEntityAction: function (href, successCallback, cancelCallbak) {
            handleEntityAction(href, successCallback, cancelCallbak);
        }
    };
}();