﻿using Wandio.Web.Mvc.Html;

namespace Wandio.Web.Mvc
{
    public class EntityAction
    {
        public EntityAction()
        {
        }

        public EntityAction(string title, string href, string icon = null, BootstrapContext? backgroundContext = null, string group = null, bool isCustom = false, string customActionName = null)
        {
            Title = title;
            Href = href;
            Icon = icon;
            BootstrapContext = backgroundContext;
            Group = group;
            IsCustomAction = isCustom;
            CustomActionName = customActionName;
        }

        public string Title { get; set; }

        public BootstrapContext? BootstrapContext { get; set; }

        public string Href { get; set; }

        public string Icon { get; set; }

        public string Group { get; set; }

        public bool IsCustomAction { get; set; }

        public string CustomActionName { get; set; }

        public string Target { get; set; }
    }
}
