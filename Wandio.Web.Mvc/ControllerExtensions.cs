﻿/*
using Microsoft.AspNetCore.Mvc;

namespace Wandio.Web.Mvc
{
public static class ControllerExtensions
{
    public static JsonResult JsonSuccess(this Controller controller, string message = null, string redirect = null, object data = null)
        => controller.Json(new { success = true, message, redirect, data });

    public static JsonResult JsonFailure(this Controller controller, string message = null, string redirect = null, object data = null)
        => controller.Json(new { success = false, message, redirect, data });

    public static JsonResult JsonResult(this Controller controller, bool success = true, string message = null, string redirect = null, object data = null)
        => controller.Json(new { success, message, redirect, data });


            public static void EnsureResourceFound(this Controller controller, object resource)
            {
                if (resource == null)
                {
                    throw new HttpException((int)System.Net.HttpStatusCode.NotFound, System.Net.HttpStatusCode.NotFound.ToString());
                }
            }

            /// <summary>
            /// Renders _ConfirmationModal view, generally bootstrap modal with confirmation text, yes and cancel
            /// </summary>
            /// <param name="confirmationText">text to be rendered. May contain html</param>
            /// <returns></returns>
            public static PartialViewResult ConfirmationModalView(this Controller controller, string confirmationText)
            {
                var result = new PartialViewResult()
                {
                    ViewName = "_ConfirmationModal",
                    ViewData = new ViewDataDictionary
                    {
                        Model = confirmationText
                    }
                };
                return result;
            }

            /// <summary>
            /// Renders _ErrorModal view, generally bootstrap modal with error list and ok button
            /// </summary>
            /// <param name="errors">errors to be rendered</param>
            /// <returns></returns>
            public static PartialViewResult ErrorModalView(this Controller controller, IEnumerable<string> errors)
            {
                var result = new PartialViewResult()
                {
                    ViewName = "_ErrorModal",
                    ViewData = new ViewDataDictionary
                    {
                        Model = errors
                    }
                };
                return result;
            }

            public static string RenderViewToString(this Controller controller, string viewName, object model = null)
            {
                ViewDataDictionary viewData = new ViewDataDictionary();
                viewData.Model = model;
                using (var sw = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext,
                                                                             viewName);
                    var viewContext = new ViewContext(controller.ControllerContext, viewResult.View,
                                                 viewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                    return sw.GetStringBuilder().ToString();
                }
            }
    }
}
    */
