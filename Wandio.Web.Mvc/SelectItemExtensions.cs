﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Wandio.Common.Extensions;

namespace Wandio.Web.Mvc
{
    public static class SelectItemExtensions
    {
        public enum LocalizationKeyForEnumSelectionType
        {
            Value = 1,
            EnumValue = 2,
            EnumUnderscoreValue = 3,
        }

        public static void AddModelErrors(this ModelStateDictionary modelState, IEnumerable<string> errors)
        {
            if (errors != null && errors.Any())
            {
                foreach (var error in errors)
                {
                    modelState.AddModelError(string.Empty, error);
                }
            }
        }

        public static IEnumerable<SelectListItem> YesNoSelectList(string yesText = "Yes", string noText = "No", bool prependNull = true)
        {
            var selectList = new List<SelectListItem>
            {
                new SelectListItem { Text = yesText, Value = bool.TrueString },
                new SelectListItem { Text = noText, Value = bool.FalseString },
            };

            return prependNull ? selectList.PrependNull() : selectList;
        }

        public static IEnumerable<SelectListItem> ToSelectListItems<T>(
            this IEnumerable<T> items,
            Func<T, string> localizerKeySelector,
            Func<T, string> valueSelector,
            string defaultText = null,
            string selectedId = null,
            Func<T, string> getId = null)
        {
            IEnumerable<SelectListItem> itemsList = new List<SelectListItem>();

            if (defaultText != null)
            {
                itemsList = new[] { new SelectListItem { Text = defaultText, Value = string.Empty } };
            }

            if (items == null)
            {
                return itemsList;
            }

            itemsList = itemsList.Concat(items.Select(item =>
                new SelectListItem
                {
                    Selected = selectedId != null ? getId(item) == selectedId : false,
                    Text = localizerKeySelector(item),
                    Value = valueSelector(item),
                }));

            return itemsList;
        }

        public static string Localize(
            this bool what,
            IStringLocalizer localizer,
            string yesKey = "Yes",
            string noKey = "No")
            => what ? localizer[yesKey] : localizer[noKey];

        public static string LocalizeEnum<T>(
            this T what,
            IStringLocalizer localizer,
            LocalizationKeyForEnumSelectionType keySelectionType = LocalizationKeyForEnumSelectionType.EnumValue)
            where T : struct, IConvertible
        {
            string key;
            switch (keySelectionType)
            {
                case LocalizationKeyForEnumSelectionType.EnumUnderscoreValue:
                    key = $"{what.GetType().Name}_{what}";
                    break;
                case LocalizationKeyForEnumSelectionType.Value:
                    key = $"{what}";
                    break;
                case LocalizationKeyForEnumSelectionType.EnumValue:
                    key = $"{what.GetType().Name}{what}";
                    break;
                default:
                    throw new InvalidOperationException(
                        $"Invalid LocalizationKeyForEnumSelectionType : {keySelectionType}");
            }

            return localizer[key];
        }

        public static IEnumerable<SelectListItem> ToSelectListItems<T>(
            IStringLocalizer localizer,
            LocalizationKeyForEnumSelectionType keySelectionType,
            bool prependNull = false)
            where T : struct, IConvertible
            => ToSelectListItems<T>(it => LocalizeEnum(it, localizer, keySelectionType), prependNull);

        public static IEnumerable<SelectListItem> ToSelectListItems<T>(
            Func<T, string> enumLocalizer = null,
            bool prependNull = false,
            string nullText = "")
            where T : struct, IConvertible
        {
            if (!typeof(T).GetTypeInfo().IsEnum)
            {
                throw new InvalidOperationException($"{typeof(T).Name} must be enum");
            }

            var result = EnumExtensions
                .ListValues<T>()
                .Select(it =>
                {
                    var localizedName = enumLocalizer?.Invoke(it) ?? it.ToString();
                    var value = Convert.ToInt32(Enum.Parse(typeof(T), it.ToString()) as Enum);
                    return new SelectListItem
                    {
                        Text = localizedName,
                        Value = value.ToString(),
                    };
                });

            return prependNull ? result.PrependNull() : result;
        }

        private static IEnumerable<T> PrependNull<T>(this IEnumerable<T> source, string nullText = "")
            where T : SelectListItem, new()
        {
            var first = new T[1];
            var item = new T
            {
                Text = nullText,
                Value = string.Empty,
            };
            first[0] = item;
            return first.Concat(source);
        }
    }
}