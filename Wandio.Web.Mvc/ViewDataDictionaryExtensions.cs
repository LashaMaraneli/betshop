﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Wandio.Web.Mvc
{
    public static class ViewDataDictionaryExtensions
    {
        public static ViewDataDictionary CreateViewDataDictionary<T>(this T model, IModelMetadataProvider metadataProvider)
            => new ViewDataDictionary<T>(metadataProvider, new ModelStateDictionary()) { Model = model };
    }
}