﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using static Wandio.Common.Extensions.StringExtensions;

namespace Wandio.Web.Mvc
{
    public static class StringExtensions
    {
        public static string TrimController(this string on)
            => on.TrimEnd(nameof(Controller));

        public static string TrimViewComponent(this string on)
            => on.TrimEnd(nameof(ViewComponent));

        public static IHtmlContent ToHtmlContent(this string what)
            => new HtmlContentBuilder().Append(what);
    }
}
