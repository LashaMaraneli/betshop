﻿namespace Wandio.Web.Mvc
{
    public static class EntityActionsExtension
    {
        public static void AddAction(
            this EntityActions src,
            string url,
            string title)
            => src.Add(new EntityAction { Href = url, Title = title });
    }
}