﻿using Microsoft.AspNetCore.Html;

namespace Wandio.Web.Mvc
{
    public static class HtmlStringExtensions
    {
        public static HtmlString WriteIf(this string value, bool condition)
            => condition ? new HtmlString(value) : HtmlString.Empty;
    }
}
