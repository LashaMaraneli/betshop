﻿// using Newtonsoft.Json;
// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Text;
// using Microsoft.AspNetCore.Mvc;
// using Microsoft.Net.Http.Headers;
//
// namespace Wandio.Web.Mvc
// {
//    public class JsonNetResult : ActionResult
//    {
//        public Encoding ContentEncoding { get; set; }
//        public string ContentType { get; set; }
//        public object Data { get; set; }
//
//        public JsonSerializerSettings SerializerSettings { get; set; }
//        public Formatting Formatting { get; set; }
//
//        public JsonNetResult()
//        {
//            SerializerSettings = new JsonSerializerSettings();
//        }
//
//        //        public override void ExecuteResult(ActionContext context)
//        //        {
//        //            base.ExecuteResult(context);
//        //        }
//
//        public override void ExecuteResult(ActionContext context)
//        {
//            if (context == null)
//                throw new ArgumentNullException("context");
//
//            var response = context.HttpContext.Response;
//
//            response.ContentType = !string.IsNullOrEmpty(ContentType)
//              ? ContentType
//              : "application/json";
//
//            if (ContentEncoding != null)
//                //response.ContentEncoding = ContentEncoding;
//                response.ContentType = new MediaTypeHeaderValue("application/json")
//                {
//                    Encoding = ContentEncoding
//                }
//                .ToString();
//
//            if (Data != null)
//            {
//                JsonTextWriter writer = new JsonTextWriter(response.Output) { Formatting = Formatting };
//                JsonSerializer serializer = JsonSerializer.Create(SerializerSettings);
//                serializer.Serialize(writer, Data);
//
//                writer.Flush();
//            }
//        }
//    }
// }