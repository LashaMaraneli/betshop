﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Wandio.Web.Mvc.TagHelpers
{
    [HtmlTargetElement(Attributes = "show-if")]
    public class ShowIfTagHelper : TagHelper
    {
        public bool ShowIf { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
            => Show(output);

        public override Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
            => Task.Run(() => { Show(output); });

        private void Show(TagHelperOutput output)
        {
            if (!ShowIf)
            {
                output.SuppressOutput();
            }
        }
    }

    [HtmlTargetElement("select", Attributes = "asp-disabled")]
    public class SelectDisableHelper : TagHelper
    {
        public class SelectTagHelper : TagHelper
        {
            private const string DisabledAttributeName = "asp-disabled";

            [HtmlAttributeName(DisabledAttributeName)]
            public bool Disabled { get; set; }

            public override void Process(TagHelperContext context, TagHelperOutput output)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }

                if (output == null)
                {
                    throw new ArgumentNullException(nameof(output));
                }

                if (Disabled)
                {
                    output.Attributes.SetAttribute("disabled", null);
                }
            }
        }
    }
}