﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Wandio.Web.Mvc.TagHelpers
{
    [HtmlTargetElement(Attributes = "hide-if")]
    public class HideIfTagHelper : TagHelper
    {
        private bool HideIf { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
            => Show(output);

        public override Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
            => Task.Run(() => { Show(output); });

        private void Show(TagHelperOutput output)
        {
            if (!HideIf)
            {
                output.SuppressOutput();
            }
        }
    }
}