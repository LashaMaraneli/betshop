﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Wandio.Web.Mvc.TagHelpers
{
    [HtmlTargetElement(Attributes = "write-if")]
    public class WriteIfTagHelper : TagHelper
    {
        public bool WriteIf { get; set; }

        public override Task ProcessAsync(TagHelperContext context, TagHelperOutput output) => Task.Run(() => { Write(output); });

        public override void Process(TagHelperContext context, TagHelperOutput output) => Write(output);

        public void Write(TagHelperOutput output)
        {
            if (!WriteIf)
            {
                output.SuppressOutput();
            }
        }
    }
}
