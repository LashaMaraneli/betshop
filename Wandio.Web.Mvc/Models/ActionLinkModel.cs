﻿namespace Wandio.Web.Mvc.Models
{
    public class ActionLinkModel
    {
        public string Title { get; set; }

        public string Href { get; set; }
    }
}
