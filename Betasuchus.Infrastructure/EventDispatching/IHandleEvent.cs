﻿using Betasuchus.Shared;
using Microsoft.EntityFrameworkCore;

namespace Betasuchus.Infrastructure.EventDispatching
{
    public interface IHandleEvent<in T>
        where T : DomainEvent
    {
        void Handle(T @event, DbContext db);
    }
}
