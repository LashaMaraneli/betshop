﻿using Betasuchus.Shared;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Betasuchus.Infrastructure.EventDispatching
{
    public class InternalDomainEventDispatcher : IInternalEventDispatcher<DomainEvent>
    {
        private readonly IDictionary<Type, List<Type>> eventhandlerMaps = new Dictionary<Type, List<Type>>();
        private readonly IServiceProvider _serviceProvider;

        public InternalDomainEventDispatcher(IServiceProvider serviceProvider, Assembly domainEventsAssembly,
            params Assembly[] eventHandlersAssemblies)
        {
            eventhandlerMaps = EventHandlerMapping.DomainEventHandlerMapping<IHandleEvent<DomainEvent>>(domainEventsAssembly, eventHandlersAssemblies);
            _serviceProvider = serviceProvider;
        }

        public void Dispatch(IReadOnlyList<DomainEvent> events, DbContext dbContext)
        {
            Log.Information("InternalDomainEventDispatcher.Dispatch started");

            foreach (var item in events)
            {
                this.Dispatch(item, dbContext);
            }
        }

        private void Dispatch(dynamic evnt, DbContext dbContext)
        {
            Log.Information("InternalDomainEventDispatcher.Dispatch started");

            var type = evnt.GetType();
            if (!eventhandlerMaps.ContainsKey(type))
            {
                return;
            }

            var @eventHandlers = eventhandlerMaps[type];
            foreach (var handler in @eventHandlers)
            {
                var domainEventHandler = _serviceProvider.GetService(handler);

                if (domainEventHandler == null)
                {
                    domainEventHandler = InstatiateHandler(handler);
                }

                var handlerInstance = domainEventHandler as dynamic;
                handlerInstance.Handle(evnt, dbContext);
            }
        }

        private object InstatiateHandler(Type type)
        {
            Log.Information("InternalDomainEventDispatcher.InstatiateHandler started");

            var ctor = type.GetConstructors(BindingFlags.Instance | BindingFlags.Public)[0];
            var parameters = ctor.GetParameters();

            var handlerArgs = new object[parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                handlerArgs[i] = _serviceProvider.GetService(parameters[i].ParameterType);
            }

            return Activator.CreateInstance(type, handlerArgs);
        }
    }
}
