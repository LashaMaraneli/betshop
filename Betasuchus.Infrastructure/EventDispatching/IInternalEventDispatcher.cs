﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Betasuchus.Infrastructure.EventDispatching
{
    public interface IInternalEventDispatcher<TDomainEvent>
    {
        void Dispatch(IReadOnlyList<TDomainEvent> domainEvents, DbContext dbContext);
    }
}
