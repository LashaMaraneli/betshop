﻿using Betasuchus.Domain.CategoryManagement;
using Betasuchus.Domain.CategoryManagement.Events;
using Betasuchus.Domain.OfferManagement;
using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.ProviderManagement;
using Betasuchus.Domain.OrderManagement;
using Betasuchus.Domain.UserManagement;
using Betasuchus.Shared;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Reflection;
using Betasuchus.Domain.OperationManagement;

namespace Betasuchus.Infrastructure.Db
{
    public class BetasuchusDbContext : DbContext
    {
        public BetasuchusDbContext(DbContextOptions<BetasuchusDbContext> options)
            : base(options)
        {
        }

        public BetasuchusDbContext(string connectionString)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserClaim>().ToTable("UserClaims").HasKey(x => x.Id);
            builder.Entity<UserLogin>().ToTable("UserLogins").HasKey(x => x.Id);
            builder.Entity<UserRole>().ToTable("UserRoles").HasKey(x => new { x.UserId, x.RoleId });
            builder.Entity<Role>().ToTable("Roles").HasKey(x => x.Id);
            builder.Entity<User>().ToTable("Users").HasKey(x => x.Id);
            builder.Entity<UserToken>().ToTable("UserTokens").HasKey(x => x.Id);
            builder.Entity<RoleClaim>().ToTable("RoleClaims").HasKey(x => x.Id);

            builder.Entity<Category>().OwnsOne(t => t.Name);
            builder.Entity<Provider>().OwnsOne(t => t.Name);

            builder.Entity<Offer>().OwnsOne(t => t.OfferText);
            builder.Entity<ExtendedOffer>();
            builder.Entity<SlotOffer>().OwnsOne(t => t.GameName);
            builder.Entity<Sport>();
            builder.Entity<EGT>();
            builder.Entity<PlayNGo>();
            builder.Entity<Poker>();
            builder.Entity<TableGame>();

            builder.Entity<Order>().HasKey(t => t.Id);
            builder.Entity<Operation>().HasKey(o => o.Id);

            AddDomainEventImplementations(builder);
        }

        private void AddDomainEventImplementations(ModelBuilder builder)
        {
            var domainEventType = typeof(DomainEvent);
            var eventTypes = Assembly.GetAssembly(typeof(CategoryCreated)).GetTypes()
                .Where(t => !t.IsAbstract && t.IsClass && domainEventType.IsAssignableFrom(t)).ToList();

            builder.Entity(domainEventType);

            foreach (var eventType in eventTypes)
            {
                builder.Entity(eventType);
            }
        }
    }
}
