﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Infrastructure.Db
{
    public class BetasuchusDbContextFactory : IDesignTimeDbContextFactory<BetasuchusDbContext>
    {
        public BetasuchusDbContext CreateDbContext(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($@"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json")
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<BetasuchusDbContext>();
            optionsBuilder.UseNpgsql(config.GetConnectionString(nameof(BetasuchusDbContext)));

            return new BetasuchusDbContext(optionsBuilder.Options);
        }
    }
}
