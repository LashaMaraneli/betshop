﻿using Betasuchus.Infrastructure.EventDispatching;
using Betasuchus.Shared;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Infrastructure.Db
{
    public class UnitOfWork
    {
        private readonly BetasuchusDbContext _db;
        private readonly InternalDomainEventDispatcher _internalDomainEventDispatcher;

        public UnitOfWork(BetasuchusDbContext db, InternalDomainEventDispatcher internalDomainEventDispatcher)
        {
            _db = db;
            _internalDomainEventDispatcher = internalDomainEventDispatcher;
        }

        public async Task Save()
        {
            Log.Information("UnitOfWork.Save start");

            using var transaction = await _db.Database.BeginTransactionAsync();

            var modifiedEntries = _db.ChangeTracker.Entries<IHasDomainEvents>().ToList();

            foreach (var entry in modifiedEntries)
            {
                var events = entry.Entity.UncommittedChanges();
                if (!events.Any())
                {
                    continue;
                }

                _internalDomainEventDispatcher.Dispatch(events, _db);

                _db.Set<DomainEvent>().AddRange(events);

                entry.Entity.MarkChangesAsCommitted();
            }

            await _db.SaveChangesAsync();

            await transaction.CommitAsync();

            Log.Information("UnitOfWork.Save finish");
        }
    }
}
