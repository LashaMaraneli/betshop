﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static bool NotNullOrEmpty(this string str)
        {
            return !string.IsNullOrEmpty(str);
        }
    }
}
