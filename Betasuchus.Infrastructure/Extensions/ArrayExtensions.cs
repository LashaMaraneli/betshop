﻿using System;
using System.Collections.Generic;

namespace Betasuchus.Infrastructure.Extensions
{
    public static class ArrayExtensions
    {
        public static IList<T> Add<T>(this IList<T> list, T obj)
        {
            list.Add(obj);
            return list;
        }

        public static IList<T> AddIf<T>(this IList<T> list, Func<bool> expression, T obj)
        {
            if (expression.Invoke())
            {
                list.Add(obj);
            }

            return list;
        }

        public static IList<T> AddIf<T>(this IList<T> list, bool condition, T obj)
        {
            if (condition)
            {
                list.Add(obj);
            }

            return list;
        }
    }
}
