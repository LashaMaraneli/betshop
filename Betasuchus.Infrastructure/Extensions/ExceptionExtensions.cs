﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Betasuchus.Infrastructure.Extensions
{
    public static class ExceptionExtensions
    {
        public static IEnumerable<TSource> FromHierarchy<TSource>(this TSource source, Func<TSource, TSource> nextItem, Func<TSource, bool> canContinue)
        {
            for (var current = source; canContinue(current); current = nextItem(current))
            {
                yield return current;
            }
        }

        public static IEnumerable<TSource> FromHierarchy<TSource>(this TSource source, Func<TSource, TSource> nextItem)
            where TSource : class
        {
            return FromHierarchy(source, nextItem, s => s != null);
        }

        public static string GetAllExceptionMessages(this Exception exception)
        {
            var messages =
                exception
                    .FromHierarchy(ex => ex.InnerException)
                    .Select(ex => $"{ex.Message} {Environment.NewLine} {ex.StackTrace}");

            var exceptionSeparator = $"{Environment.NewLine} {new string('=', 150)} {Environment.NewLine}";

            return string.Join(exceptionSeparator, messages);
        }
    }
}
