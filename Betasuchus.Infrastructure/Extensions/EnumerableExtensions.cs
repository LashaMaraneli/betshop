﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Betasuchus.Infrastructure.Extensions
{
    public static class EnumerableExtensions
    {
        public static bool IsNullOrEmpty(this string[] str)
        {
            return str != null && str.Length > 0;
        }
    }
}
