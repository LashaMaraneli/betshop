﻿using PhoneNumbers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Infrastructure.Extensions
{
    public static class Extensions
    {
        public static bool ValidatePhoneNumber(this string phoneNumber)
        {
            var phoneNumberUtil = PhoneNumberUtil.GetInstance();

            try
            {
                var parsedPhoneNumber = phoneNumberUtil.Parse(phoneNumber, "GE");

                return phoneNumberUtil.IsValidNumber(parsedPhoneNumber);
            }
            catch
            {
            }

            return false;
        }

        public static DateTime UnixDateToDateTime(this long unixDate)
        {
            return DateTimeOffset.FromUnixTimeMilliseconds(unixDate).DateTime;
        }

        public static DateTime? UnixDateToDateTime(this long? unixDate)
        {
            if (!unixDate.HasValue)
            {
                return null;
            }

            return UnixDateToDateTime(unixDate.Value);
        }

        public static long DateTimeToUnixDate(this DateTime dateTime)
        {
            var epoch = dateTime - new DateTime(1970, 1, 1, 0, 0, 0);

            return (long)epoch.TotalMilliseconds;
        }

        public static long? DateTimeToUnixDate(this DateTime? dateTime)
        {
            if (!dateTime.HasValue)
            {
                return null;
            }

            return DateTimeToUnixDate(dateTime.Value);
        }
    }
}
