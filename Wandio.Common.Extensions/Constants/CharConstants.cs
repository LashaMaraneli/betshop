﻿// <copyright file="CharConstants.cs" company="Wandio">
// Copyright (c) Wandio. All rights reserved.
// </copyright>

namespace Wandio.Common.Constants
{
    /// <summary>
    /// Char constants.
    /// </summary>
    public class CharConstants
    {
        /// <summary>
        /// Whitespace constant.
        /// </summary>
        public const char WhiteSpace = ' ';
    }
}