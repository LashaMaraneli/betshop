﻿// <copyright file="TargetConstants.cs" company="Wandio">
// Copyright (c) Wandio. All rights reserved.
// </copyright>

namespace Wandio.Common.Constants
{
    /// <summary>
    /// Target constants.
    /// </summary>
    public static class TargetConstants
    {
        /// <summary>
        /// Gets or sets Blank.
        /// </summary>
        public static string Blank { get; set; } = "_blank";

        /// <summary>
        /// Gets or sets Self.
        /// </summary>
        public static string Self { get; set; } = "_self";

        /// <summary>
        /// Gets or sets Parent.
        /// </summary>
        public static string Parent { get; set; } = "_parent";

        /// <summary>
        /// Gets or sets Top.
        /// </summary>
        public static string Top { get; set; } = "_top";
    }
}
