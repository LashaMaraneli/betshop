﻿// <copyright file="StringConstants.cs" company="Wandio">
// Copyright (c) Wandio. All rights reserved.
// </copyright>

namespace Wandio.Common.Constants
{
    /// <summary>
    /// String constants.
    /// </summary>
    public static class StringConstants
    {
        /// <summary>
        /// Whitespace.
        /// </summary>
        public const string WhiteSpace = " ";
    }
}