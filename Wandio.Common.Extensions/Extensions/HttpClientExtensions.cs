﻿// <copyright file="HttpClientExtensions.cs" company="Wandio">
// Copyright (c) Wandio. All rights reserved.
// </copyright>

using System.Net.Http;

namespace Wandio.Common.Extensions
{
    /// <summary>
    /// HttpClient Extensions.
    /// </summary>
    public static class HttpClientExtensions
    {
        /// <summary>
        /// Tries post provided number of times, default is 3.
        /// </summary>
        /// <param name="client">Http client.</param>
        /// <param name="requestUrl">Request uri.</param>
        /// <param name="tryTime">Try times.</param>
        /// <returns>Returns HttpResponseMessage.</returns>
        public static HttpResponseMessage TryPost(this HttpClient client, string requestUrl, int tryTime = 3)
        {
            var httpResponseMessage = new HttpResponseMessage();
            for (var i = 0; i < tryTime; i++)
            {
                try
                {
                    httpResponseMessage = client.PostAsync(requestUrl, new StringContent(string.Empty)).GetAwaiter().GetResult();
                    httpResponseMessage.EnsureSuccessStatusCode();
                    break;
                }
                catch
                {
                    if (i == tryTime - 1)
                    {
                        throw;
                    }
                }
            }

            return httpResponseMessage;
        }

        /// <summary>
        /// Tries post provided number of times, default is 3.
        /// </summary>
        /// <param name="client">Http client.</param>
        /// <param name="requestUrl">Request uri.</param>
        /// <param name="content">Content to pass.</param>
        /// <param name="tryTime">Try times.</param>
        /// <returns>Returns HttpResponseMessage.</returns>
        public static HttpResponseMessage TryPost(this HttpClient client, string requestUrl, HttpContent content, int tryTime = 3)
        {
            var httpResponseMessage = new HttpResponseMessage();
            for (var i = 0; i < tryTime; i++)
            {
                try
                {
                    httpResponseMessage = client.PostAsync(requestUrl, content).GetAwaiter().GetResult();
                    httpResponseMessage.EnsureSuccessStatusCode();
                    break;
                }
                catch
                {
                    if (i == tryTime - 1)
                    {
                        throw;
                    }
                }
            }

            return httpResponseMessage;
        }

        /// <summary>
        /// Tries Get provided number of times, default is 3.
        /// </summary>
        /// <param name="client">Http client.</param>
        /// <param name="requestUrl">Request uri.</param>
        /// <param name="tryTime">Try times.</param>
        /// <returns>Returns HttpResponseMessage.</returns>
        public static HttpResponseMessage TryGet(this HttpClient client, string requestUrl, int tryTime = 3)
        {
            var httpResponseMessage = new HttpResponseMessage();
            for (var i = 0; i < tryTime; i++)
            {
                try
                {
                    httpResponseMessage = client.GetAsync(requestUrl).GetAwaiter().GetResult();
                    httpResponseMessage.EnsureSuccessStatusCode();
                    break;
                }
                catch
                {
                    if (i == tryTime - 1)
                    {
                        throw;
                    }
                }
            }

            return httpResponseMessage;
        }
    }
}
