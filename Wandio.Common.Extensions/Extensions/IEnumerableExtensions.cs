﻿// <copyright file="IEnumerableExtensions.cs" company="Wandio">
// Copyright (c) Wandio. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;

namespace Wandio.Common.Extensions
{
    /// <summary>
    /// Enumerable extensions for easing processing.
    /// </summary>
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// applies action to each element of the array.
        /// </summary>
        /// <typeparam name="T">T.</typeparam>
        /// <param name="source">source array.</param>
        /// <param name="action">action method.</param>
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var element in source)
            {
                action(element);
            }
        }

        /// <summary>
        /// Retrieves n'th element from an IEnumerable or default if one does not exist.
        /// </summary>
        /// <typeparam name="T">T.</typeparam>
        /// <param name="source">source array.</param>
        /// <param name="which">0 based index of element.</param>
        /// <returns>T value.</returns>
        public static T NthOrDefault<T>(this IEnumerable<T> source, int which)
        {
            if (which >= source.Count())
            {
                return default(T);
            }

            return source.Skip(which).First();
        }
    }
}