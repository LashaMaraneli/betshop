﻿// <copyright file="StringExtensions.cs" company="Wandio">
// Copyright (c) Wandio. All rights reserved.
// </copyright>

namespace Wandio.Common.Extensions
{
    /// <summary>
    /// String extensions.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Trims string end, which ends with concrete parameter.
        /// </summary>
        /// <param name="on">source string.</param>
        /// <param name="what">ends with.</param>
        /// <returns>trimed string.</returns>
        public static string TrimEnd(this string on, string what)
                   => on.EndsWith(what)
                       ? on.Substring(0, on.Length - what.Length)
                       : on;
    }
}