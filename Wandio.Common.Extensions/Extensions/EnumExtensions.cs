﻿// <copyright file="EnumExtensions.cs" company="Wandio">
// Copyright (c) Wandio. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Wandio.Common.Extensions
{
    /// <summary>
    /// Enum extensions.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Enum to list.
        /// </summary>
        /// <typeparam name="TEnum">Enum to parse.</typeparam>
        /// <returns>array of enum values.</returns>
        public static IEnumerable<TEnum> ListValues<TEnum>()
            where TEnum : struct, IConvertible
        {
            var enumType = typeof(TEnum);
            if (!enumType.GetTypeInfo().IsEnum)
            {
                throw new InvalidOperationException($"{enumType.Name} must be enum");
            }

            return Enum.GetValues(enumType).Cast<TEnum>();
        }

        /// <summary>
        /// enum to array.
        /// </summary>
        /// <typeparam name="TEnum">enum type.</typeparam>
        /// <param name="parameter">parameter.</param>
        /// <returns>array of enum values.</returns>
        public static IEnumerable<TEnum> ToEnumArray<TEnum>(this TEnum parameter)
            where TEnum : struct, IComparable
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }

            var values = Enum.GetValues(typeof(TEnum)).Cast<int>().ToList();
            values.Remove(0);
            var flag = Convert.ToInt32(parameter);

            return values.Where(x => (x & flag) == x).Cast<TEnum>().AsEnumerable();
        }

        /// <summary>
        /// Array to enum. sum of [Flag]Enum arrays.
        /// </summary>
        /// <typeparam name="TEnum">Type.</typeparam>
        /// <param name="selectedValues">Values.</param>
        /// <returns>Single sum enum.</returns>
        public static TEnum ToEnum<TEnum>(this IEnumerable<TEnum> selectedValues)
            where TEnum : struct, IComparable
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }

            int flags = 0;

            if (selectedValues != null)
            {
                var values = selectedValues.Cast<int>();
                foreach (var type in values)
                {
                    flags |= type;
                }
            }

            return (TEnum)(object)flags;
        }

        /// <summary>
        /// Remove flag from enum.
        /// </summary>
        /// <typeparam name="TEnum">Type.</typeparam>
        /// <param name="source">Source enum.</param>
        /// <param name="parameter">Parameter to remove.</param>
        /// <returns>Returns enum.</returns>
        public static TEnum RemoveFlag<TEnum>(this TEnum source, TEnum parameter)
            where TEnum : struct, IComparable
                => source.ToEnumArray().Where(x => !x.Equals(parameter)).ToEnum();

        /// <summary>
        /// tests if enum.value is in possible array.
        /// </summary>
        /// <typeparam name="TEnum">Type of enum.</typeparam>
        /// <param name="source">Source enum.</param>
        /// <param name="possibles">Possible values.</param>
        /// <returns>Returns boolean.</returns>
        public static bool IsIn<TEnum>(this TEnum source, params TEnum[] possibles)
            where TEnum : struct, IComparable
                => possibles.Contains(source);

        /// <summary>
        /// tests if [nullable]enum.value is in possible array.
        /// </summary>
        /// <typeparam name="TEnum">Type of enum.</typeparam>
        /// <param name="source">Source enum.</param>
        /// <param name="possibles">Possible values.</param>
        /// <returns>Returns boolean.</returns>
        public static bool IsIn<TEnum>(this TEnum? source, params TEnum[] possibles)
            where TEnum : struct, IComparable
                => source.HasValue ? source.Value.IsIn(possibles) : false;
    }
}