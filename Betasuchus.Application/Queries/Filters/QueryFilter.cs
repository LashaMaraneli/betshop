﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Resources;
using Betasuchus.Domain.OfferManagement;
using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.ProviderManagement;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Filters
{
    public class QueryFilter : Query<QueryFilterResult>
    {
        public long? CategoryId { get; set; }

        public override async Task<QueryExecutionResult<QueryFilterResult>> ExecuteAsync()
        {
            decimal minPrice = 0, maxPrice = 0;
            var localizer = GetService<IStringLocalizer<ResourceStrings>>();
            IEnumerable<QueryFilterResult.Provider> providers = Enumerable.Empty<QueryFilterResult.Provider>();

            var gameTypes = Enum.GetValues(typeof(GameType)).Cast<GameType>().Select(x => new QueryFilterResult.GameTypeItem
            {
                Text = localizer[x.ToString()],
                Id = (int)x
            });

            var offers = _db.Set<Offer>().Where(x => x.ExpireDate > DateTime.Now)
                .Where(x => CategoryId.HasValue && x.CategoryId == CategoryId);

            if (offers.Any())
            {
                minPrice = offers.Min(x => x.DiscountedPrice);
                maxPrice = offers.Max(x => x.DiscountedPrice);

                providers = offers.ToList()
                    .Where(x => CategoryId.HasValue && x is SlotOffer)
                    .Select(x =>
                    {
                        var slot = x as SlotOffer;

                        return new QueryFilterResult.Provider
                        {
                            Id = slot.ProviderId.Value,
                            Name = JsonConvert.DeserializeObject<Provider>(slot.Provider,
                            new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None }).Name.Text
                        };
                    });
            }

            return await OkAsync(new QueryFilterResult
            {
                GameTypes = gameTypes,
                Providers = providers,
                Price = new QueryFilterResult.PriceItem
                {
                    MaxPrice = maxPrice,
                    MinPrice = minPrice
                }
            });
        }
    }

    public class QueryFilterResult
    {
        public IEnumerable<GameTypeItem> GameTypes { get; set; }

        public PriceItem Price { get; set; }

        public IEnumerable<Provider> Providers { get; set; }

        public class GameTypeItem
        {
            public int Id { get; set; }

            public string Text { get; set; }
        }

        public class PriceItem
        {
            public decimal MinPrice { get; set; }

            public decimal MaxPrice { get; set; }
        }

        public class Provider
        {
            public long Id { get; set; }

            public string Name { get; set; }
        }
    }
}
