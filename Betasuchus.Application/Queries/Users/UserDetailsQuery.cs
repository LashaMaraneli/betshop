﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.UserManagement;
using Serilog;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Users
{
    public class UserDetailsQuery : Query<UserDetailsQueryResult>
    {
        public string UserName { get; set; }

        public override async Task<QueryExecutionResult<UserDetailsQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var user = _db.Set<User>().FirstOrDefault(x => x.UserName == UserName);

            if (user == null)
            {
                return await FailAsync();
            }

            return await OkAsync(new UserDetailsQueryResult
            {
                UserName = user.UserName,
                Email = user.Email
            });
        }
    }

    public class UserDetailsQueryResult
    {
        public string UserName { get; set; }

        public string Email { get; set; }
    }
}
