﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.UserManagement;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Users
{
    public class UsersQuery : PagedQuery<UsersQueryResult>
    {
        public override async Task<QueryExecutionResult<UsersQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var query = _db.Set<User>().Where(x => !x.DeleteDate.HasValue);

            var users = await query
                .Select(x => new UsersQueryResult.UserItem
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    PhoneNumber = x.PhoneNumber,
                    UserName = x.UserName
                }).SortAndPage(this).ToListAsync();

            return await OkAsync(new UsersQueryResult
            {
                Users = users,
                TotalCount = await query.CountAsync()
            });
        }
    }

    public class UsersQueryResult : PagedQueryResult
    {
        public IList<UserItem> Users { get; set; }

        public class UserItem
        {
            public long Id { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string UserName { get; set; }

            public string Email { get; set; }

            public string PhoneNumber { get; set; }
        }
    }
}
