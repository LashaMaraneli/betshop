﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.UserManagement;
using Microsoft.AspNetCore.Identity;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Users
{
    public class UserQuery : Query<UserQueryResult>
    {
        public long Id { get; set; }

        public override async Task<QueryExecutionResult<UserQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var userManager = GetService<UserManager<User>>();

            var user = await userManager.FindByIdAsync(Id.ToString());

            var userItem = new UserQueryResult
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                Role = userManager.GetRolesAsync(user).GetAwaiter().GetResult().FirstOrDefault(),
                BirthDate = user.BirthDate
            };

            return await OkAsync(userItem);
        }
    }

    public class UserQueryResult
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Role { get; set; }

        public DateTime? BirthDate { get; set; }
    }
}
