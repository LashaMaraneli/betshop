﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.UserManagement;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Users
{
    public class RolesQuery : Query<RolesQueryResult>
    {
        public override async Task<QueryExecutionResult<RolesQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var roles = await _db.Set<Role>().Select(x => x.Name).ToListAsync();

            return await OkAsync(new RolesQueryResult
            {
                Roles = roles
            });
        }
    }

    public class RolesQueryResult
    {
        public IList<string> Roles { get; set; }
    }
}
