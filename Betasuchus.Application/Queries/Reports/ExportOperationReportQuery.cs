﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.OfferManagement;
using Betasuchus.Domain.OperationManagement;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Reports
{
    public class ExportOperationReportQuery : Query<ExportOperationReportQueryResult>
    {
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public override async Task<QueryExecutionResult<ExportOperationReportQueryResult>> ExecuteAsync()
        {
            var operations = _db.Set<Operation>().Where(x => x.CreateDate >= BeginDate && x.CreateDate <= EndDate)
                                .Select(x => new ExportOperationReportQueryResult.Operation
                                {
                                    Id = x.Id,
                                    BonusPoolId = x.BonusPoolId,
                                    CreateDate = x.CreateDate,
                                    Customer = x.Customer,
                                    CustomerId = x.CustomerId,
                                    OfferString = x.Offer,
                                    OperationStatus = x.Status,
                                    OrderId = x.OrderId,
                                    PayedAmount = x.PayedAmount,
                                    PaymentTransactionId = x.PaymentTransactionId
                                });

            return await OkAsync(new ExportOperationReportQueryResult
            {
                Operations = await operations.ToListAsync()
            });
        }
    }

    public class ExportOperationReportQueryResult
    {
        public List<Operation> Operations { get; set; }

        public class Operation
        {
            public long Id { get; set; }

            public string OfferString { get; set; }

            public OfferSlimModel Offer
                => JsonConvert.DeserializeObject<OfferSlimModel>(OfferString);

            public DateTime CreateDate { get; set; }

            public long CustomerId { get; set; }

            public string Customer { get; set; }

            public decimal PayedAmount { get; set; }

            public long OrderId { get; set; }

            public string BonusPoolId { get; set; }

            public string PaymentTransactionId { get; set; }

            public OperationStatus? OperationStatus { get; set; }
        }
    }
}
