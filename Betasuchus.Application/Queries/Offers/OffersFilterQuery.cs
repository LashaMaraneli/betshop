﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.OfferManagement;
using Betasuchus.Domain.OfferManagement.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Offers
{
    public class OffersFilterQuery : PagedQuery<OffersFilterQueryResult>
    {
        public decimal? MinAmount { get; set; }

        public decimal? MaxAmount { get; set; }

        public string Name { get; set; }

        public IEnumerable<long> Providers { get; set; }

        public IEnumerable<GameType> GameTypes { get; set; }

        public override async Task<QueryExecutionResult<OffersFilterQueryResult>> ExecuteAsync()
        {
            var offers = _db.Set<SlotOffer>().AsNoTracking().Where(x => x.ExpireDate >= DateTime.Now);

            if (MinAmount.HasValue)
            {
                offers = offers.Where(x => x.DiscountedPrice >= MinAmount);
            }

            if (MaxAmount.HasValue)
            {
                offers = offers.Where(x => x.DiscountedPrice <= MaxAmount);
            }

            if (!string.IsNullOrEmpty(Name))
            {
                offers = offers.Where(x => x.GameName.Text.Contains(Name));
            }

            if (Providers != null && Providers.Any())
            {
                offers = offers.Where(x => x.ProviderId.HasValue && Providers.Contains(x.ProviderId.Value));
            }

            if (GameTypes != null && GameTypes.Any())
            {
                offers = offers.Where(x => GameTypes.Contains(x.GameType));
            }

            var offersResponse = new OffersFilterQueryResult
            {
                Offers = await offers.Select(x => new OffersFilterQueryResult.Offer
                {
                    Amount = x.Amount,
                    Id = x.Id,
                    DiscountedPrice = x.DiscountedPrice,
                    Price = x.Price,
                    ThumbnailUrl = x.ThumbnailUrl,
                    OfferText = x.OfferText.Text,
                    GameType = x.GameType,
                    ExpireDate = x.ExpireDate,
                    Name = x.GameName.Text
                }).Page(this).ToListAsync(),
                TotalCount = await offers.CountAsync()
            };

            return await OkAsync(offersResponse);
        }
    }

    public class OffersFilterQueryResult : PagedQueryResult
    {
        public List<Offer> Offers { get; set; }

        public class Offer
        {
            public long Id { get; set; }

            public int Amount { get; set; }

            public decimal Price { get; set; }

            public decimal DiscountedPrice { get; set; }

            public string ThumbnailUrl { get; set; }

            public string OfferText { get; set; }

            public GameType GameType { get; set; }

            public DateTime? ExpireDate { get; set; }

            public string Name { get; set; }
        }
    }
}