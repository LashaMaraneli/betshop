﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.OfferManagement.Abstract;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Offers
{
    public class OffersQuery : PagedQuery<OffersQueryResult>
    {
        public bool Active { get; set; }

        public override async Task<QueryExecutionResult<OffersQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            IQueryable<Offer> offersQuery = _db.Set<Offer>();
            if (Active)
            {
                offersQuery = offersQuery.Where(x => x.ExpireDate >= DateTime.Now && !x.DeleteDate.HasValue);
            }
            else
            {
                offersQuery = offersQuery.Where(x => x.ExpireDate < DateTime.Now && !x.DeleteDate.HasValue);
            }

            var offers = offersQuery.Select(x => new OffersQueryResult.OfferItem
            {
                Id = x.Id,
                DiscountedPrice = x.DiscountedPrice,
                OfferText = x.OfferText.Georgian,
                Price = x.Price,
                ExpireDate = x.ExpireDate
            });

            return await OkAsync(new OffersQueryResult
            {
                Offers = offers.SortAndPage(this).ToList(),
                TotalCount = offers.Count()
            });
        }
    }

    public class OffersQueryResult : PagedQueryResult
    {
        public List<OfferItem> Offers { get; internal set; }

        public class OfferItem
        {
            public long Id { get; set; }
            public decimal DiscountedPrice { get; internal set; }
            public string OfferText { get; internal set; }
            public decimal Price { get; internal set; }
            public DateTime ExpireDate { get; internal set; }
            public long ProviderId { get; internal set; }
        }
    }
}
