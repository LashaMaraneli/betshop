﻿using Betasuchus.Application.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Offers
{
    public class QueryNavigation : Query<QueryNavigationResult>
    {
        public override async Task<QueryExecutionResult<QueryNavigationResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var categoriesQuery = await _db.Set<Domain.CategoryManagement.Category>().ToListAsync();
            var categories = categoriesQuery.Select(x => new QueryNavigationResult.Category
            {
                Id = x.Id,
                Name = x.Name.Text,
                ThumbnailUrl = x.ThumbnailUrl
            }).ToList();

            return await OkAsync(new QueryNavigationResult()
            {
                Categories = categories,
            });
        }
    }

    public class QueryNavigationResult
    {
        public IList<Category> Categories { get; set; }

        public class Category
        {
            public long Id { get; set; }

            public string Name { get; set; }

            public string ThumbnailUrl { get; set; }
        }
    }
}
