﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.ProviderManagement;
using Betasuchus.Shared;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Providers
{
    public class ProvidersSelectListQuery : Query<ProvidersSelectListQueryResult>
    {
        public override async Task<QueryExecutionResult<ProvidersSelectListQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var providers = await _db.Set<Provider>().AsNoTracking().Select(x => new ProvidersSelectListQueryResult.Provider
            {
                Id = x.Id,
                Name = x.Name
            }).ToListAsync();

            return await OkAsync(new ProvidersSelectListQueryResult
            {
                Providers = providers
            });
        }
    }

    public class ProvidersSelectListQueryResult
    {
        public IEnumerable<Provider> Providers { get; set; }

        public class Provider
        {
            public long Id { get; set; }

            public MultilanguageString Name { get; set; }
        }
    }
}
