﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.ProviderManagement;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Providers
{
    public class GetProvidersQuery : Query<GetProvidersQueryResult>
    {
        public override async Task<QueryExecutionResult<GetProvidersQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var providers = _db.Set<Provider>().ToList().Select(x => new GetProvidersQueryResult.ProviderItem
            {
                Id = x.Id,
                Name = x.Name.Text
            });

            return await OkAsync(new GetProvidersQueryResult { Providers = providers });
        }
    }

    public class GetProvidersQueryResult
    {
        public IEnumerable<ProviderItem> Providers { get; set; }

        public class ProviderItem
        {
            public long Id { get; set; }

            public string Name { get; set; }
        }
    }
}
