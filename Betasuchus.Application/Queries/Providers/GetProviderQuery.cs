﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.ProviderManagement;
using Betasuchus.Shared;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Providers
{
    public class GetProviderQuery : Query<GetProviderQueryResult>
    {
        public long Id { get; set; }

        public override async Task<QueryExecutionResult<GetProviderQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var provider = await _db.Set<Provider>().FindAsync(Id);

            if (provider != null)
            {
                return await OkAsync(new GetProviderQueryResult
                {
                    Provider = new GetProviderQueryResult.ProviderItem
                    {
                        Id = provider.Id,
                        Name = provider.Name
                    }
                });
            }

            return await FailAsync("Provider not found");
        }
    }

    public class GetProviderQueryResult
    {
        public ProviderItem Provider { get; set; }

        public class ProviderItem
        {
            public long Id { get; set; }

            public MultilanguageString Name { get; set; }
        }
    }
}
