﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.ProviderManagement;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Providers
{
    public class ProvidersQuery : PagedQuery<ProvidersQueryResult>
    {
        public override async Task<QueryExecutionResult<ProvidersQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var providers = await _db.Set<Provider>().AsNoTracking().Select(x => new ProvidersQueryResult.ProviderItem
            {
                Id = x.Id,
                English = x.Name.English,
                Georgian = x.Name.Georgian,
                Russian = x.Name.Russian,
                Turkish = x.Name.Turkish,
            }).ToListAsync();

            return await OkAsync(new ProvidersQueryResult()
            {
                Providers = providers,
                TotalCount = providers.Count
            });
        }
    }

    public class ProvidersQueryResult : PagedQueryResult
    {

        public IEnumerable<ProviderItem> Providers { get; set; }

        public class ProviderItem
        {
            public long Id { get; set; }

            public string Georgian { get; set; }

            public string English { get; set; }

            public string Turkish { get; set; }

            public string Russian { get; set; }

            public string CategoryName { get; set; }
        }
    }
}
