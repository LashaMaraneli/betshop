﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.CategoryManagement;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Categories
{
    public class CategoriesQuery : PagedQuery<CategoriesQueryResult>
    {
        public override async Task<QueryExecutionResult<CategoriesQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var categories = _db.Set<Category>().AsNoTracking().Select(x => new CategoriesQueryResult.Category()
            {
                Id = x.Id,
                Georgian = x.Name.Georgian,
                English = x.Name.English,
                Russian = x.Name.Russian,
                Turkish = x.Name.Turkish,
                ParentCategoryId = x.ParentCategoryId
            }).ToList();

            return await OkAsync(new CategoriesQueryResult()
            {
                Categories = categories,
                TotalCount = categories.Count
            });
        }
    }

    public class CategoriesQueryResult : PagedQueryResult
    {
        public IEnumerable<Category> Categories { get; set; }

        public class Category
        {
            public long Id { get; set; }

            public string Georgian { get; set; }

            public string English { get; set; }

            public string Turkish { get; set; }

            public string Russian { get; set; }

            public long? ParentCategoryId { get; set; }
        }
    }
}
