﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.CategoryManagement;
using Betasuchus.Shared;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Categories
{
    public class CategoriesSelectListQuery : Query<CategoriesSelectListQueryResult>
    {
        public override async Task<QueryExecutionResult<CategoriesSelectListQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            return await OkAsync(new CategoriesSelectListQueryResult()
            {
                Categories = _db.Set<Category>().AsNoTracking().Select(x => new CategoriesSelectListQueryResult.Category
                {
                    Id = x.Id,
                    Name = x.Name
                })
            });
        }
    }

    public class CategoriesSelectListQueryResult
    {
        public IEnumerable<Category> Categories { get; set; }

        public class Category
        {
            public long Id { get; set; }

            public MultilanguageString Name { get; set; }
        }
    }
}
