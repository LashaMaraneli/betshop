﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.CategoryManagement;
using Betasuchus.Shared;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.Categories
{
    public class GetCategoryQuery : Query<GetCategoryQueryResult>
    {
        public long Id { get; set; }

        public override async Task<QueryExecutionResult<GetCategoryQueryResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var category = _db.Set<Category>().Find(Id);

            if (category != null)
            {
                return await OkAsync(new GetCategoryQueryResult
                {
                    CategoryItem = new GetCategoryQueryResult.Category
                    {
                        Id = category.Id,
                        Name = category.Name,
                        ParentCategoryId = category.ParentCategoryId
                    }
                });
            }

            return await FailAsync("Category not found");
        }
    }

    public class GetCategoryQueryResult
    {
        public Category CategoryItem { get; set; }

        public class Category
        {
            public long Id { get; set; }

            public MultilanguageString Name { get; set; }

            public long? ParentCategoryId { get; set; }
        }
    }
}
