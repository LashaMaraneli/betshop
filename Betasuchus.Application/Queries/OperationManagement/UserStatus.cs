﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.Queries.OperationManagement
{
    public enum UserStatus
    {
        Processing = 1,
        Success = 2,
        Fail = 3
    }
}
