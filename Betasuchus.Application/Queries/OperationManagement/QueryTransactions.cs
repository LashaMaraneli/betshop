﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.OfferManagement;
using Betasuchus.Domain.OperationManagement;
using Betasuchus.Domain.OrderManagement;
using Newtonsoft.Json;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.OperationManagement
{
    public class QueryTransactions : Query<QueryTransactionsResult>
    {
        public long Id { get; set; }

        public override async Task<QueryExecutionResult<QueryTransactionsResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var operationsQuery = _db.Set<Operation>().Where(x => x.OrderId == Id).ToList();
            var operations = operationsQuery
                .Select(x => new QueryTransactionsResult.OperationItem
                {
                    OfferId = x.OfferId,
                    Id = x.Id,
                    Status = x.InternalStatus != Domain.OperationManagement.InternalStatus.Completed ?
                                UserStatus.Processing :
                                    x.Status == OperationStatus.PaymentFailed || x.Status == OperationStatus.BonusFail ?
                                        UserStatus.Fail : UserStatus.Success,

                    Offer = JsonConvert.DeserializeObject<OfferSlimModel>(x.Offer),
                    PayedAmount = x.PayedAmount
                });

            var order = _db.Set<Order>().Find(Id);

            var orderItem = new QueryTransactionsResult.OrderItem
            {
                Id = order.Id,
                PaymentType = order.PaymentType,
                Status = order.InternalStatus,
                TotalAmount = order.TotalAmount
            };

            return await OkAsync(new QueryTransactionsResult
            {
                Order = orderItem,
                Operations = operations
            });
        }
    }

    public class QueryTransactionsResult
    {
        public OrderItem Order { get; set; }

        public IEnumerable<OperationItem> Operations { get; set; }

        public class OperationItem
        {
            public long Id { get; set; }

            public long OfferId { get; set; }

            public UserStatus Status { get; set; }

            public OfferSlimModel Offer { get; set; }

            public decimal PayedAmount { get; set; }
        }

        public class OrderItem
        {
            public long Id { get; set; }

            public PaymentType PaymentType { get; set; }

            public Domain.OrderManagement.InternalStatus Status { get; set; }

            public decimal TotalAmount { get; set; }
        }
    }
}
