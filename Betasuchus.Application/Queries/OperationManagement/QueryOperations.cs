﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.OperationManagement;
using Betasuchus.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.OperationManagement
{
    public class QueryOperations : PagedQuery<QueryOperationsResult>
    {
        public long? CustomerId { get; set; }

        public string Customer { get; set; }

        public string PaymentId { get; set; }

        public PaymentStatus? PaymentStatus { get; set; }

        public OperationStatus? OperationStatus { get; set; }

        public long? OrderId { get; set; }

        public override async Task<QueryExecutionResult<QueryOperationsResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            IQueryable<Operation> baseQuery = _db.Set<Operation>();

            if (CustomerId.HasValue)
            {
                baseQuery = baseQuery.Where(x => x.CustomerId == CustomerId);
            }

            if (Customer.NotNullOrEmpty())
            {
                baseQuery = baseQuery.Where(x => x.Customer.Contains(Customer));
            }

            if (PaymentId.NotNullOrEmpty())
            {
                baseQuery = baseQuery.Where(x => x.PaymentTransactionId == PaymentId);
            }

            if (PaymentStatus.HasValue)
            {
                baseQuery = baseQuery.Where(x => x.PaymentStatus == PaymentStatus);
            }

            if (OrderId.HasValue)
            {
                baseQuery = baseQuery.Where(x => x.OrderId == OrderId);
            }

            var data = baseQuery.Select(x => new QueryOperationsResult.OperationItem
            {
                Id = x.Id,
                OfferSlimModel = x.Offer,
                CreateDate = x.CreateDate,
                Customer = x.Customer,
                OfferId = x.OfferId,
                PayedAmount = x.PayedAmount,
                ProcessStatus = x.InternalStatus,
                Status = x.Status
            });

            var count = await data.CountAsync();

            data = data.SortAndPage(this);

            var operations = await data.ToListAsync();

            return await OkAsync(new QueryOperationsResult
            {
                TotalCount = count,
                Operations = operations
            });
        }
    }

    public class QueryOperationsResult : PagedQueryResult
    {
        public IList<OperationItem> Operations { get; set; }

        public class OperationItem
        {
            public long Id { get; set; }

            public long OfferId { get; set; }

            public string OfferSlimModel { get; set; }

            public Dictionary<string, object> Offer =>
                JsonConvert.DeserializeObject<Dictionary<string, object>>(OfferSlimModel);

            public string Customer { get; set; }

            public InternalStatus ProcessStatus { get; set; }

            public OperationStatus? Status { get; set; }

            public decimal PayedAmount { get; set; }

            public DateTime CreateDate { get; set; }
        }
    }
}
