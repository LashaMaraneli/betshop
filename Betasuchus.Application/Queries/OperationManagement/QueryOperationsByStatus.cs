﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.OperationManagement;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Queries.OperationManagement
{
    public class QueryOperationsByStatus : Query<QueryOperationsByStatusResult>
    {
        public InternalStatus Status { get; set; }

        public override async Task<QueryExecutionResult<QueryOperationsByStatusResult>> ExecuteAsync()
        {
            Log.Information("Executing query: " + GetType().Name);

            var operations = await _db.Set<Operation>()
                .Where(x => x.InternalStatus == Status)
                .ToListAsync();

            return await OkAsync(new QueryOperationsByStatusResult
            {
                Operations = operations
            });
        }
    }

    public class QueryOperationsByStatusResult
    {
        public IList<Operation> Operations { get; set; }
    }
}
