﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.UserManagement;
using Betasuchus.Infrastructure.Db;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Betasuchus.Application.Helpers
{
    //public class AuthHelper
    //{
    //    private readonly BetasuchusDbContext _db;
    //    private readonly UserManager<User> _userManager;
    //    private readonly ApplicationContext _applicationContext;

    //    public AuthHelper(BetasuchusDbContext db, UserManager<User> userManager, ApplicationContext applicationContext)
    //    {
    //        _db = db;
    //        _userManager = userManager;
    //        _applicationContext = applicationContext;
    //    }

    //    public string GetToken(User user)
    //    {
    //        var tokenHandler = new JwtSecurityTokenHandler();
    //        var tokenDescriptor = new SecurityTokenDescriptor
    //        {
    //            Subject = new ClaimsIdentity(new Claim[]
    //            {
    //                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
    //                new Claim(ClaimTypes.Name, user.FirstName + user.LastName),
    //                new Claim(ClaimTypes.MobilePhone, user.PhoneNumber)
    //            }),
    //            Expires = DateTime.UtcNow.AddDays(1),
    //            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(_applicationContext.AppSetting.JwtSignKey), SecurityAlgorithms.HmacSha256Signature)
    //        };

    //        var token = tokenHandler.CreateToken(tokenDescriptor);

    //        return tokenHandler.WriteToken(token);
    //    }
    //}
}
