﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application
{
    public static class Constants
    {
        public static class CacheKeys
        {
            public const string Categories = "Categories";

            public const string Providers = "Providers";

            public const string Offers = "Offers";
        }
    }
}
