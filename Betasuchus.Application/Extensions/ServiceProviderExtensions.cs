﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static T Get<T>(this IServiceProvider serviceProvider)
            => (T)serviceProvider.GetService(typeof(T));
    }
}
