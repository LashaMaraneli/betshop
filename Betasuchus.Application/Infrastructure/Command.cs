﻿using Betasuchus.Infrastructure.Extensions;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Betasuchus.Application.Infrastructure
{
    public abstract class Command : ApplicationBase
    {
        public abstract Task<CommandExecutionResult> ExecuteAsync();

        protected Task<CommandExecutionResult> FailAsync(params string[] errorMessages)
        {
            var result = new CommandExecutionResult
            {
                Success = false
            };

            if (!errorMessages.IsNullOrEmpty())
            {
                result.Error.Errors = errorMessages;
            }

            return Task.FromResult(result);
        }

        protected Task<CommandExecutionResult> FailAsync(ErrorCode errorCode)
        {
            var result = new CommandExecutionResult
            {
                Success = false,
                ErrorCode = errorCode
            };

            return Task.FromResult(result);
        }

        protected async Task<CommandExecutionResult> OkAsync(DomainOperationResult data)
        {
            var result = new CommandExecutionResult
            {
                Data = data,
                Success = true
            };

            return await Task.FromResult(result);
        }

        public string GetJsonObject()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}