﻿using Betasuchus.Infrastructure.Db;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Betasuchus.Application.Infrastructure
{
    public class CommandExecutor
    {
        private readonly BetasuchusDbContext _db;
        private readonly UnitOfWork _unitOfWork;
        private readonly ApplicationContext _applicationContext;
        private readonly IServiceProvider _serviceProvider;

        public CommandExecutor(BetasuchusDbContext db, UnitOfWork unitOfWork, IServiceProvider serviceProvider, ApplicationContext applicationContext)
        {
            _db = db;
            _unitOfWork = unitOfWork;
            _serviceProvider = serviceProvider;
            _applicationContext = applicationContext;
            _serviceProvider = serviceProvider;
        }

        public async Task<CommandExecutionResult> ExecuteAsync(Command command)
        {
            try
            {
                command.Resolve(_db, _unitOfWork, _serviceProvider, _applicationContext);

                return await command.ExecuteAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);

                // TODO: LogException(ex);
                return new CommandExecutionResult
                {
                    Success = false
                };
            }
        }
    }
}
