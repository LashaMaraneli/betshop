﻿using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Claims;

namespace Betasuchus.Application.Infrastructure
{
    public class ApplicationContext
    {
        private readonly IHttpContextAccessor _contextAccessor;

        public ApplicationContext(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;

            var httpContext = contextAccessor.HttpContext;

            if (httpContext != null)
            {
                SetCurrentUserName(httpContext);
                SetCurrentUserIp(httpContext);
                SetUserId(httpContext);
                SetCommonInformation(httpContext);

                if (ClientIp.Contains("localhost") || ClientIp == "127.0.0.1" || ClientIp == "::1")
                {
                    ClientIp = "95.104.55.119";
                }
            }
        }

        public long? UserId { get; private set; }

        public string ClientIp { get; private set; }

        public string CurrentUserName { get; private set; }

        public string EbPin { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string PhoneNumber { get; private set; }

        //public AppSettings AppSetting { get; set; }

        private void SetUserId(HttpContext httpContext)
        {
            var user = httpContext?.User;

            if (user != null)
            {
                if (long.TryParse(user.FindFirst("sub")?.Value, out long userId))
                {
                    UserId = userId;
                }
            }
        }

        //public class AppSettings
        //{
        //    public byte[] JwtSignKey { get; set; }
        //}

        public void SetCurrentUserIp(HttpContext httpContext)
        {
            var currentUserIP = httpContext?.Connection?.RemoteIpAddress?.ToString();

            var forwardedHeader = httpContext?.Request?.Headers["X-Forwarded-For"];
            if (forwardedHeader.HasValue && forwardedHeader.Value.Count != 0)
            {
                currentUserIP = forwardedHeader.Value.First();
            }

            ClientIp = currentUserIP;
        }

        public void SetCurrentUserName(HttpContext httpContext)
        {
            CurrentUserName = httpContext?.User?.Identity?.Name;
        }

        private void SetCommonInformation(HttpContext httpContext)
        {
            var claimsPrincipal = httpContext?.User;

            if (claimsPrincipal != null)
            {
                if (claimsPrincipal.FindFirst("pin")?.Value != null)
                {
                    EbPin = claimsPrincipal.FindFirst("pin").Value;
                }

                if (claimsPrincipal.FindFirst("name")?.Value != null)
                {
                    FirstName = claimsPrincipal.FindFirst("name").Value;
                }

                if (claimsPrincipal.FindFirst("family_name")?.Value != null)
                {
                    LastName = claimsPrincipal.FindFirst("family_name").Value;
                }

                if (claimsPrincipal.FindFirst("phone_number")?.Value != null)
                {
                    PhoneNumber = claimsPrincipal.FindFirst("phone_number").Value;
                }

                if (claimsPrincipal.FindFirst("pin")?.Value != null)
                {
                    EbPin = claimsPrincipal.FindFirst("pin").Value;
                }
            }
        }
    }
}
