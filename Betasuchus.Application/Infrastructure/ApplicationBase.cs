﻿using System;
using Betasuchus.Infrastructure.Db;
using Microsoft.EntityFrameworkCore;

namespace Betasuchus.Application.Infrastructure
{
    public abstract class ApplicationBase
    {
        protected DbContext _db;
        protected UnitOfWork _unitOfWork;
        protected IServiceProvider _serviceProvider;

        protected ApplicationContext ApplicationContext { get; private set; }

        public void Resolve(DbContext db, UnitOfWork unitOfWork, IServiceProvider serviceProvider, ApplicationContext applicationContext)
        {
            _db = db;
            _unitOfWork = unitOfWork;
            _serviceProvider = serviceProvider;
            ApplicationContext = applicationContext;
        }

        public T GetService<T>() => (T)_serviceProvider.GetService(typeof(T));
    }
}
