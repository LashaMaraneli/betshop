﻿namespace Betasuchus.Application.Infrastructure
{
    public class DomainOperationResult
    {
        public DomainOperationResult()
        {
        }

        public DomainOperationResult(long id)
        {
            Id = id;
        }

        public long Id { get; }

        public static DomainOperationResult Create(long id) => new DomainOperationResult(id);

        public static DomainOperationResult CreateEmpty() => new DomainOperationResult();
    }
}
