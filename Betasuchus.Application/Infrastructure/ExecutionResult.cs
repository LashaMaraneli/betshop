﻿using Betasuchus.Shared;
using System.Text.Json.Serialization;

namespace Betasuchus.Application.Infrastructure
{
    public class ExecutionResult
    {
        public ExecutionResult()
        {
            Error = new Error();
        }

        public bool Success { get; set; }

        public DomainOperationResult Data { get; set; }

        public Error Error { get; set; }

        [JsonIgnore]
        public ErrorCode ErrorCode { get; set; }
    }
}
