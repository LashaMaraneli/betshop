﻿namespace Betasuchus.Application.Infrastructure
{
    public class PagedQueryResult
    {
        public int TotalCount { get; set; }
    }
}
