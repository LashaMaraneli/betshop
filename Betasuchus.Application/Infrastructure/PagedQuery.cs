﻿using Betasuchus.Infrastructure.Extensions;
using Betasuchus.Shared;
using System.Threading.Tasks;

namespace Betasuchus.Application.Infrastructure
{
    public abstract class PagedQuery<TQueryResult> : ApplicationBase, ISortedAndPagedListRequestBase
       where TQueryResult : class
    {
        public int PageSize { get; set; }

        public int Page { get; set; }

        public int Total { get; set; }

        public string SortBy { get; set; }

        public SortOrder SortOrder { get; set; }

        public abstract Task<QueryExecutionResult<TQueryResult>> ExecuteAsync();

        protected async Task<QueryExecutionResult<TQueryResult>> FailAsync(params string[] errorMessages)
        {
            var result = new QueryExecutionResult<TQueryResult>
            {
                Success = false
            };

            if (!errorMessages.IsNullOrEmpty())
            {
                result.Error.Errors = errorMessages;
            }

            return await Task.FromResult(result);
        }

        protected async Task<QueryExecutionResult<TQueryResult>> OkAsync(TQueryResult data)
        {
            var result = new QueryExecutionResult<TQueryResult>
            {
                Data = data,
                Success = true
            };

            return await Task.FromResult(result);
        }
    }
}
