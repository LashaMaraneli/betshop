﻿using Betasuchus.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic.Core;
using System.Reflection;

namespace Betasuchus.Application.Infrastructure
{
    public static class QueryableFilteringExtensions
    {
        public static IQueryable<TSource> PageAndSort<TSource, TProp>(this IQueryable<TSource> source, ISortedAndPagedListRequestBase request, Dictionary<string, Expression<Func<TSource, TProp>>> columnsMap)
        {
            if (request == null)
            {
                return source;
            }

            // Default values
            var page = request.Page == 0 ? 1 : request.Page;
            var pageSize = request.PageSize == 0 ? 25 : request.PageSize;
            var sortBy = request.SortBy ?? "Id";
            var isAsc = request.SortOrder == SortOrder.Asc;

            var expression = columnsMap[sortBy];
            var unaryExpression = expression.Body as UnaryExpression;

            if (unaryExpression == null)
            {
                return isAsc ? source.OrderBy(expression) : source.OrderByDescending(expression);
            }

            var propertyExpression = (MemberExpression)unaryExpression.Operand;
            var parameters = expression.Parameters;

            if (propertyExpression.Type == typeof(DateTime))
            {
                var newExpression = Expression.Lambda<Func<TSource, DateTime>>(propertyExpression, parameters);
                return isAsc ? source.OrderBy(newExpression) : source.OrderByDescending(newExpression);
            }

            if (propertyExpression.Type == typeof(DateTime?))
            {
                var newExpression = Expression.Lambda<Func<TSource, DateTime?>>(propertyExpression, parameters);
                return isAsc ? source.OrderBy(newExpression) : source.OrderByDescending(newExpression);
            }

            if (propertyExpression.Type == typeof(int))
            {
                var newExpression = Expression.Lambda<Func<TSource, int>>(propertyExpression, parameters);
                return isAsc ? source.OrderBy(newExpression) : source.OrderByDescending(newExpression);
            }

            if (propertyExpression.Type == typeof(decimal))
            {
                var newExpression = Expression.Lambda<Func<TSource, decimal>>(propertyExpression, parameters);
                return isAsc ? source.OrderBy(newExpression) : source.OrderByDescending(newExpression);
            }

            if (propertyExpression.Type == typeof(decimal?))
            {
                var newExpression = Expression.Lambda<Func<TSource, decimal?>>(propertyExpression, parameters);
                return isAsc ? source.OrderBy(newExpression) : source.OrderByDescending(newExpression);
            }

            if (propertyExpression.Type == typeof(double))
            {
                var newExpression = Expression.Lambda<Func<TSource, double>>(propertyExpression, parameters);
                return isAsc ? source.OrderBy(newExpression) : source.OrderByDescending(newExpression);
            }

            if (propertyExpression.Type == typeof(double?))
            {
                var newExpression = Expression.Lambda<Func<TSource, double?>>(propertyExpression, parameters);
                return isAsc ? source.OrderBy(newExpression) : source.OrderByDescending(newExpression);
            }

            if (propertyExpression.Type.IsEnum)
            {
                return isAsc ? source.OrderBy(expression) : source.OrderByDescending(expression);
            }

            return source
                .Skip(pageSize * (page - 1)).Take(pageSize);
        }

        public static IQueryable<TSource> SortAndPage<TSource>(this IQueryable<TSource> source, ISortedAndPagedListRequestBase request, string sortThenBy = "")
            where TSource : class
        {
            return Page(source.Sort(request, sortThenBy), request);
        }

        public static IQueryable<TSource> Page<TSource>(this IQueryable<TSource> source, ISortedAndPagedListRequestBase request)
                where TSource : class
        {
            if (request == null || request.Page == 0 || request.PageSize == 0)
            {
                return source;
            }

            return source.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize);
        }

        public static IQueryable<TSource> Sort<TSource>(this IQueryable<TSource> source, ISortedAndPagedListRequestBase request, string sortThenBy = "")
            where TSource : class
        {
            if (request != null)
            {
                return source.Sort(request.SortBy, request.SortOrder, sortThenBy);
            }

            return source;
        }

        public static IQueryable<TSource> Sort<TSource>(this IQueryable<TSource> source, string sortBy, SortOrder sortOrder, string sortThenBy = "")
            where TSource : class
        {
            string sort;

            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrWhiteSpace(sort = string.Format("{0} {1}", sortBy, sortOrder)))
            {
                var orderedSource = source.OrderBy(sort);

                if (!string.IsNullOrEmpty(sortThenBy))
                {
                    // TODO
                    // Type type = typeof(TSource);
                    // ParameterExpression arg = Expression.Parameter(type, "x");
                    // Expression expr = arg;
                    // string[] props = sortThenBy.Split('.');
                    // foreach (string prop in props)
                    // {
                    //    // use reflection (not ComponentModel) to mirror LINQ
                    //    PropertyInfo pi = type.GetProperty(prop);
                    //    expr = Expression.Property(expr, pi);
                    //    type = pi.PropertyType;
                    // }
                    string sortThenByQuery = string.Format("{0} {1}", sortThenBy, sortOrder);

                    return orderedSource.ThenBy(sortThenByQuery);
                }

                return orderedSource;
            }

            return source;
        }

        public static IQueryable<TSource> SortQuery<TSource>(this IQueryable<TSource> source, string sortBy, SortOrder sortOrder)
            where TSource : class
        {
            if (!string.IsNullOrEmpty(sortBy))
            {
                string sortOrderMethod = sortOrder == SortOrder.Desc ? "OrderByDescending" : "OrderBy";

                return source.ApplyOrder(sortBy, sortOrderMethod);
            }

            return source;
        }

        public static IOrderedQueryable<TSource> ApplyOrder<TSource>(this IQueryable<TSource> source,
         string property,
         string methodName)
        {
            string[] props = property.Split('.');
            Type type = typeof(TSource);
            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach (string prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                PropertyInfo pi = type.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }

            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(TSource), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);

            object result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == methodName
                            && method.IsGenericMethodDefinition
                            && method.GetGenericArguments().Length == 2
                            && method.GetParameters().Length == 2)
                    .MakeGenericMethod(typeof(TSource), type)
                    .Invoke(null, new object[] { source, lambda });

            return (IOrderedQueryable<TSource>)result;
        }

        public static IQueryable<TSource> And<TSource, TFilter>(this IQueryable<TSource> source, TFilter? filter, Expression<Func<TSource, bool>> predicate)
            where TFilter : struct
        {
            if (predicate == null)
            {
                throw new ArgumentNullException("predicate");
            }

            if (filter.HasValue)
            {
                return source.And(predicate);
            }

            return source;
        }

        public static IQueryable<TSource> And<TSource>(this IQueryable<TSource> source, string filter, Expression<Func<TSource, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException("predicate");
            }

            if (!string.IsNullOrWhiteSpace(filter))
            {
                return source.And(predicate);
            }

            return source;
        }

        public static IQueryable<TSource> And<TSource>(this IQueryable<TSource> source, IEnumerable input, Expression<Func<TSource, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException("predicate");
            }

            if (input != null && input.GetEnumerator().MoveNext())
            {
                return source.And(predicate);
            }

            return source;
        }

        public static IQueryable<TSource> And<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException("predicate");
            }

            return source.Where(predicate);
        }
    }
}
