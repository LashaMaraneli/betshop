﻿using Betasuchus.Shared;
using System.Collections.Generic;

namespace Betasuchus.Application.Infrastructure
{
    public class QueryExecutionResult<T>
    {
        public QueryExecutionResult()
        {
            Error = new Error();
        }

        public bool Success { get; set; }

        public T Data { get; set; }

        public Error Error { get; set; }
    }
}
