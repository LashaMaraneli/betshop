﻿using Betasuchus.Infrastructure.Db;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Betasuchus.Application.Infrastructure
{
    public class QueryExecutor
    {
        private readonly BetasuchusDbContext _db;
        private readonly UnitOfWork _unitOfWork;
        private readonly IServiceProvider _serviceProvider;
        private readonly ApplicationContext _applicationContext;

        public QueryExecutor(BetasuchusDbContext db, UnitOfWork unitOfWork, IServiceProvider serviceProvider, ApplicationContext applicationContext)
        {
            _db = db;
            _unitOfWork = unitOfWork;
            _serviceProvider = serviceProvider;
            _applicationContext = applicationContext;
            _serviceProvider = serviceProvider;
        }

        public async Task<QueryExecutionResult<TResult>> ExecuteAsync<TQuery, TResult>(TQuery query)
            where TQuery : Query<TResult>
            where TResult : class
        {
            try
            {
                query.Resolve(_db, _unitOfWork, _serviceProvider, _applicationContext);

                return await query.ExecuteAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);

                // TODO: Log Exception
                return new QueryExecutionResult<TResult>
                {
                    Success = false
                };
            }
        }

        public async Task<QueryExecutionResult<TResult>> ExecutePagedQueryAsync<TQuery, TResult>(TQuery query)
            where TQuery : PagedQuery<TResult>
            where TResult : class
        {
            try
            {
                query.Resolve(_db, _unitOfWork, _serviceProvider, _applicationContext);

                return await query.ExecuteAsync();
            }
            catch (Exception)
            {
                // TODO: Log Exception
                return new QueryExecutionResult<TResult>
                {
                    Success = false
                };
            }
        }
    }
}
