﻿using Betasuchus.Infrastructure.Extensions;
using System.Threading.Tasks;

namespace Betasuchus.Application.Infrastructure
{
    public abstract class Query<TQueryResult> : ApplicationBase
        where TQueryResult : class
    {
        public abstract Task<QueryExecutionResult<TQueryResult>> ExecuteAsync();

        protected async Task<QueryExecutionResult<TQueryResult>> FailAsync(params string[] errorMessages)
        {
            var result = new QueryExecutionResult<TQueryResult>
            {
                Success = false
            };

            if (!errorMessages.IsNullOrEmpty())
            {
                result.Error.Errors = errorMessages;
            }

            return await Task.FromResult(result);
        }

        protected async Task<QueryExecutionResult<TQueryResult>> OkAsync(TQueryResult data)
        {
            var result = new QueryExecutionResult<TQueryResult>
            {
                Data = data,
                Success = true
            };

            return await Task.FromResult(result);
        }
    }
}
