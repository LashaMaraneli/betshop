﻿using Betasuchus.Domain.OperationManagement;
using Betasuchus.Domain.OrderManagement;
using Betasuchus.Domain.OrderManagement.Events;
using Betasuchus.Infrastructure.EventDispatching;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog;
using System.Linq;

namespace Betasuchus.Application.EventHandlers.OrderManagement
{
    public class OrderEventHandlers :
        IHandleEvent<OrderProcessingStarted>
    {
        public void Handle(OrderProcessingStarted @event, DbContext db)
        {
            Log.Information("Handling event: " + @event.GetType().Name);

            var operations = db.Set<Operation>().Where(x => x.OrderId == @event.AggregateRootId).ToList();

            var order = JsonConvert.DeserializeObject<Order>(@event.Payload);

            foreach (var operation in operations)
            {
                operation.SetTransactionId(order.PaymentTransactionId);
                operation.SetStatus(Domain.OperationManagement.InternalStatus.AwaitingPayment);
            }
        }
    }
}
