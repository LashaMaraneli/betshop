﻿using Betasuchus.Domain.OperationManagement;
using Betasuchus.Domain.OperationManagement.Events;
using Betasuchus.Domain.OrderManagement;
using Betasuchus.Infrastructure.EventDispatching;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog;

namespace Betasuchus.Application.EventHandlers.OperationManagement
{
    public class OperationEventHandlers :
        IHandleEvent<OperationStatusChanged>
    {
        public void Handle(OperationStatusChanged @event, DbContext db)
        {
            Log.Information("Handling event: " + @event.GetType().Name);

            var operation = JsonConvert.DeserializeObject<Operation>(@event.Payload);

            if (operation.InternalStatus == Domain.OperationManagement.InternalStatus.Completed)
            {
                var order = db.Set<Order>().Find(operation.OrderId);
                order.Complete();
            }
        }
    }
}
