﻿using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.ProviderManagement.Events;
using Betasuchus.Infrastructure.EventDispatching;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog;
using System.Linq;

namespace Betasuchus.Application.EventHandlers.ProviderManagement
{
    public class ProviderEventHandlers
        : IHandleEvent<ProviderChanged>
    {
        public void Handle(ProviderChanged @event, DbContext db)
        {
            Log.Information("Handling event: " + @event.GetType().Name);

            var category = JsonConvert.DeserializeObject<Domain.ProviderManagement.Provider>(@event.Payload);

            var offers = db.Set<SlotOffer>().Where(x => x.ProviderId == @event.AggregateRootId).ToListAsync().GetAwaiter().GetResult(); ;

            foreach (var offer in offers)
            {
                offer.SetProvider(category);
            }
        }
    }
}
