﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.BonusHub;
using Betasuchus.Domain.OperationManagement;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OperationManagement
{
    public class CheckBonusHubStatusCommand : Command
    {
        public long Id { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var operation = await _db.Set<Operation>().FindAsync(Id);

            var bonusHubRespone = await GetService<IBonusHubManager>().CheckAwardStatus(new IntegrationServices.BonusHub.Models.CheckStatusRequest
            {
                BonusPoolId = operation.BonusPoolId
            });

            if (bonusHubRespone.Success)
            {
                if (bonusHubRespone.Data == IntegrationServices.BonusHub.Models.AwardStatus.Success)
                {
                    operation.SetStatus(InternalStatus.Completed, OperationStatus.BonusSuccess);
                }
                if (bonusHubRespone.Data == IntegrationServices.BonusHub.Models.AwardStatus.Failed)
                {
                    operation.SetStatus(InternalStatus.Completed, OperationStatus.BonusFail);
                }
            }

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}
