﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.BonusHub;
using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.OperationManagement;
using Betasuchus.Domain.OrderManagement;
using Microsoft.Extensions.Options;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OperationManagement
{
    public class StartAwardingCommand : Command
    {
        public long Id { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var config = GetService<IOptions<BonusHubSettings>>().Value;

            var operation = await _db.Set<Operation>().FindAsync(Id);
            var offer = await _db.Set<Offer>().FindAsync(operation.OfferId);
            var order = await _db.Set<Order>().FindAsync(operation.OrderId);

            var bonusHubObject = offer.BonusHubObject();
            bonusHubObject.Add("playedId", order.CustomerPin);

            var bonusHubResponse = await GetService<IBonusHubManager>().
                Reward(
                    new IntegrationServices.BonusHub.Models.BonusRequest(
                        config.Channel,
                        config.UserName,
                        $"BetMarket {typeof(Offer).Name} Award",
                        bonusHubObject));

            if (bonusHubResponse.Success)
            {
                operation.SetBonusPoolId(bonusHubResponse.Data.BonusPoolId);
                operation.SetStatus(Domain.OperationManagement.InternalStatus.Sent);
            }
            else
            {
                operation.SetStatus(Domain.OperationManagement.InternalStatus.Completed, OperationStatus.BonusFail);
            }

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}
