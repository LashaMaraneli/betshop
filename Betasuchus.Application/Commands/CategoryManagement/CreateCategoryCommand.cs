﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.FileManagement;
using Betasuchus.Application.IntegrationServices.FileManagement.Models;
using Betasuchus.Application.IntegrationServices.SequenceProvider;
using Betasuchus.Domain.CategoryManagement;
using Betasuchus.Shared;
using Microsoft.AspNetCore.Http;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.CategoryManagement
{
    public class CreateCategoryCommand : Command
    {
        public long? ParentCategoryId { get; set; }

        public MultilanguageString Name { get; set; }

        public IFormFile Thumbnail { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var thumbnail = await UploadFile(new UploadFileRequest { UploadedFile = Thumbnail });

            var sequence = GetService<ISequenceProvider<Category>>();
            var id = await sequence.Next();

            var category = new Category(id, ParentCategoryId, Name, thumbnail.Url);
            await _db.Set<Category>().AddAsync(category);

            await _unitOfWork.Save();

            return await OkAsync(DomainOperationResult.Create(category.Id));
        }

        public async Task<UploadFileResponse> UploadFile(UploadFileRequest request)
            => await GetService<IFileManager>().UploadFile(request);
    }
}
