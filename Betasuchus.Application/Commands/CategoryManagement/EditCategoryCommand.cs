﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.FileManagement;
using Betasuchus.Application.IntegrationServices.FileManagement.Models;
using Betasuchus.Domain.CategoryManagement;
using Betasuchus.Shared;
using Microsoft.AspNetCore.Http;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.CategoryManagement
{
    public class EditCategoryCommand : Command
    {
        public long Id { get; set; }

        public long? ParentCategoryId { get; set; }

        public MultilanguageString Name { get; set; }

        public IFormFile Thumbnail { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            string thumbnailUrl = null;
            if (Thumbnail != null)
            {
                var thumbnail = await UploadFile(new UploadFileRequest { UploadedFile = Thumbnail });
                thumbnailUrl = thumbnail.Url;
            }

            var category = _db.Set<Category>().Find(Id);

            if (category != null)
            {
                category.EditCategory(Name, thumbnailUrl);

                await _unitOfWork.Save();
                return await OkAsync(DomainOperationResult.Create(category.Id));
            }

            return await FailAsync("Category not found");
        }

        public async Task<UploadFileResponse> UploadFile(UploadFileRequest request)
            => await GetService<IFileManager>().UploadFile(request);
    }
}
