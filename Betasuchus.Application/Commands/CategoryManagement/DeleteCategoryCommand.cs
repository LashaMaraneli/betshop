﻿using Betasuchus.Application.Infrastructure;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.CategoryManagement
{
    public class DeleteCategoryCommand : Command
    {
        public long Id { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var category = _db.Set<Domain.CategoryManagement.Category>().Find(Id);

            if (category != null)
            {
                _db.Entry(category).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                await _unitOfWork.Save();

                return await OkAsync(new DomainOperationResult());
            }

            return await FailAsync("Category not found");
        }
    }
}
