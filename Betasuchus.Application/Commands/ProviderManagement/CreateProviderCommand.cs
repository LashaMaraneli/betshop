﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.SequenceProvider;
using Betasuchus.Domain.ProviderManagement;
using Betasuchus.Shared;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.ProviderManagement
{
    public class CreateProviderCommand : Command
    {
        public MultilanguageString Name { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var id = await GetService<ISequenceProvider<Provider>>().Next();

            var provider = new Provider(id, Name);

            _db.Set<Provider>().Add(provider);

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}
