﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.ProviderManagement;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.ProviderManagement
{
    public class DeleteProviderCommand : Command
    {
        public long Id { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var provider = await _db.Set<Provider>().FindAsync(Id);

            if (provider == null)
            {
                return await FailAsync("Provider not found");
            }

            _db.Entry(provider).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}
