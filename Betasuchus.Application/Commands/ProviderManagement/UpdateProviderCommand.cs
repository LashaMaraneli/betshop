﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.ProviderManagement;
using Betasuchus.Shared;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.ProviderManagement
{
    public class UpdateProviderCommand : Command
    {
        public long Id { get; set; }

        public long ParentCategoryId { get; set; }

        public MultilanguageString Name { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var provider = await _db.Set<Provider>().FindAsync(Id);

            if (provider == null)
            {
                return await FailAsync("Provider not found");
            }

            provider.Update(Name);

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}
