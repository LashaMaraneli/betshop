﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.PaymentManagement;
using Betasuchus.Domain.OrderManagement;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OrderManagement
{
    public class RequestPaymentCommand : Command
    {
        public long Id { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var order = await _db.Set<Order>().FindAsync(Id);

            var paymentService = GetService<IPaymentProviderFactory>().ResolvePaymentProvider(order.PaymentType, ApplicationContext);

            var payment = await paymentService.Pay(new IntegrationServices.PaymentManagement.Models.PayRequest
            {
                Amount = order.TotalAmount,
                CustomerId = order.CustomerId
            });

            order.SetPaymentIdAndBeginProcessing(payment.TransactionId);

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}