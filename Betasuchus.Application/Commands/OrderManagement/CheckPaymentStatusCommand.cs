﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.PaymentManagement;
using Betasuchus.Domain.OperationManagement;
using Betasuchus.Domain.OrderManagement;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OrderManagement
{
    public class CheckPaymentStatusCommand : Command
    {
        public long Id { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var order = await _db.Set<Order>().FindAsync(Id);

            var paymentService = GetService<IPaymentProviderFactory>().ResolvePaymentProvider(order.PaymentType, ApplicationContext);

            var result = await paymentService.CheckPayment(new IntegrationServices.PaymentManagement.Models.CheckPaymentRequest
            {
                TransactionId = order.PaymentTransactionId
            });

            var operations = await _db.Set<Operation>().Where(x => x.OrderId == Id).ToListAsync();

            foreach (var operation in operations)
            {
                if (result.Status == "Success")
                {
                    operation.SetStatus(Domain.OperationManagement.InternalStatus.AwaitingSending);
                }
                else if (result.Status == "Fail")
                {
                    operation.SetStatus(Domain.OperationManagement.InternalStatus.Completed, OperationStatus.PaymentFailed);
                }
            }

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}
