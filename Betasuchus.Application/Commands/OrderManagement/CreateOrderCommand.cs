﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.SequenceProvider;
using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.OperationManagement;
using Betasuchus.Domain.OrderManagement;
using Newtonsoft.Json;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OrderManagement
{
    public class CreateOrderCommand : Command
    {
        public long CardId { get; set; }

        public string Card { get; set; }

        public PaymentType PaymentType { get; set; }

        public IEnumerable<OrderData> Offers { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var operations = new List<Operation>();

            var sequence = GetService<ISequenceProvider<Operation>>();
            var orderId = await GetService<ISequenceProvider<Order>>().Next();

            var customer = new Customer
            {
                FirstName = ApplicationContext.FirstName,
                LastName = ApplicationContext.LastName,
                EbPin = ApplicationContext.EbPin,
                PhoneNumber = ApplicationContext.PhoneNumber
            };

            foreach (var item in Offers)
            {
                var operationId = await sequence.Next();
                var offer = await _db.Set<Offer>().FindAsync(item.OfferId);

                var offerSlimModel = JsonConvert.SerializeObject(offer.ToOfferSlimModel());

                var operation = new Operation(
                    operationId,
                    offer.Id,
                    offerSlimModel,
                    ApplicationContext.UserId.Value,
                    customer.ToString(),
                    offer.DiscountedPrice * item.Amount,
                    orderId);

                operations.Add(operation);
            }

            await _db.Set<Operation>().AddRangeAsync(operations);

            var order = new Order(
                orderId,
                operations.Sum(x => x.PayedAmount),
                Card,
                ApplicationContext.UserId.Value,
                customer.ToString(),
                PaymentType,
                ApplicationContext.EbPin
                );

            await _db.Set<Order>().AddAsync(order);

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult(orderId));
        }

        public class OrderData
        {
            public long OfferId { get; set; }

            public int Amount { get; set; }
        }

        public class Customer
        {
            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string PhoneNumber { get; set; }

            public string EbPin { get; set; }

            public override string ToString()
            {
                return JsonConvert.SerializeObject(this);
            }
        }
    }
}