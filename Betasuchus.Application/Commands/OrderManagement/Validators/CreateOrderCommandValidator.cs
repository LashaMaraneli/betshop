﻿using FluentValidation;

namespace Betasuchus.Application.Commands.OrderManagement.Validators
{
    public class CreateOrderCommandValidator : AbstractValidator<CreateOrderCommand>
    {
        public CreateOrderCommandValidator()
        {
            RuleFor(x => x.CardId).NotEmpty().When(t => t.PaymentType == Domain.OrderManagement.PaymentType.Card);

            RuleFor(x => x.Offers).NotEmpty();

            RuleFor(x => x.Card).NotEmpty().When(t => t.PaymentType == Domain.OrderManagement.PaymentType.Card);
        }
    }
}
