﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.FileManagement.Models;
using Betasuchus.Application.IntegrationServices.SequenceProvider;
using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.CategoryManagement;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OfferManagement.Poker
{
    public class CreatePokerCommand : CreateExtendedOfferCommand
    {
        public string BonusCode { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var uploadFileResponse = await UploadFile(new UploadFileRequest()
            {
                UploadedFile = Thumbnail
            });

            var id = await GetService<ISequenceProvider<Offer>>().Next();

            var category = await _db.Set<Category>().FindAsync(ProviderId);

            var poker = new Domain.OfferManagement.Poker(
                id,
                Amount,
                ExpireDate,
                Price,
                DiscountedPrice,
                Expire,
                uploadFileResponse.Url,
                OfferText,
                GameType,
                CategoryId,
                category.GetJsonObject(),
                BonusCode,
                GameName);

            await _db.Set<Offer>().AddAsync(poker);

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}
