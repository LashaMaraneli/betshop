﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.OfferManagement.Abstract;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OfferManagement
{
    public class DeleteOfferCommand : Command
    {
        public long Id { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var offer = await _db.Set<Offer>().FindAsync(Id);

            if (offer != null)
            {
                offer.Delete();

                await _unitOfWork.Save();
                return await OkAsync(new DomainOperationResult());
            }

            return await FailAsync("Offer not found");
        }
    }
}
