﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.SequenceProvider;
using Betasuchus.Domain.OfferManagement.Abstract;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OfferManagement
{
    public class ActivateExpiredOfferCommand : Command
    {
        public long Id { get; set; }

        public DateTime ExpireDate { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var offer = await _db.Set<Offer>().FindAsync(Id);

            var id = await GetService<ISequenceProvider<Offer>>().Next();

            var offerClone = offer.CloneWithNewId(id, ExpireDate);

            await _db.Set<Offer>().AddAsync(offerClone);

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}
