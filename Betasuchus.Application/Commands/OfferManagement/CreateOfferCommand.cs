﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.FileManagement;
using Betasuchus.Application.IntegrationServices.FileManagement.Models;
using Betasuchus.Domain.OfferManagement;
using Betasuchus.Shared;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OfferManagement
{
    public abstract class CreateOfferCommand : Command
    {
        public int Amount { get; set; }

        public DateTime ExpireDate { get; set; }

        public decimal Price { get; set; }

        public decimal DiscountedPrice { get; set; }

        public long ProviderId { get; set; }

        public long CategoryId { get; set; }

        public IFormFile Thumbnail { get; set; }

        public MultilanguageString OfferText { get; set; }

        public int FreebetNominal { get; set; }

        public GameType GameType { get; set; }

        public MultilanguageString GameName { get; set; }

        public async Task<UploadFileResponse> UploadFile(UploadFileRequest request)
            => await GetService<IFileManager>().UploadFile(request);
    }
}
