﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.FileManagement.Models;
using Betasuchus.Application.IntegrationServices.SequenceProvider;
using Betasuchus.Domain.CategoryManagement;
using Betasuchus.Domain.OfferManagement.Abstract;
using Betasuchus.Domain.ProviderManagement;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.OfferManagement.PlayNGo
{
    public class CreatePlayNGoCommand : CreateSlotOfferCommand
    {
        public string TriggerId { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var uploadFileResponse = await UploadFile(new UploadFileRequest()
            {
                UploadedFile = Thumbnail
            });

            var id = await GetService<ISequenceProvider<Offer>>().Next();

            var provider = await _db.Set<Provider>().FindAsync(ProviderId);
            var category = await _db.Set<Category>().FindAsync(CategoryId);

            var playNGo = new Domain.OfferManagement.PlayNGo(
                id,
                Amount,
                ExpireDate,
                Price,
                DiscountedPrice,
                Expire,
                uploadFileResponse.Url,
                OfferText,
                GameType,
                CategoryId,
                category.GetJsonObject(),
                TriggerId,
                ProviderId,
                provider.GetJsonObject(),
                GameName);

            await _db.Set<Offer>().AddAsync(playNGo);

            await _unitOfWork.Save();

            return await OkAsync(new DomainOperationResult());
        }
    }
}
