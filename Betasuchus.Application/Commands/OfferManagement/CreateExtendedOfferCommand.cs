﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.Commands.OfferManagement
{
    public abstract class CreateExtendedOfferCommand : CreateOfferCommand
    {
        public int Expire { get; set; }
    }
}
