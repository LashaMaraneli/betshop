﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.UserManagement;
using Microsoft.AspNetCore.Identity;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.UserCommands
{
    public class LoginUserCommand : Command
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var signInManager = GetService<SignInManager<User>>();

            var result = await signInManager.PasswordSignInAsync(UserName, Password, false, false);

            if (result.Succeeded)
            {
                return await OkAsync(DomainOperationResult.CreateEmpty());
            }

            return await FailAsync();
        }
    }
}
