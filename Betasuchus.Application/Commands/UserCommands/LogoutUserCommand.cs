﻿using System.Threading.Tasks;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.UserManagement;
using Microsoft.AspNetCore.Identity;
using Serilog;

namespace Betasuchus.Application.Commands.UserCommands
{
    public class LogoutUserCommand : Command
    {
        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var signInManager = GetService<SignInManager<User>>();

            await signInManager.SignOutAsync();

            return await OkAsync(DomainOperationResult.CreateEmpty());
        }
    }
}
