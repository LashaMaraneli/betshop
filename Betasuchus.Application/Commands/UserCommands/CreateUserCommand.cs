﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.SequenceProvider;
using Betasuchus.Application.Resources;
using Betasuchus.Domain.UserManagement;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.UserCommands
{
    public class CreateUserCommand : Command
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime? BirthDate { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var localizer = GetService<IStringLocalizer<ResourceStrings>>();

            if (_db.Set<User>().Any(x => x.Email == Email))
            {
                return await FailAsync(localizer["UserAlreadyExists"]);
            }

            var userManager = GetService<UserManager<User>>();
            var id = await GetService<ISequenceProvider<User>>().Next();

            var user = new User(id, FirstName, LastName, Email, PhoneNumber, BirthDate);

            var result = await userManager.CreateAsync(user);

            if (!result.Succeeded)
            {
                return await FailAsync("Could not create user");
            }

            user = await userManager.FindByEmailAsync(Email);

            result = await userManager.AddPasswordAsync(user, Password);

            if (!result.Succeeded)
            {
                return await FailAsync("User created, could not save password");
            }

            result = await userManager.AddToRoleAsync(user, Role);

            if (!result.Succeeded)
            {
                return await FailAsync("User created, could not add to role");
            }

            return await OkAsync(DomainOperationResult.Create(user.Id));
        }
    }
}
