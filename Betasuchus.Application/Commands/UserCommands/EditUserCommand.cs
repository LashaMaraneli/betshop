﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Resources;
using Betasuchus.Domain.UserManagement;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.UserCommands
{
    public class EditUserCommand : Command
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime? BirthDate { get; set; }

        public string Role { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var localizer = GetService<IStringLocalizer<ResourceStrings>>();
            var userManager = GetService<UserManager<User>>();

            var user = await userManager.FindByIdAsync(Id.ToString());

            if (_db.Set<User>().Any(x => x.Email == Email && x.Id != Id))
            {
                return await FailAsync(localizer["UserAlreadyExists"]);
            }

            user.UpdateUser(FirstName, LastName, Email, PhoneNumber, BirthDate);

            var result = await userManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                return await FailAsync();
            }

            var roles = await userManager.GetRolesAsync(user);

            if (roles.Count > 0 && roles.First().ToUpper() != Role.ToUpper())
            {
                await userManager.RemoveFromRoleAsync(user, roles.First());
                result = await userManager.AddToRoleAsync(user, Role);
            }

            return await OkAsync(DomainOperationResult.Create(user.Id));
        }
    }
}
