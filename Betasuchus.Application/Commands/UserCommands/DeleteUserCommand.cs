﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.UserManagement;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.UserCommands
{
    public class DeleteUserCommand : Command
    {
        public long Id { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            var user = await _db.Set<User>().FindAsync(Id);

            if (user != null)
            {
                user.Delete();
                await _unitOfWork.Save();

                return await OkAsync(new DomainOperationResult());
            }

            return await FailAsync("User not found");
        }
    }
}
