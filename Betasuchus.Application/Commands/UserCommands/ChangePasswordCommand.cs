﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.UserManagement;
using Betasuchus.Domain.UserManagement.Events;
using Microsoft.AspNetCore.Identity;
using Serilog;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Application.Commands.UserCommands
{
    public class ChangePasswordCommand : Command
    {
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

        public string ConfirmPassword { get; set; }

        public override async Task<CommandExecutionResult> ExecuteAsync()
        {
            Log.Information("Executing command: " + GetType().Name);

            if (NewPassword.Equals(ConfirmPassword))
            {
                var userManager = GetService<UserManager<User>>();

                var user = _db.Set<User>().FirstOrDefault(x => x.Id == ApplicationContext.UserId.Value);

                if (user == null)
                {
                    return await FailAsync();
                }

                var changePasswordResult = await userManager.ChangePasswordAsync(user, OldPassword, NewPassword);

                if (changePasswordResult.Succeeded)
                {
                    user.Raise(new PasswordChanged(user.Id, this.GetJsonObject()));

                    return await OkAsync(DomainOperationResult.CreateEmpty());
                }
            }

            return await FailAsync();
        }
    }
}
