﻿using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.SequenceProvider
{
    public interface ISequenceProvider<T>
    {
        Task<long> Next();
    }
}
