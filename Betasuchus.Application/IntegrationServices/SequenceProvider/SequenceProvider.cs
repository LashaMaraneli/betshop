﻿using Betasuchus.Infrastructure.Db;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.SequenceProvider
{
    public class SequenceProvider<T> : ISequenceProvider<T>
    {
        private readonly BetasuchusDbContext _db;

        public SequenceProvider(BetasuchusDbContext db)
        {
            _db = db;
        }

        public async Task<long> Next()
        {
            Log.Information("SequenceProvider.Next getting next value for " + nameof(T));

            var connection = _db.Database.GetDbConnection();
            var command = connection.CreateCommand();

            command.CommandText = $"SELECT NEXTVAL('public.\"{typeof(T).Name}_Id_seq\"')";

            if (connection.State != System.Data.ConnectionState.Open)
            {
                await connection.OpenAsync();
            }

            var seq = (long)await command.ExecuteScalarAsync();

            Log.Information("SequenceProvider.Next got next value for " + nameof(T));

            return seq;
        }
    }
}
