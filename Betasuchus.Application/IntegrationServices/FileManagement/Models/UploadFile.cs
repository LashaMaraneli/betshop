﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.FileManagement.Models
{
    public class UploadFileRequest
    {
        public IFormFile UploadedFile { get; set; }

        public string AwsFolderName { get; set; }
    }

    public class UploadFileResponse
    {
        public string FileName { get; set; }

        public string Url { get; set; }

        public bool Success { get; set; }
    }
}
