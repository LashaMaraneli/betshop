﻿using Betasuchus.Application.IntegrationServices.FileManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.FileManagement
{
    public interface IFileManager
    {
        Task<UploadFileResponse> UploadFile(UploadFileRequest request);
    }
}
