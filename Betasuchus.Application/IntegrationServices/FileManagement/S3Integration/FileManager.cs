﻿using Amazon.S3;
using Betasuchus.Application.IntegrationServices.FileManagement.Models;
using Microsoft.Extensions.Options;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.FileManagement.S3Integration
{
    public class FileManager : IFileManager
    {
        private readonly S3Config _config;

        public FileManager(IOptions<S3Config> config)
        {
            _config = config.Value;
        }

        public async Task<UploadFileResponse> UploadFile(UploadFileRequest request)
        {
            Log.Information("FileManager.UploadFile: Uploading file");

            var s3Client = new AmazonS3Client(
                _config.AccessKeyId,
                _config.SecretAccessKey,
                Amazon.RegionEndpoint.GetBySystemName(_config.SystemName));

            var folderName = string.IsNullOrEmpty(request.AwsFolderName) ? string.Empty : request.AwsFolderName;

            var fileName = string.Format("{0}.{1}", Guid.NewGuid().ToString("N"), Path.GetExtension(request.UploadedFile.FileName));
            var url = _config.RootUrl + folderName + fileName;

            try
            {
                using var memStream = new MemoryStream();
                await request.UploadedFile.CopyToAsync(memStream);

                var putObjectResponse = await s3Client.PutObjectAsync(new Amazon.S3.Model.PutObjectRequest
                {
                    AutoCloseStream = true,
                    BucketName = _config.BucketName,
                    InputStream = memStream,
                    Key = string.IsNullOrEmpty(request.AwsFolderName) + fileName,
                    CannedACL = new S3CannedACL(S3CannedACL.PublicRead)
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "FileManager.UploadFile");
                throw;
            }

            return new UploadFileResponse
            {
                FileName = fileName,
                Success = true,
                Url = url
            };
        }
    }
}