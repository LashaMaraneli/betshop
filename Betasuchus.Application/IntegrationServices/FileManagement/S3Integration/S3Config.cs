﻿using Amazon;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.FileManagement.S3Integration
{
    public class S3Config
    {
        public string SecretAccessKey { get; set; }
        public string AccessKeyId { get; set; }
        public string SystemName { get; set; }
        public string BucketName { get; set; }
        public string RootUrl { get; set; }
    }
}
