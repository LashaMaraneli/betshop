﻿using Betasuchus.Application.IntegrationServices.FileManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.FileManagement
{
    public class MockFileManager : IFileManager
    {
        public async Task<UploadFileResponse> UploadFile(UploadFileRequest request)
        {
            var fileName = string.Format("{0}.jpg", Guid.NewGuid().ToString());
            return await Task.FromResult(new UploadFileResponse
            {
                Success = true,
                FileName = fileName,
                Url = $@"https://localhost:5001/{fileName}"
            });
        }
    }
}
