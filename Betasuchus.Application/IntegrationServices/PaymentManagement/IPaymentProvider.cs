﻿using Betasuchus.Application.IntegrationServices.PaymentManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement
{
    public interface IPaymentProvider
    {
        Task<PayResponse> Pay(PayRequest request);

        Task<CheckPaymentResponse> CheckPayment(CheckPaymentRequest request);
    }
}
