﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.PaymentManagement.Models;
using Betasuchus.Domain.OrderManagement;
using System;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement
{
    public class MockPaymentProviderFactory : IPaymentProviderFactory
    {
        public IPaymentProvider ResolvePaymentProvider(PaymentType paymentType, ApplicationContext context)
        {
            return new MockPayment();
        }
    }

    public class MockPayment : IPaymentProvider
    {
        public MockPayment()
        {
        }

        public async Task<CheckPaymentResponse> CheckPayment(CheckPaymentRequest request)
        {
            return await Task.FromResult(new CheckPaymentResponse() { Status = "Success" });
        }

        public async Task<PayResponse> Pay(PayRequest request)
        {
            return await Task.FromResult(new PayResponse() { TransactionId = Guid.NewGuid().ToString() });
        }
    }
}
