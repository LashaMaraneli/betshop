﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.OrderManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement
{
    public interface IPaymentProviderFactory
    {
        IPaymentProvider ResolvePaymentProvider(PaymentType paymentType, ApplicationContext context);
    }
}
