﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement.Models
{
    public class PayRequest
    {
        public long CustomerId { get; set; }

        public decimal Amount { get; set; }

        public IEnumerable<OrderData> Offers { get; set; }

        public string AuthToken { get; set; }
        
        public long CreditCardId { get; set; }

        public class OrderData
        {
            public string OfferId { get; set; }

            public string Offer { get; set; }

            public decimal Amount { get; set; }
        }
    }

    public class PayResponse
    {
        public string TransactionId { get; set; }
    }
}
