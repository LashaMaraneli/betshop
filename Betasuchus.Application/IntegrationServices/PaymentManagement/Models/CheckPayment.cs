﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement.Models
{
    public class CheckPaymentRequest
    {
        public string TransactionId { get; set; }
    }

    public class CheckPaymentResponse
    {
        public string Status { get; set; }
    }
}
