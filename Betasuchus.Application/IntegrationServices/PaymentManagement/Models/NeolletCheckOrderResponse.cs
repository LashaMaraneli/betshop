﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement.Models
{
    public class NeolletCheckOrderResponse
    {
        public long Id { get; set; }
        public string ProductName { get; set; }
        public string AbonentCode { get; set; }
        public string AbonentDescription { get; set; }
        public string OpCode { get; set; }
        public decimal Amount { get; set; }
        public TransactionStatus Status { get; set; }
        public string LastDigits { get; set; }
        public string CardType { get; set; }
        public string Name { get; set; }
    }
}
