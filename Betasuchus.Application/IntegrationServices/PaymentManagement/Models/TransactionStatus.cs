﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement.Models
{
    public enum TransactionStatus
    {
        Pending = 0,
        ProcessingInternally = 1,
        ProcessingExternally = 2,
        Success = 10,
        Block = 20,
        TimedOut = 30
    }
}
