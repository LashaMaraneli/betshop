﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement.Models
{
    public class AddCardRequest
    {
        public long CustomerId { get; set; }

        public string AuthToken { get; set; }
    }

    public class AddCardResponse
    {
        public string TransactionId { get; set; }
    }
}
