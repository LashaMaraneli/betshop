﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement.Models
{
    public class Sequence
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Protection { get; set; }
    }
}
