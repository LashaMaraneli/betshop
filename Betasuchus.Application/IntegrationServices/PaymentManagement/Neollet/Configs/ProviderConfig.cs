﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement.Neollet.Configs
{
    public class ProviderConfig
    {
        public string BaseUrl { get; set; }

        public string PayUrl { get; set; }

        public string CheckPaymentUrl { get; set; }
    }
}
