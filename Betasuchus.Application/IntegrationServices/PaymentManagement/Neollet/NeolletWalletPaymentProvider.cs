﻿using Betasuchus.Application.Helpers;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.PaymentManagement.Models;
using Betasuchus.Application.IntegrationServices.PaymentManagement.Neollet.Configs;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement.Neollet
{
    public class NeolletWalletPaymentProvider : IPaymentProvider
    {
        private readonly NeolletPaymentProviderConfig _providerConfig;
        private readonly ApplicationContext _appContext;

        public NeolletWalletPaymentProvider(
            IOptions<NeolletPaymentProviderConfig> providerConfig,
            ApplicationContext appContext)
        {
            _providerConfig = providerConfig.Value;
            _appContext = appContext;
        }

        public async Task<CheckPaymentResponse> CheckPayment(CheckPaymentRequest request)
        {
            using var httpClient = new HttpClient();

            var responseString = await httpClient.GetStringAsync(_providerConfig.BaseUrl + _providerConfig.CheckPaymentUrl + request.TransactionId);

            var response = JsonConvert.DeserializeObject<NeolletCheckOrderResponse>(responseString);

            var status = string.Empty;
            switch (response.Status)
            {
                case TransactionStatus.Block:
                case TransactionStatus.Pending:
                case TransactionStatus.ProcessingExternally:
                case TransactionStatus.ProcessingInternally: status = "Processing"; break;
                case TransactionStatus.TimedOut: status = "Fail"; break;
                case TransactionStatus.Success: status = "Success"; break;
            }

            return new CheckPaymentResponse
            {
                Status = status
            };
        }

        public async Task<PayResponse> Pay(PayRequest request)
        {
            var dictionary = new Dictionary<string, object>
            {
                { "creditCardId", 0 },
                { "paymentType", "Wallet" },
                { "clientIpAddress", _appContext.ClientIp }
            };

            var list = new List<Dictionary<string, object>>();
            foreach (var item in request.Offers)
            {
                var subItem = new Dictionary<string, object>
                {
                    { "offerId", item.OfferId },
                    { "offer", item.Offer },
                    { "amount", item.Amount },
                    { "ccy", "GEL" }
                };

                list.Add(subItem);
            }

            dictionary.Add("offer", list);

            var json = JsonConvert.SerializeObject(dictionary);
            var htmlContent = new StringContent(json);

            try
            {
                Log.Information("Wallet payment - Sending parameters headers: Authorization, ", request.AuthToken);
                Log.Information("Wallet payment - Sending parameters: " + htmlContent);

                using var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", request.AuthToken);

                var response = await httpClient.PostAsync(_providerConfig.BaseUrl + _providerConfig.PayUrl, htmlContent);

                response.EnsureSuccessStatusCode();

                var responseContent = await response.Content.ReadAsStringAsync();

                Log.Information("Wallet payment - Received response: " + responseContent);

                return new PayResponse { TransactionId = responseContent };
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
                throw;
            }
        }
    }
}
