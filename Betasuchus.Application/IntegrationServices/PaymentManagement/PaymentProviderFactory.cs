﻿using Betasuchus.Application.Extensions;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.IntegrationServices.PaymentManagement.Neollet;
using Betasuchus.Application.IntegrationServices.PaymentManagement.Neollet.Configs;
using Betasuchus.Domain.OrderManagement;
using Microsoft.Extensions.Options;
using System;

namespace Betasuchus.Application.IntegrationServices.PaymentManagement
{
    public class PaymentProviderFactory : IPaymentProviderFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public PaymentProviderFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IPaymentProvider ResolvePaymentProvider(PaymentType paymentType, ApplicationContext context)
        {
            switch (paymentType)
            {
                case PaymentType.Card:
                    return new CardPaymentProvider(
                        _serviceProvider.Get<IOptions<CardPaymentProviderConfig>>(),
                        context);
                case PaymentType.NeolletWallet:
                    return new NeolletWalletPaymentProvider(
                        _serviceProvider.Get<IOptions<NeolletPaymentProviderConfig>>(),
                        context);
                case PaymentType.EuropeBetWallet:
                    return new EuropeBetWalletPaymentProvider(
                        _serviceProvider.Get<IOptions<EuropeBetWalletProviderConfig>>(),
                        context);
                default:
                    break;
            }

            return null;
        }
    }
}
