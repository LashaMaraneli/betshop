﻿using Betasuchus.Application.IntegrationServices.BonusHub.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.BonusHub
{
    public interface IBonusHubManager
    {
        Task<CheckStatusResponse> CheckAwardStatus(CheckStatusRequest request);
        Task<BonusResponse> Reward(BonusRequest bonus);
    }
}
