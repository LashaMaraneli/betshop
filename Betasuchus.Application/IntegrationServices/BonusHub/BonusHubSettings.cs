﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.BonusHub
{
    public class BonusHubSettings
    {
        public int Channel { get; set; }

        public string UserName { get; set; }

        public string BaseUrl { get; set; }

        public string BonusAwardUrl { get; set; }

        public string CheckStatusUrl { get; set; }

        public string AwardUrl => BaseUrl + BonusAwardUrl;

        public string StatusUrl => BaseUrl + CheckStatusUrl;
    }
}
