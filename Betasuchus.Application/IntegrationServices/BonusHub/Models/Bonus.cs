﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Betasuchus.Application.IntegrationServices.BonusHub.Models
{
    public class BonusRequest
    {
        [JsonProperty(PropertyName = "channel")]
        public int Channel { get; private set; }

        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; private set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; private set; }

        [JsonProperty(PropertyName = "award")]
        public Dictionary<string, object> Award { get; private set; }

        public BonusRequest(int channel, string userName, string description, Dictionary<string, object> award)
        {
            Channel = channel;
            UserName = userName;
            Description = description;
            Award = award;
        }
    }

    public class BonusResponse
    {
        [JsonProperty(PropertyName = "data")]
        public ResponseData Data { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "isSuccess")]
        public bool Success { get; set; }
    }

    public class ResponseData
    {
        [JsonProperty(PropertyName = "bonusPoolID")]
        public string BonusPoolId { get; set; }

        [JsonProperty(PropertyName = "validationMessages")]
        public string[] ValidationMessages { get; set; }
    }
}
