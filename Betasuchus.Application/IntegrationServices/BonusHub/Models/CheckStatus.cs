﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Betasuchus.Application.IntegrationServices.BonusHub.Models
{
    public class CheckStatusRequest
    {
        public string BonusPoolId { get; set; }
    }

    public class CheckStatusResponse
    {
        [JsonProperty(PropertyName = "data")]
        public AwardStatus Data { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "isSuccess")]
        public bool Success { get; set; }
    }

    public enum AwardStatus
    {
        Success = 1,
        Pending = 2,
        Failed = 3
    }
}
