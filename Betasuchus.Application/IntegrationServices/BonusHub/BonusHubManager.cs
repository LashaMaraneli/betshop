﻿using Betasuchus.Application.IntegrationServices.BonusHub.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.BonusHub
{
    public class BonusHubManager : IBonusHubManager
    {
        private readonly BonusHubSettings _hubSettings;

        public BonusHubManager(IOptions<BonusHubSettings> options)
        {
            _hubSettings = options.Value;
        }

        public async Task<BonusResponse> Reward(BonusRequest request)
        {
            Log.Information("BonusHubManager.Reward Sending Request to Bonus hub - award");

            using var httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

            var content = JsonConvert.SerializeObject(request);
            Log.Information("BonusHubManager.Reward Posting httpContent " + content);

            var httpContent = new StringContent(content);

            try
            {
                var response = await httpClient.PostAsync(_hubSettings.AwardUrl, httpContent);

                response.EnsureSuccessStatusCode();

                var responseContent = await response.Content.ReadAsStringAsync();

                Log.Information("BonusHubManager.Reward Received response from Bonus hub - award: " + responseContent);

                var obj = JsonConvert.DeserializeObject<BonusResponse>(responseContent);

                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "BonusHubManager.Reward Exception received from Bonus hub - award");

                throw;
            }
        }

        public async Task<CheckStatusResponse> CheckAwardStatus(CheckStatusRequest request)
        {
            Log.Information("BonusHubManager.CheckAwardStatus Sending Request to Bonus hub - check");

            using var httpClient = new HttpClient();

            var content = JsonConvert.SerializeObject(request);
            Log.Information("BonusHubManager.CheckAwardStatusgetting httpContent " + content);

            var httpContent = new StringContent(content);

            try
            {
                var response = await httpClient.GetAsync(_hubSettings.CheckStatusUrl + "?BonusPoolId=" + request.BonusPoolId);

                response.EnsureSuccessStatusCode();

                var responseContent = await response.Content.ReadAsStringAsync();

                Log.Information("BonusHubManager.CheckAwardStatus Received response from Bonus hub - check: " + responseContent);

                var obj = JsonConvert.DeserializeObject<CheckStatusResponse>(responseContent);

                return obj;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "BonusHubManager.CheckAwardStatus Exception received from Bonus hub - check");

                throw;
            }
        }
    }
}