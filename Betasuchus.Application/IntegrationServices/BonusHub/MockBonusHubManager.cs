﻿using Betasuchus.Application.IntegrationServices.BonusHub.Models;
using System;
using System.Threading.Tasks;

namespace Betasuchus.Application.IntegrationServices.BonusHub
{
    public class MockBonusHubManager : IBonusHubManager
    {
        public async Task<CheckStatusResponse> CheckAwardStatus(CheckStatusRequest request)
        {
            var rnd = new Random().Next(1, 3);

            var awardStatus = (AwardStatus)rnd;
            var success = awardStatus == AwardStatus.Success;

            return await Task.FromResult(new CheckStatusResponse
            {
                Data = awardStatus,
                Success = success,
                Message = rnd.ToString()
            });
        }

        public async Task<BonusResponse> Reward(BonusRequest bonus)
        {
            return await Task.FromResult(new BonusResponse
            {
                Success = true,
                Data = new ResponseData
                {
                    BonusPoolId = Guid.NewGuid().ToString()
                },
                Message = string.Empty
            });
        }
    }
}
