﻿using Wandio.Domain.Services.Models;

namespace Wandio.Domain
{
    public static class SortOrderExtensions
    {
        public static SortOrder ParseAsSortOrder(this string what, SortOrder defaultValue = SortOrder.Asc)
        {
            var whatToLower = what?.ToLower();
            switch (whatToLower)
            {
                case SortingConstants.Asc:
                case SortingConstants.AscLong:
                    return SortOrder.Asc;
                case SortingConstants.Desc:
                case SortingConstants.DescLong:
                    return SortOrder.Desc;
                default:
                    return defaultValue;
            }
        }
    }
}