﻿namespace Wandio.Domain
{
    public static class SortingConstants
    {
        public const string Asc = "asc";

        public const string AscLong = "ascending";

        public const string Desc = "desc";

        public const string DescLong = "descending";
    }
}