﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Wandio.Domain.Services.Models;

namespace Wandio.Domain.Services
{
    public static class QueryableFilteringExtensions
    {
        public static IQueryable<TSource> Filter<TSource>(
            this IQueryable<TSource> source,
            string input,
            Expression<Func<TSource, string>> property)
            where TSource : class
        {
            if (property == null)
            {
                throw new ArgumentNullException(nameof(property));
            }

            if (property.Body.NodeType != ExpressionType.MemberAccess)
            {
                throw new ArgumentException("expression should be of member access");
            }

            if (!string.IsNullOrWhiteSpace(input))
            {
                var targetType = Expression.Parameter(typeof(TSource), property.Parameters[0].Name);
                var targetProperty = Expression.Property(targetType, ((MemberExpression)property.Body).Member.Name);
                var stringContains = typeof(string).GetMethod("Contains", new[] { typeof(string) });

                var body = Expression.Call(targetProperty, stringContains, Expression.Constant(input));

                var lambda = Expression.Lambda<Func<TSource, bool>>(body, targetType);

                return source.And(lambda);
            }

            return source;
        }

        public static IQueryable<TSource> Filter<TSource>(
            this IQueryable<TSource> source,
            UnboundedPeriod period,
            Expression<Func<TSource, DateTime>> property)
        {
            if (period != null)
            {
                return Filter(source, period.StartDate, period.EndDate, property);
            }

            return source;
        }

        public static IQueryable<TSource> Filter<TSource>(
            this IQueryable<TSource> source,
            UnboundedPeriod period,
            Expression<Func<TSource, DateTime?>> property)
        {
            if (period != null)
            {
                return Filter(source, period.StartDate, period.EndDate, property);
            }

            return source;
        }

        public static IQueryable<TSource> Filter<TSource, TStruct>(
            this IQueryable<TSource> source,
            UnboundedRange<TStruct> range,
            Expression<Func<TSource, TStruct?>> property)
            where TStruct : struct
        {
            if (range != null)
            {
                return Filter(source, range.From, range.To, property);
            }

            return source;
        }

        public static IQueryable<TSource> Filter<TSource, TStruct>(
            this IQueryable<TSource> source,
            UnboundedRange<TStruct> range,
            Expression<Func<TSource, TStruct>> property)
            where TStruct : struct
        {
            if (range != null)
            {
                return Filter(source, range.From, range.To, property);
            }

            return source;
        }

        public static IQueryable<TSource> Filter<TSource, TStruct>(
            this IQueryable<TSource> source,
            TStruct? min,
            TStruct? max,
            Expression<Func<TSource, TStruct?>> property)
            where TStruct : struct
        {
            if (property == null)
            {
                throw new ArgumentNullException(nameof(property));
            }

            if (!new[] { ExpressionType.MemberAccess, ExpressionType.Convert }.Contains(property.Body.NodeType))
            {
                throw new ArgumentException("expression should be of member access");
            }

            if (!min.HasValue && !max.HasValue)
            {
                return source;
            }

            var targetType = Expression.Parameter(typeof(TSource), property.Parameters[0].Name);
            var targetProperty = Expression.PropertyOrField(targetType, ((MemberExpression)property.Body).Member.Name);

            if (min.HasValue)
            {
                var body = Expression.LessThanOrEqual(
                    Expression.Constant(min),
                    Expression.Convert(targetProperty, typeof(TStruct)));
                var lambda = Expression.Lambda<Func<TSource, bool>>(body, targetType);

                source = source.And(lambda);
            }

            if (max.HasValue)
            {
                var body = Expression.GreaterThan(
                    Expression.Constant(max),
                    Expression.Convert(targetProperty, typeof(TStruct)));
                var lambda = Expression.Lambda<Func<TSource, bool>>(body, targetType);

                source = source.And(lambda);
            }

            return source;
        }

        public static IQueryable<TSource> Filter<TSource, TStruct>(
            this IQueryable<TSource> source,
            TStruct? minValue,
            TStruct? maxValue,
            Expression<Func<TSource, TStruct>> property)
            where TStruct : struct
        {
            if (property == null)
            {
                throw new ArgumentNullException(nameof(property));
            }

            if (!new[] { ExpressionType.MemberAccess, ExpressionType.Convert }.Contains(property.Body.NodeType))
            {
                throw new ArgumentException("expression should be of member access");
            }

            if (!minValue.HasValue && !maxValue.HasValue)
            {
                return source;
            }

            var targetType = Expression.Parameter(typeof(TSource), property.Parameters[0].Name);
            var targetProperty = Expression.PropertyOrField(targetType, ((MemberExpression)property.Body).Member.Name);

            if (minValue.HasValue)
            {
                var body = Expression.LessThanOrEqual(Expression.Constant(minValue), targetProperty);
                var lambda = Expression.Lambda<Func<TSource, bool>>(body, targetType);

                source = source.And(lambda);
            }

            if (maxValue.HasValue)
            {
                var body = Expression.GreaterThan(Expression.Constant(maxValue), targetProperty);
                var lambda = Expression.Lambda<Func<TSource, bool>>(body, targetType);

                source = source.And(lambda);
            }

            return source;
        }

        public static IQueryable<TSource> Filter<TSource, TTarget>(
            this IQueryable<TSource> source,
            IEnumerable<TTarget> input,
            Expression<Func<TSource, TTarget>> property)
        {
            if (property == null)
            {
                throw new ArgumentNullException(nameof(property));
            }

            if (property.Body.NodeType != ExpressionType.MemberAccess)
            {
                throw new ArgumentException("expression should be of member access");
            }

            if (!(source == null) && source.Count() > 0)
            {
                var targetType = Expression.Parameter(typeof(TSource), property.Parameters[0].Name);
                var targetProperty = Expression.PropertyOrField(
                    targetType,
                    ((MemberExpression)property.Body).Member.Name);
                var enumerableContains =
                    typeof(Enumerable).GetMethods()
                        .Single(x => x.Name.Equals("Contains") && x.GetParameters().Count() == 2)
                        .MakeGenericMethod(typeof(TTarget));

                var body = Expression.Call(enumerableContains, Expression.Constant(source), targetProperty);

                var lambda = Expression.Lambda<Func<TSource, bool>>(body, targetType);
                return source.And(lambda);
            }

            return source;
        }

        public static IQueryable<TSource> Filter<TSource>(
            this IQueryable<TSource> source,
            bool? input,
            Expression<Func<TSource, bool>> property)
        {
            if (property == null)
            {
                throw new ArgumentNullException(nameof(property));
            }

            if (property.Body.NodeType != ExpressionType.MemberAccess)
            {
                throw new ArgumentException("expression should be of member access");
            }

            if (input.HasValue)
            {
                var targetType = Expression.Parameter(typeof(TSource), property.Parameters[0].Name);
                var targetProperty = Expression.PropertyOrField(
                    targetType,
                    ((MemberExpression)property.Body).Member.Name);

                var body = Expression.Equal(targetProperty, Expression.Constant(input.Value));

                var lambda = Expression.Lambda<Func<TSource, bool>>(body, targetType);
                return source.And(lambda);
            }

            return source;
        }

        public static IQueryable<TSource> And<TSource, TStruct>(
            this IQueryable<TSource> source,
            UnboundedRange<TStruct> range,
            Expression<Func<TSource, bool>> predicate)
            where TStruct : struct
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            if (range == null)
            {
                return source;
            }

            if (!range.From.HasValue && !range.To.HasValue)
            {
                return source;
            }

            if (range != null)
            {
                return source.And(predicate);
            }

            return source;
        }

        public static IQueryable<TSource> And<TSource>(
            this IQueryable<TSource> source,
            UnboundedPeriod period,
            Expression<Func<TSource, bool>> property)
        {
            if (property == null)
            {
                throw new ArgumentNullException("predicate");
            }

            if (period == null)
            {
                return source;
            }

            if (!period.StartDate.HasValue && !period.EndDate.HasValue)
            {
                return source;
            }

            if (period != null)
            {
                return source.And(property);
            }

            return source;
        }

        public static IQueryable<TSource> And<TSource, TFilter>(
            this IQueryable<TSource> source,
            TFilter? filter,
            Expression<Func<TSource, bool>> predicate)
            where TFilter : struct
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            if (filter.HasValue)
            {
                return source.And(predicate);
            }

            return source;
        }

        public static IQueryable<TSource> And<TSource>(
            this IQueryable<TSource> source,
            string filter,
            Expression<Func<TSource, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            if (!string.IsNullOrWhiteSpace(filter))
            {
                return source.And(predicate);
            }

            return source;
        }

        public static IQueryable<TSource> And<TSource>(
            this IQueryable<TSource> source,
            IEnumerable input,
            Expression<Func<TSource, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            if (input != null && input.GetEnumerator().MoveNext())
            {
                return source.And(predicate);
            }

            return source;
        }

        public static IQueryable<TSource> And<TSource>(
            this IQueryable<TSource> source,
            Expression<Func<TSource, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            return source.Where(predicate);
        }

        public static IQueryable<TSource> Sort<TSource>(
            this IQueryable<TSource> source,
            string sortBy,
            SortOrder sortOrder)
            where TSource : class
        {
            string sort;
            if (!string.IsNullOrWhiteSpace(sort = string.Format("{0} {1}", sortBy, sortOrder)))
            {
                return source.OrderBy(it => sort);
            }

            return source;
        }

        public static IQueryable<TSource> Sort<TSource>(
            this IQueryable<TSource> source,
            SortedAndPagedListRequestBase request)
            where TSource : class
        {
            if (request != null)
            {
                return Sort(source, request.SortBy, request.SortOrder);
            }

            return source;
        }

        public static IQueryable<TSource> SortAndPage<TSource>(
            this IQueryable<TSource> source,
            SortedAndPagedListRequestBase request)
            where TSource : class
        {
            return Page(source.Sort(request), request);
        }

        public static IQueryable<TSource> Page<TSource>(this IQueryable<TSource> source, PagedListRequestBase request)
            where TSource : class
        {
            if (request == null)
            {
                return source;
            }

            return source.Skip(request.PageSize * (request.Page - 1)).Take(request.PageSize);
        }
    }
}