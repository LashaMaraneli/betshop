﻿namespace Wandio.Domain.Services
{
    public class UnboundedPeriodTimeStamp
    {
        public UnboundedPeriodTimeStamp(long? startDate = null, long? endDate = null)
        {
            StartDate = startDate;
            EndDate = endDate;
        }

        public long? StartDate { get; set; }

        public long? EndDate { get; set; }
    }
}
