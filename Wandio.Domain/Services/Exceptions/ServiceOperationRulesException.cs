﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wandio.Domain.Services.Exceptions
{
    // [Serializable]
    public class ServiceOperationRulesException : Exception
    {
        private readonly IEnumerable<Tuple<string, IEnumerable<string>>> _entities;

        public ServiceOperationRulesException()
        {
        }

        public ServiceOperationRulesException(string message)
            : base(message)
        {
        }

        public ServiceOperationRulesException(string message, Exception inner)
            : base(message, inner)
        {
        }

        // protected ServiceOperationRulesException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public ServiceOperationRulesException(string message, IEnumerable<Tuple<string, IEnumerable<string>>> entities)
            : base(message)
        {
            _entities = entities;
        }

        public IEnumerable<string> GetDetails(bool asList = false)
        {
            if (asList)
            {
                return _entities.SelectMany(item => item.Item2);
            }

            return _entities.Select(item => string.Format("{0} - {1}", item.Item1, string.Join(", ", item.Item2)));
        }
    }
}