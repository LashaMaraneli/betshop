﻿namespace Wandio.Domain.Services.Models
{
    public class SortedAndPagedListRequestBase : PagedListRequestBase
    {
        public virtual string SortBy { get; set; }

        public virtual SortOrder SortOrder { get; set; }
    }
}