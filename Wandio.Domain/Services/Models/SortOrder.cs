﻿namespace Wandio.Domain.Services.Models
{
    public enum SortOrder
    {
        Asc = 1,
        Desc = 2,
    }
}