﻿namespace Wandio.Domain.Services.Models
{
    public class PagedListRequestBase
    {
        public int PageSize { get; set; } = 10;

        public int Page { get; set; } = 1;
    }
}