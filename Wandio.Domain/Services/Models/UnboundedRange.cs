﻿namespace Wandio.Domain.Services.Models
{
    public class UnboundedRange<TStruct>
        where TStruct : struct
    {
        public TStruct? From { get; set; }

        public TStruct? To { get; set; }
    }
}