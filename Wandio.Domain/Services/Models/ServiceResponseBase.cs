﻿using System.Collections.Generic;

namespace Wandio.Domain.Services.Models
{
    public class ServiceResponseBase
    {
        public bool Ok { get; set; }

        public IEnumerable<string> Errors { get; set; }
    }
}