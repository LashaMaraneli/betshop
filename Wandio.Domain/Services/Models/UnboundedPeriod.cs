﻿using System;
using System.Text;

namespace Wandio.Domain.Services.Models
{
    public class UnboundedPeriod
    {
        public UnboundedPeriod(DateTime? start = null, DateTime? end = null)
        {
            StartDate = start;
            EndDate = end;
        }

        public UnboundedPeriod()
        {
        }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}