﻿namespace Wandio.Domain.Services.Models
{
    public class PagedListResponseBase
    {
        public int TotalCount { get; set; }
    }
}