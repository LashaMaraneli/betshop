﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wandio.Domain.Services.Exceptions;

namespace Wandio.Domain.Services
{
    public class RuleEnforcer<TEntity>
    {
        private readonly List<Tuple<Func<TEntity, bool>, string>> _rules = new List<Tuple<Func<TEntity, bool>, string>>();

        private static string generalErrorMessage = "Unknown error";

        private string generalMessage;

        public RuleEnforcer()
        {
        }

        public RuleEnforcer(string generalMessage)
        {
            this.generalMessage = generalMessage;
        }

        public RuleEnforcer<TEntity> And(RuleEnforcer<TEntity> source)
        {
            _rules.AddRange(source._rules);

            return this;
        }

        public RuleEnforcer<TEntity> Rules(List<Tuple<Func<TEntity, bool>, string>> rules)
        {
            _rules.AddRange(rules);

            return this;
        }

        /// <summary>
        ///     Add rule to enforcer.
        /// </summary>
        /// <param name="rule">Func that should return true if this rule if ok.</param>
        /// <param name="message">Error message when rule check fails.</param>
        /// <returns>RuleEnforcer.</returns>
        public RuleEnforcer<TEntity> Rule(Func<TEntity, bool> rule, string message)
        {
            _rules.Add(new Tuple<Func<TEntity, bool>, string>(rule, message));

            return this;
        }

        public void Enforce(IEnumerable<TEntity> items)
        {
            var ruleViolations = new Dictionary<TEntity, IEnumerable<string>>();
            foreach (var entity in items)
            {
                var violations = _rules.Where(rule => !rule.Item1(entity)).Select(ruleViolation => ruleViolation.Item2);
                if (violations.Any())
                {
                    ruleViolations.Add(entity, violations);
                }
            }

            if (ruleViolations.Any())
            {
                throw new ServiceOperationRulesException(
                    generalMessage ?? generalErrorMessage,
                    ruleViolations.Select(x => new Tuple<string, IEnumerable<string>>(x.Key.ToString(), x.Value)));
            }
        }

        public void Enforce(TEntity entity)
        {
            var ruleViolations = new Dictionary<TEntity, IEnumerable<string>>();
            var violations = _rules.Where(rule => !rule.Item1(entity)).Select(ruleViolation => ruleViolation.Item2);
            if (violations.Any())
            {
                ruleViolations.Add(entity, violations);
            }

            if (ruleViolations.Any())
            {
                throw new ServiceOperationRulesException(
                    generalMessage ?? generalErrorMessage,
                    ruleViolations.Select(x => new Tuple<string, IEnumerable<string>>(x.Key.ToString(), x.Value)));
            }
        }

        public bool Check(TEntity entity)
        {
            return _rules.All(rule => rule.Item1(entity));
        }
    }
}