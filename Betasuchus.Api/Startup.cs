using Betasuchus.Application.Commands.OrderManagement;
using Betasuchus.DI;
using FluentValidation.AspNetCore;
using IdentityServer4.AccessTokenValidation;
using JsonNet.ContractResolvers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Claims;

namespace Betasuchus.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Resolve(Env, Configuration);

            services.AddCors(policy =>
            {
                policy.AddDefaultPolicy(t =>
                {
                    t.AllowAnyHeader();
                    t.AllowAnyMethod();
                    //t.AllowCredentials();
                    t.AllowAnyOrigin();
                });
            });

            var policy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();

            services.AddAuthorization(opts =>
            {
                // TODO Add possible policies.
                opts.DefaultPolicy = policy;
            });

            //services.AddAuthentication("Bearer")
            //    .AddIdentityServerAuthentication(options =>
            //    {
            //        options.Authority = "http://localhost";
            //        options.RequireHttpsMetadata = false;
            //        options.ApiName = "Betshop";
            //        //options.ApiName = "VFHfneQeozF19qBLQCCAdb7guQ3GrwBC/bqMl4aV74510S8Gp8M6+pPkB9tXZAeH";

            //        options.IntrospectionDiscoveryPolicy = new IdentityModel.Client.DiscoveryPolicy()
            //        {
            //            RequireHttps = false,
            //            Authority = "http://localhost",
            //        };

            //        options.SupportedTokens = SupportedTokens.Both;

            //        options.TokenRetriever =
            //            x => x.Headers["Authorization"].ToString();
            //    });

            services.AddAuthentication("bearer")
                .AddJwtBearer("bearer", options =>
                {
                    options.Authority = "http://localhost";
                    options.RequireHttpsMetadata = false;

                    options.TokenValidationParameters.ValidTypes = new[] { "at+jwt" };

                    // if token does not contain a dot, it is a reference token
                    //options.ForwardDefaultSelector = x => "introspection";
                });
            //.AddOAuth2Introspection("introspection", options =>
            //{
            //    options.Authority = "http://localhost";
            //});

            services.AddControllers()
                .AddFluentValidation(x =>
                {
                    x.RegisterValidatorsFromAssemblyContaining<CreateOrderCommand>(lifetime: ServiceLifetime.Singleton);
                    x.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                });

            services.AddSession();

            var georgianCultureInfo = new CultureInfo("ka-GE");
            var englishCultureInfo = new CultureInfo("en-US");
            var russianCultureInfo = new CultureInfo("ru-RU");
            var turkishCultureInfo = new CultureInfo("tr-TR");

            var supportedCultures = new List<CultureInfo>
            {
                georgianCultureInfo,
                englishCultureInfo,
                russianCultureInfo,
                turkishCultureInfo
            };

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture(georgianCultureInfo);
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            services.AddLocalization();

            services.AddDistributedMemoryCache(x =>
            {
                x.ExpirationScanFrequency = TimeSpan.FromHours(24);
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Betshop Api",
                    Description = "",
                    Contact = new OpenApiContact
                    {
                        Name = string.Empty,
                        Email = string.Empty
                    }
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme."
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}
                    }
                });

                c.OperationFilter<LangParameter>();
            });

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                TypeNameHandling = TypeNameHandling.Objects,
                ContractResolver = new PrivateSetterContractResolver()
            };
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();


            app.UseRouting();

            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);
            app.UseMiddleware<LocalizationMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", "API V1");
                // For Model Section
                c.DefaultModelsExpandDepth(-1);
            });

            Migrate(app);

            Seed(app);
        }

        private static void Migrate(IApplicationBuilder app)
        {
            //using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            //serviceScope.ServiceProvider.GetService<BetasuchusDbContext>().Database.Migrate();
        }

        private static void Seed(IApplicationBuilder app)
        {
            //using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            //var db = serviceScope.ServiceProvider.GetService<DoggosaurusDbContext>();
        }
    }

    public class LangParameter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
            {
                operation.Parameters = new List<OpenApiParameter>();
            }

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "lang",
                In = ParameterLocation.Header,
                Required = false
            });
        }
    }
}
