﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Betasuchus.Api.Controllers
{
    public class FilterController : Controller
    {
        private readonly QueryExecutor _queryExecutor;

        public FilterController(
            QueryExecutor queryExecutor)
        {
            _queryExecutor = queryExecutor;
        }

        [HttpGet("GetFilters")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(QueryFilterResult))]
        public async Task<IActionResult> GetFilters([FromQuery] QueryFilter query)
        {
            return await Task.FromResult(Json(await _queryExecutor.ExecuteAsync<QueryFilter, QueryFilterResult>(query)));
        }
    }
}
