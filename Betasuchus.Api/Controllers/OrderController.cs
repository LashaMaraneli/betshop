﻿using Betasuchus.Application.Commands.OrderManagement;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.OperationManagement;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Betasuchus.Api.Controllers
{
    [Route("/Order")]
    [EnableCors]
    public class OrderController : Controller
    {
        private readonly CommandExecutor _commandExecutor;
        private readonly QueryExecutor _queryExecutor;

        public OrderController(
            CommandExecutor commandExecutor,
            QueryExecutor queryExecutor)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
        }

        [HttpPost("Create")]
        [Authorize]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(CommandExecutionResult))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create([FromBody] CreateOrderCommand command)
        {
            if (!ModelState.IsValid)
            {
                return await Task.FromResult(BadRequest(ModelState));
            }

            var result = await _commandExecutor.ExecuteAsync(command);

            if (result.Success)
            {
                return await Task.FromResult(Json(result.Data));
            }

            return await Task.FromResult(Json(result));
        }

        [HttpGet("Check")]
        [AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(QueryTransactionsResult))]
        public async Task<IActionResult> Check(QueryTransactions query)
        {
            var result = await _queryExecutor.ExecuteAsync<QueryTransactions, QueryTransactionsResult>(query);

            return await Task.FromResult(Json(result.Data));
        }
    }
}
