﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Offers;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Betasuchus.Api.Controllers
{
    public class OfferController : Controller
    {
        private readonly QueryExecutor _queryExecutor;

        public OfferController(
            QueryExecutor queryExecutor)
        {
            _queryExecutor = queryExecutor;
        }

        [HttpGet("GetOffers")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(OffersFilterQueryResult))]
        public async Task<IActionResult> GetOffers([FromQuery] OffersFilterQuery query)
        {
            query.SortBy = "Id";
            query.SortOrder = Shared.SortOrder.Desc;

            return await Task.FromResult(Json(await _queryExecutor.ExecutePagedQueryAsync<OffersFilterQuery, OffersFilterQueryResult>(query)));
        }
    }
}
