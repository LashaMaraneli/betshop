﻿using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Offers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Betasuchus.Api.Controllers
{
    [Route("/Navigation")]
    [EnableCors]
    [AllowAnonymous]
    public class CategoryController : Controller
    {
        private readonly QueryExecutor _queryExecutor;

        public CategoryController(
            QueryExecutor queryExecutor)
        {
            _queryExecutor = queryExecutor;
        }

        [HttpGet("GetCategories")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(QueryNavigationResult))]
        public async Task<IActionResult> GetCategories()
        {
            return await Task.FromResult(Json(await _queryExecutor.ExecuteAsync<QueryNavigation, QueryNavigationResult>(new QueryNavigation())));
        }
    }
}