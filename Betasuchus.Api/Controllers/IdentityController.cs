﻿using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Betasuchus.Api.Controllers
{
    [Authorize]
    [Route("/Identity")]
    [EnableCors]
    public class IdentityController : Controller
    {
        [HttpGet("Token")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> IdentityToken()
        {
            var client = new HttpClient();
            var disco = await client.GetDiscoveryDocumentAsync("http://localhost");

            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", Request.Headers["Authorization"].ToString());
            var userResponse = await client.GetAsync(disco.UserInfoEndpoint);

            userResponse.EnsureSuccessStatusCode();

            return Content(await userResponse.Content.ReadAsStringAsync());
        }
    }
}
