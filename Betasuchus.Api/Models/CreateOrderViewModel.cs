﻿using Betasuchus.Domain.OrderManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Api.Models
{
    public class CreateOrderViewModel
    {
        public long CardId { get; set; }

        public string Card { get; set; }

        public long CustomerId { get; set; }

        public string Customer { get; set; }

        public string CustomerPin { get; set; }

        public PaymentType PaymentType { get; set; }

        public long OfferId { get; set; }
    }
}
