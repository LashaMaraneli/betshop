﻿using Microsoft.AspNetCore.Http;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Betasuchus.Api
{
    public class LocalizationMiddleware
    {
        public readonly RequestDelegate _next;

        public LocalizationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var defaultCulture = new CultureInfo("ka-GE");

            var requestCulture = context.Request.Headers["lang"].ToString();

            CultureInfo culture;

            switch (requestCulture.ToLower())
            {
                case "ka":
                case "ka-ge": culture = new CultureInfo("ka-GE"); break;

                case "en":
                case "en-us": culture = new CultureInfo("en-US"); break;

                case "ru":
                case "ru-ru": culture = new CultureInfo("ru-RU"); break;

                case "tr":
                case "tr-tr": culture = new CultureInfo("tr-TR"); break;

                default: culture = new CultureInfo("ka-GE"); break;
            }

            culture.NumberFormat.NumberDecimalSeparator = ".";
            culture.NumberFormat.CurrencyDecimalSeparator = ".";
            culture.NumberFormat.PercentDecimalSeparator = ".";

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            await _next.Invoke(context);
        }
    }
}
