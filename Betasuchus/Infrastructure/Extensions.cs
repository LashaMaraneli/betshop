﻿using Betasuchus.Admin.Models.Shared.KtCommon;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Betasuchus.Admin.Infrastructure
{
    public static class Extensions
    {
        public static JsonResult ToDataSourceResult(this SortedAndPagedListRequestBase request, object data)
        {
            var model = new KtDatatableResult
            {
                Data = data,
                Meta = new Meta
                {
                    SortBy = request.SortBy,
                    SortOrder = request.SortOrder.ToString().ToLower(),
                    PageSize = request.PageSize,
                    Page = request.Page,
                    Total = request.Total
                }
            };

            return new JsonResult(model);
        }

        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (request.Headers == null)
            {
                return false;
            }

            return request.Headers["X-Requested-With"] == "XMLHttpRequest";
        }

        public static JsonResult JsonSuccess(this Controller controller, object additionalData = null)
        {
            return controller.Json(new { success = true, additionalData });
        }

        public static JsonResult JsonFail(this Controller controller, object additionalData = null)
        {
            return controller.Json(new { success = false, additionalData });
        }

        public static void SetSuccessMessage(this Controller controller, string success = "")
        {
            if (string.IsNullOrEmpty(success))
            {
                success = "Success";
            }

            controller.TempData["MessageType"] = "success";
            controller.TempData["SuccessMessage"] = success;
        }

        public static void SetErrorMessage(this Controller controller, string error = "")
        {
            if (string.IsNullOrEmpty(error))
            {
                error = "Error";
            }

            controller.TempData["MessageType"] = "error";
            controller.TempData["ErrorMessage"] = error;
        }

        public static string JoinStrings(this IEnumerable<string> strings, string separator = ",")
        {
            return string.Join(separator, strings);
        }

        public static IEnumerable<SelectListItem> EnumToSelectList<T>(this Controller controller, Func<T, bool> predicate = null) where T : struct, IComparable, IConvertible
        {
            var values = Enum.GetValues(typeof(T)).Cast<T>();

            if (predicate != null)
            {
                values = values.Where(predicate);
            }

            return values.Select(x => new SelectListItem
            {
                Text = x.ToString(),
                Value = x.ToString()
            });
        }

        public static IEnumerable<SelectListItem> ToSelectList<T>(this Controller controller, IEnumerable<T> source, Func<T, string> text, Func<T, string> value)
        {
            foreach (var itm in source)
            {
                yield return new SelectListItem
                {
                    Text = text(itm),
                    Value = value(itm)
                };
            }
        }

        public static Dictionary<string, object> ObjectToDictionary(this object obj)
        {
            var dictionary = new Dictionary<string, object>();
            var props = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                try
                {
                    dictionary.Add(prop.Name, prop.GetValue(obj));
                }
                catch
                {
                }
            }

            return dictionary;
        }
    }
}
