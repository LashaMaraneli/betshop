﻿using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Betasuchus.Admin.Infrastructure
{
    public static class ExportToExcel
    {
        public static async Task<IActionResult> ToExcelFile<T>(this Controller controller, ExportToExcelRequest<T> request)
        {
            // using non commercial licence
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            MemoryStream stream = new MemoryStream();

            using var excel = new ExcelPackage();

            var sheet = excel.Workbook.Worksheets.Add("Report sheet");

            var headers = new List<string[]>{
                GetHeaders<T>()
            };

            var headerRange = "A1:A" + (headers[0].Count() + 1);
            sheet.Cells[headerRange].LoadFromArrays(headers);

            var data = GetData(request.Data);

            var cellsRange = "B1:B" + ((data.Count + 1) * (headers[0].Count() + 1));
            sheet.Cells[cellsRange].LoadFromCollection(request.Data);

            await excel.SaveAsAsync(stream);

            stream.Seek(0, SeekOrigin.Begin);

            return controller.File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", request.FileName);
        }

        private static string[] GetHeaders<T>() =>
            typeof(T)
            .GetProperties(BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Instance)
            .Select(x => x.Name).ToArray();

        private static List<List<object>> GetData<T>(IEnumerable<T> data)
        {
            var list = new List<List<object>>();

            Parallel.ForEach(data, (item) =>
            {
                list.Add(ItemToObjectList(item));
            });

            return list;
        }

        private static List<object> ItemToObjectList<T>(T obj) =>
            typeof(T)
            .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty)
            .Select(x => x.GetValue(obj))
            .ToList();
    }

    public class ExportToExcelRequest<T>
    {
        public ExportToExcelRequest(string fileName,
            IEnumerable<T> data)
        {
            FileName = fileName;
            Data = data;
        }

        public IEnumerable<T> Data { get; set; }

        public string FileName { get; set; }
    }
}
