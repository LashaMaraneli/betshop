﻿using Betasuchus.Application.Infrastructure;

namespace Betasuchus.Admin.Infrastructure
{
    public interface IViewModel
    {
        public Command ToCommand();
    }
}
