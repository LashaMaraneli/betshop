﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Betasuchus.Admin.Infrastructure
{
    public class SortedAndPagedListRequestBaseModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            Task bindValueByProperty(string keyword)
            {
                var valueProviderResult = bindingContext.ValueProvider.GetValue(keyword);
                if (valueProviderResult == ValueProviderResult.None)
                {
                    return Task.CompletedTask;
                }

                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);

                var value = valueProviderResult.FirstValue;

                if (string.IsNullOrEmpty(value))
                {
                    return Task.CompletedTask;
                }

                var typeConvertor = TypeDescriptor.GetConverter(bindingContext.ModelType);
                var convertedValue = typeConvertor.ConvertFromInvariantString(value);

                bindingContext.Result = ModelBindingResult.Success(convertedValue);

                return Task.CompletedTask;
            }

            switch (bindingContext.ModelName)
            {
                case "query.Page":
                case "Page":
                    return bindValueByProperty("pagination[page]");
                case "query.PageSize":
                case "PageSize":
                    return bindValueByProperty("pagination[perpage]");
                case "query.SortBy":
                case "SortBy":
                    return bindValueByProperty("sort[field]");
                case "query.SortOrder":
                case "SortOrder":
                    return bindValueByProperty("sort[sort]");
            }

            return Task.CompletedTask;
        }
    }
}
