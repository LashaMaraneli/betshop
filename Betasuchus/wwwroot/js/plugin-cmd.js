﻿var PluginCmd = function () {

    var handleSelect2 = function () {
        $(".js-select2").select2({
            placeholder: ""
        });

        var select2SearchItems = $(".js-select2-search");
        if (select2SearchItems) {
            $.each(select2SearchItems.toArray(), function (index, element) {
                var isAsigned = $(element).data('assigned');
                if (!isAsigned) {
                    $(element).select2({
                        placeholder: function () {
                            $(this).data('placeholder');
                        },
                        language: {
                            noResults: function () {
                                return "";
                            }
                        },
                        minimumResultsForSearch: 5
                    });

                    $(element).data('assigned', true)
                }
            });
        }

        $(".js-select2-clearable").select2({
            placeholder: function () {
                $(this).data('placeholder');
            },
            language: {
                noResults: function () {
                    return "";
                }
            },
            minimumResultsForSearch: 5,
            allowClear: true
        });
    };

    var handleGlobalKTAppSettings = function () {
        return {
            "breakpoints":
                { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 },
            "colors": {
                "theme": {
                    "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" },
                    "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" },
                    "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" }
                },
                "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" }
            }, "font-family": "Poppins"
        };
    };

    var handleDeleteConfirmAction = function () {
        $(document).on('click', 'js-delete-action', function (e) {
            e.preventDefault();
            var $this = $(this);
            var action = $this.attr('href');
            if (!action) {
                action = $this.data('url');
            }

            if (action) {
                handleDeleteConfirmActionBase(action);
            }
        });
    };

    var handleDeleteConfirmActionBase = function (action, data, successCallback, cancelCallbak) {
        Swal.fire({
            title: 'Are you sure you want to continue?',
            text: 'Can not revert this action',
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: action,
                    type: 'POST',
                    data: data,
                    success: function (result) {
                        if (result.success) {
                            if (successCallback) {
                                successCallback(result);
                            }
                        } else {
                            var message = 'Could not delete';

                            if (result.additionalData) {
                                message = result.additionalData;
                            }

                            Swal.fire(
                                locals.CanNotDelete,
                                message,
                                "error"
                            )
                        }
                    },
                    error: function (xhr) {
                        var message = 'Something went wrong';

                        if (xhr.responseText) {

                        }
                    }
                });
            }
        });
    };

    return {
        init: function () {
            this.initSelect2();
        },
        returnGlobalKTAppSettings: function () {
            return handleGlobalKTAppSettings();
        },
        initSelect2: function () {
            handleSelect2();
        },
        initHandleDeleteConfirmAction: function () {
            handleDeleteConfirmAction();
        },
        initHandleDeleteConfirmActionBase: function (action, data, successCallback, cancelCallbak) {
            handleDeleteConfirmActionBase(action, data, successCallback, cancelCallbak);
        }
    }
}();