﻿"use strict";

var KtDatatable = function () {
    var setUpTable = function (data) {
        var dataTableName = data.DatatableName;

        var card = $('#' + dataTableName).closest('.container-fluid');
        if (card) {

            var searchForm = card.prev('.kt-search-form');

            if (searchForm) {
                var searchFormhtml = searchForm.html();

                var tableSearchForm = $('#search-' + dataTableName);
                if (tableSearchForm) {
                    tableSearchForm.append(searchFormhtml);

                    var selectItems = tableSearchForm.find('select');
                    if (selectItems) {
                        $.each(selectItems, function (index, element) {
                            $(element).select2({
                                placeholder: function () {
                                    $(this).data('placeholder');
                                },
                                language: {
                                    noResults: function () {
                                        return "";
                                    }
                                },
                                minimumResultsForSearch: 5
                            });
                        });
                    }

                    $('.select2.select2-container').width('100%');
                }

                $(document).on('click', '.js-modal-filter', function () {
                    var $modal = tableSearchForm.find('.modal');
                    $modal.modal();
                });
            }
            else {
                tableSearchForm.remove();
            }
        }

        generateTable(data, dataTableName);
        handleDeleteAction();
    }

    var generateTable = function (model, tableId) {
        var fn_getColumns = KtDatatableCmd[model.ColumnFunctionName];

        var columns = fn_getColumns();
        $.nonSubTablesRowIds = [];

        if ((model.EditAction && !model.EditAction.Hide) || (model.DeleteAction && !model.DeleteAction.Hide) || (model.AdditionalActions && !model.AdditionalActions.Hide)) {

            var actionField = {
                field: 'Actions',
                title: locals.Actions,
                sortable: false,
                width: 125,
                overflow: 'visible',
                autoHide: false,
                template: function (row) {
                    var template = '';

                    if (model.AdditionalActions) {
                        template += KtDatatableCmd.getAdditionalActionTemplate(model.AdditionalActions, row);
                    }

                    if (model.EditAction) {

                        if (!model.EditAction.Hide) {
                            var editActionUrl = model.EditAction.ActionUrl + '/' + row.Id;
                            var editActionTemplate = KtDatatableCmd.getEditActionTemplate(editActionUrl, model.EditAction, model.EditAction, row)
                            template += editActionTemplate;
                        }
                    }

                    if (model.DeleteAction) {
                        if (!model.DeleteAction.Hide) {
                            var deleteActionUrl = model.DeleteAction.ActionUrl + '/' + row.Id;
                            var deleteActionTemplate = KtDatatableCmd.getDeleteActionTemplate(deleteActionUrl, model.DeleteAction.CustomClassName, model.DeleteAction, row);
                            template += deleteActionTemplate;

                        }
                    }

                    return template;
                }
            };

            columns.push(actionField);
        }

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: model.ReadUrl,
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }

                            return dataSet;
                        }
                    }
                },
                saveState: false,
                pageSize: model.PageSize,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            layout: {
                scroll: model.Scrollable,
                footer: false
            },
            toolbar: {
                items: {
                    pagination: {
                        pageSizeSelect: model.PageSizes
                    }
                }
            },
            sortable: true,
            pagination: true,
            columns: columns,
            scrollX: true,
        };

        if (model.ChildTable) {
            options.detail = {
                content: setupChildTable(model.ChildTable)
            }
        }

        var table = $('#' + tableId).KTDatatable(options);

        var tableSearchForm = $('#search-' + tableId);

        if (tableSearchForm) {
            var searchFields = tableSearchForm.find('.js-search-field');

            if (searchFields) {

                $.each(searchFields, function (index, element) {

                    var value = $(element).val();

                    if ((element.type === "select-one" || element.type !== "select-multipl") && value.length > 0 && value !== "0") {
                        var name = $(element).attr('name');
                        table.search(value, name);
                    }

                    $(element).on('change', function () {
                        var name = $(this).attr('name');
                        var value = $(this).val();

                        if (typeof name !== 'undefined' && typeof value !== 'undefined') {
                            table.search(value, name);
                        }
                    });
                });
            }
        }

        table.on('datatable-on-layout-updated', function (e, args) {
            if (args.table == tableId) {
                for (var rowId of $.nonSubTablesRowIds) {
                    $('tr').each(function () {
                        if ($(this).data('row') == rowId) {
                            $(this).find('i').remove();
                        }
                    });
                }

                $.nonSubTablesRowIds = [];
            }
        });
    }

    var handleDeleteAction = function () {
        $(document).on('click', '.js-delete-action-table', function (e) {
            e.preventDefault();
            var $this = $(this);
            var action = $this.attr('href');

            if (action) {
                var table = $this.closest('.js-kt-table');
                var id = table.attr('id');

                PluginCmd.initHandleDeleteConfirmActionBase(action, null,
                    function (result) {
                        $('#' + id).KTDatatable('reload');
                    }
                );
            }
        });
    }

    function setupChildTable(childModel) {
        var childTableColumnFn = KtDatatableCmd[childModel.ColumnFunctionName];
        var queryParamsFn = KtDatatableCmd[childModel.QueryParamsFunctionName];
        var columns = childTableColumnFn();

        return function (e) {
            if (e.data.HasChildTable) {
                $('<div/>').attr('id', 'child_data_ajax_' + e.data.Id).appendTo(e.detailCell).KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: childModel.ReadUrl,
                                params: {
                                    // custom query params
                                    query: queryParamsFn(e)
                                },
                            },
                        },
                        pageSize: childModel.PageSize,
                        serverPaging: true,
                        serverFiltering: false,
                        serverSorting: true,
                    },
                    layout: {
                        scroll: false,
                        footer: false,
                        // enable/disable datatable spinner.
                        spinner: {
                            type: 1,
                            theme: 'default',
                        },
                    },
                    sortable: true,
                    columns: columns,
                    scrollX: true,
                });
            }
        }
    }

    return {
        setUpTable: function (data) {
            setUpTable(data);
        }
    };
}();

