﻿var App = function () {
    var confirmOnlyAction = 'confirm-only';
    var isCursorFocusedInInput = false;

    var handleCommon = function () {

        $(document).on("wheel", "input[type=number]", function (e) {
            $(this).blur();
        });

        $(document).on('focus', 'input', function () {
            isCursorFocusedInInput = true;
        });

        $(document).on('focusout', 'input', function () {
            isCursorFocusedInInput = false;
        });

        $(document).ajaxStop(function () {
            $.unblockUI();
        });

        $(document).ajaxError(function (event, response) {
            var errorText = locals.InternalServerError;

            if (response.status !== 500) {
                errorText = response.responseText;
            }

            if (response.status !== 0) {
                Swal.fire({
                    title: "500",
                    text: errorText,
                    icon: "error",
                    buttonsStyling: false,
                    showCancelButton: false,
                    confirmButtonText: locals.Close,
                    customClass: {
                        confirmButton: "btn btn-danger"
                    }
                });
            }
        });

        $('.js-datepicker').datepicker({
            todayHighlight: true,
            language: 'en',
            rtl: KTUtil.isRTL(),
            format: 'mm.dd.yyyy',
            orientation: "bottom left",
            autoclose: true,
            keepOpen: false
        });
    };

    var showLoader = function (isModal, newclassName, newdomEl) {
        $('body').removeClass('loader_fixed');
        $('.js-custom-loader').remove();

        isModal = true;

        var className = 'main_loader ';
        if (newclassName) {
            className += newclassName;
        }

        var domEl;
        if (newdomEl) {
            domEl = newdomEl;
        }
        else {
            if (isModal) {
                domEl = 'body';
            }
            else {
                domEl = 'section';
            }
        }

        var loaderHtml = `<div class="${className} js-custom-loader">
                <img src="/images/loader.gif">
            </div>`;

        $(domEl).append(loaderHtml);
        $('body').addClass('loader_fixed');
    };

    var hideLoader = function () {
        $('body').removeClass('loader_fixed');
        $('.js-custom-loader').remove();
    };

    var handleModalActions = function () {

        $(document).on('click', 'a.prevent-middle', function (event) {

        });

        $(document).on('click', 'a.js-entity-action:not(.custom)', function (event) {
            event.preventDefault();
            var $this = $(this);

            handleEntityAction($this.attr('href'));
        });
    };

    var handleEntityAction = function (href, successCallback, cancelCallbak, openCallback, openedCallback) {
        blockUI();
        $.get(href, function (result) {
            if (result.redirect) {
                blockUI({ animate: true });
                window.location.href = result.redirect;
            }
            else {
                setupModalActionModal(result, successCallback, cancelCallbak, openCallback, openedCallback);
            }
        });
    };

    var setupModalActionModal = function (data, successCallback, cancelCallbak, openCallback, openedCallback) {

        var $existing = $('#js-entity-action-modal');
        var $modal = $(data);
        var $form;
        if ($existing.length) {
            $existing.html($modal.html());
            $form = $existing.find('form');

            if (openCallback) {
                openCallback();
            }

            if (openedCallback) {
                openedCallback();
            }
        }
        else {
            $('body').append($modal);

            $modal.on('show.bs.modal', function (event) {
                if (openCallback) {
                    openCallback();
                }
            });

            $modal.on('shown.bs.modal', function (event) {
                if (openedCallback) {
                    openedCallback();
                }
            });

            $modal.filter('.modal').modal();
            $modal.on('hidden.bs.modal', function (e) {
                if (cancelCallbak) {
                    cancelCallbak();
                }

                $(this).remove();
            });

            $form = $modal.find('form');
        }

        PluginCmd.initSelect2();
        setupDatePickers();

        if ($form.length) {
            $.validator.unobtrusive.reParse($form);
            $form.on('submit', function (event) {
                event.preventDefault();
                blockUI({ target: '.modal-content' });

                $.post($form.attr('action'), $form.serialize(), function (result) {
                    if (result.success) {
                        if (successCallback) {
                            successCallback(result);
                            $('#js-entity-action-modal').modal('hide')
                        }
                        else {
                            window.location.reload();
                        }
                    }
                    else {
                        setupModalActionModal(result, successCallback, cancelCallbak, openCallback, openedCallback);
                    }
                }).done(function () {
                    unblockModal();
                });
            });
        }
    }

    var handleBlocking = function () {
        $.blockUI.defaults.message = '<div class="page-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';
        $.blockUI.defaults.css = { padding: 0, margin: 0, width: "30%", top: "40%", left: "35%", 'text-align': 'center' };
        $.blockUI.defaults.overlayCSS.opacity = '0.1';
        $.blockUI.defaults.overlayCSS.background = 'black';
        //$.blockUI.defaults.baseZ = 2000;
    };

    var blockUI = function (options) {
        options = $.extend(true, {}, options);
        html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '">' + '<div class="block-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>' + '</div>';

        if (options.target) { // element blocking
            var el = $(options.target);
            if (el.height() <= ($(window).height())) {
                options.cenrerY = true;
            }
            el.block({
                message: html,
                baseZ: options.zIndex ? options.zIndex : 1000,
                centerY: options.cenrerY !== undefined ? options.cenrerY : false,
                css: {
                    top: '10%',
                    border: '0',
                    padding: '0',
                    backgroundColor: 'none'
                },
                overlayCSS: {
                    backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                    opacity: options.boxed ? 0.05 : 0.1,
                    cursor: 'wait'
                }
            });
        } else {
            $.blockUI({
                message: html,
                baseZ: options.zIndex ? options.zIndex : 1000,
                css: {
                    border: '0',
                    padding: '0',
                    backgroundColor: 'none'
                },
                overlayCSS: {
                    backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                    opacity: options.boxed ? 0.05 : 0.1,
                    cursor: 'wait'
                }
            });
        }
    }

    var unblockUI = function (target) {
        if (target) {
            $(target).unblock({
                onUnblock: function () {
                    $(target).css('position', '');
                    $(target).css('zoom', '');
                }
            });
        } else {
            $.unblockUI();
        }
    }

    var unblockModal = function () {
        unblockUI($('.modal-content'));
    }

    var setupDatePickers = function (forbidFutureDate = false) {
        var arrows;
        if (KTUtil.isRTL()) {
            arrows = {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>'
            }
        } else {
            arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }

        var options = {
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            format: 'mm.dd.yyyy',
            templates: arrows,
            autoclose: true,
            keepOpen: false
        };

        var dateTimePickerOptions = {
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            format: 'mm.dd.yyyy hh:ii',
            //templates: arrows,
            autoclose: true,
            keepOpen: false
        };

        if (forbidFutureDate) {
            var today = new Date();
            options.endDate = 'today';
            options.maxDate = today;
        }

        $('.kt_datepicker').datepicker(options);

        $('.kt_datetimepicker').datetimepicker(dateTimePickerOptions);
    }

    return {
        initBlocking: function () {
            handleBlocking();
        },
        initEntityAction: function ($action, successCallback, cancelCallback, openCallback) {
            handleEntityAction($action, successCallback, cancelCallback, openCallback);
        },
        initModalActions: function () {
            handleModalActions();
        },
        init: function () {
            PluginCmd.init();
            this.initModalActions();
            this.initBlocking();
            handleCommon();
            this.initSetupDatePickers();
        },
        handleEntityAction: function (href, successCallback, cancelCallbak, openCallback) {
            handleEntityAction(href, successCallback, cancelCallbak, openCallback);
        },
        initSetupDatePickers: function (forbidFutureDate = false) {
            setupDatePickers(forbidFutureDate);
        },
        initShoLoader: function () {
            showLoader();
        },
        initEmployees: function () {
            employees();
        },
    };
}();

