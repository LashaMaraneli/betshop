using Betasuchus.Admin.Factories.Menu;
using Betasuchus.Admin.BackgroundJobs;
using Betasuchus.DI;
using Betasuchus.Domain.UserManagement;
using Betasuchus.Infrastructure.Db;
using Hangfire;
using JsonNet.ContractResolvers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Hangfire.PostgreSql;

namespace Betasuchus.Admin
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Env { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Resolve(Env, Configuration);

            services
                .AddIdentity<User, Role>(options =>
                {
                    options.SignIn.RequireConfirmedEmail = false;
                    options.SignIn.RequireConfirmedPhoneNumber = false;
                    options.SignIn.RequireConfirmedAccount = false;

                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequiredLength = 6;
                })
                .AddUserStore<UserStore<User, Role, BetasuchusDbContext, long, UserClaim, UserRole, UserLogin, UserToken, RoleClaim>>()
                .AddRoleManager<RoleManager<Role>>()
                .AddRoleStore<RoleStore<Role, BetasuchusDbContext, long, UserRole, RoleClaim>>()
                .AddSignInManager<SignInManager<User>>()
                .AddUserManager<UserManager<User>>()
                .AddDefaultTokenProviders()
                .AddUserValidator<UserValidator<User>>();

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(int.MaxValue);

                options.LoginPath = "/Account/Login";
                options.SlidingExpiration = true;
            });

            //Policy
            var policy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();

            services.AddAuthorization(opts =>
            {
                // TODO Add possible policies.
                opts.DefaultPolicy = policy;
            });

            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                    options.JsonSerializerOptions.PropertyNamingPolicy = null;
                });

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services.AddSession();

            // Factories
            services.AddScoped<IMenuModelFactory, MenuModelFactory>();

            services.AddLocalization(x =>
            {
                x.ResourcesPath = "/Resources";
            });

            var georgianCultureInfo = new CultureInfo("ka-GE");
            //var englishCultureInfo = new CultureInfo("en-US");

            var supportedCultures = new List<CultureInfo>
            {
                georgianCultureInfo,
                //englishCultureInfo
            };

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture(georgianCultureInfo);
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                TypeNameHandling = TypeNameHandling.Objects,
                ContractResolver = new PrivateSetterContractResolver()
            };

            services.AddHangfire(x =>
            {
                x.UseStorage(new PostgreSqlStorage(Configuration.GetConnectionString("BetasuchusDbContext")));
            });

            services.AddHangfireServer();

            services.AddScoped<OrderPaymentProcessor>();
            services.AddScoped<OrderPaymentStatusProcessor>();
            services.AddScoped<OrderBonusHubSenderProcessor>();
            services.AddScoped<OrderBonusHubStatusCheckerProcessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                app.UseForwardedHeaders();
                //app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseSession();

            app.UseRouting();

            app.UseAuthorization();

            app.UseHangfireDashboard();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "AreaRoutes",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                );

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllers();

                endpoints.MapHangfireDashboard();
            });

            Migrate(app, env);

            Seed(app);

            RegisterJobs();
        }

        private static void Migrate(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (!env.IsDevelopment())
            {
                using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
                serviceScope.ServiceProvider.GetService<BetasuchusDbContext>().Database.Migrate();
            }
        }

        private static void Seed(IApplicationBuilder app)
        {
            try
            {
                using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = serviceScope.ServiceProvider.GetService<BetasuchusDbContext>();


                if (!db.Set<Role>().Any(x => x.NormalizedName == "editor".ToUpper()))
                {
                    var adminRole = new Role("editor");
                    var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<Role>>();

                    roleManager.CreateAsync(adminRole).GetAwaiter().GetResult();
                }

                if (!db.Set<Role>().Any(x => x.NormalizedName == "admin".ToUpper()))
                {
                    var superAdminRole = new Role("admin");
                    var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<Role>>();

                    roleManager.CreateAsync(superAdminRole).GetAwaiter().GetResult();
                }

                if (!db.Set<User>().Any(x => x.Email == "admin@betshop.ge"))
                {
                    var userManager = serviceScope.ServiceProvider.GetService<UserManager<User>>();

                    var createUserResult = userManager.CreateAsync(new User("admin@betshop.ge"), "123456").GetAwaiter().GetResult();

                    var user = userManager.FindByNameAsync("admin@betshop.ge").GetAwaiter().GetResult();

                    var addToRoleResult = userManager.AddToRoleAsync(user, "admin").GetAwaiter().GetResult();
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, ex.Message);
            }
        }

        private static void RegisterJobs()
        {
            RecurringJob.AddOrUpdate<OrderBonusHubSenderProcessor>(x => x.DoWork(), Cron.Minutely);
            RecurringJob.AddOrUpdate<OrderBonusHubStatusCheckerProcessor>(x => x.DoWork(), Cron.Minutely);
            RecurringJob.AddOrUpdate<OrderPaymentProcessor>(x => x.DoWork(), Cron.Minutely);
            RecurringJob.AddOrUpdate<OrderPaymentStatusProcessor>(x => x.DoWork(), Cron.Minutely);
        }
    }
}