﻿using Betasuchus.Admin.Infrastructure;
using Betasuchus.Admin.Models.Account;
using Betasuchus.Application.Commands.UserCommands;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Betasuchus.Admin.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private readonly CommandExecutor _commandExecutor;
        private readonly QueryExecutor _queryExecutor;

        public AccountController(
            CommandExecutor commandExecutor,
            QueryExecutor queryExecutor)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
        }

        [HttpGet]
        public IActionResult Login(LoginUserCommand command)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(command);
        }

        [HttpPost]
        [ActionName("Login")]
        public async Task<IActionResult> LoginPost(LoginUserCommand command)
        {
            var executionResult = await _commandExecutor.ExecuteAsync(command);

            if (executionResult.Success)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.CanNotLogin = true;
            ViewBag.ErrorText = "Invalid Password or Username";

            return View();
        }


        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await _commandExecutor.ExecuteAsync(new LogoutUserCommand());
            }

            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Profile()
        {
            var userDetailsResponse = await _queryExecutor.ExecuteAsync<UserDetailsQuery, UserDetailsQueryResult>(new UserDetailsQuery()
            {
                UserName = User.Identity.Name
            });

            return View(new ProfileViewModel
            {
                Email = userDetailsResponse.Data.Email,
                UserName = userDetailsResponse.Data.UserName
            });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ChangePassword(ChangePasswordCommand command)
        {
            if (ModelState.IsValid)
            {
                var response = await _commandExecutor.ExecuteAsync(command);

                if (response.Success)
                {
                    this.SetSuccessMessage("Success");
                    return RedirectToAction("Profile", "Account");
                }
            }

            this.SetErrorMessage("Error");
            return RedirectToAction("Profile", "Account");
        }
    }
}
