﻿using Betasuchus.Admin.Models.Shared.Menu;

namespace Betasuchus.Admin.Factories.Menu
{
    public interface IMenuModelFactory
    {
        SideMenuModel PrepareSideMenuModel(string controllerName);

        TopMenuModel PrepareTopMenuModel(string controllerName, string actionName, string requestPathAndQuery, MenuPage menuPage);
    }
}
