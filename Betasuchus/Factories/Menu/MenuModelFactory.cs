﻿using Betasuchus.Admin.Areas.ReportingManagement.Controllers;
using Betasuchus.Admin.Areas.ServiceManagement.Controllers;
using Betasuchus.Admin.Areas.UserManagement.Controllers;
using Betasuchus.Admin.Controllers;
using Betasuchus.Admin.Models.Shared.Menu;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using Wandio.Web.Mvc;

namespace Betasuchus.Admin.Factories.Menu
{
    public class MenuModelFactory : IMenuModelFactory
    {
        private readonly LinkGenerator _linkGenerator;
        public static string bullerClassName = "menu-bullet menu-bullet-dot";

        public MenuModelFactory(LinkGenerator linkGenerator)
        {
            _linkGenerator = linkGenerator;
        }

        //Example
        private TopMenuModel PrepareHomePageTopMenuModel(string controllerName, string actionName)
        {
            //var homeControllerName = nameof(HomeController).TrimController();

            //var topMenuItem = new[]
            //{
            //homeControllerName
            //   string.Empty
            //};

            var model = new TopMenuModel
            {
                TopMenuItems = new List<MenuModel>
                {
                    //new MenuModel
                    //{
                    //    Title = "Dashboard",
                    //    RedirectUrl = _linkGenerator.GetPathByAction(nameof(HomeController.Index), homeControllerName),
                    //    Selected = actionName.Equals(nameof(HomeController.Index)),
                    //},
                    //new MenuModel
                    //{
                    //    Title = "Features",
                    //    Selected = controllerName.Equals(homeControllerName) &&
                    //               new[]{ nameof(HomeController.Privacy)}.Contains(actionName),
                    //    SubMenuModel = new List<MenuModel>
                    //    {
                    //        new MenuModel
                    //        {
                    //            Title = "Privacy",
                    //            RedirectUrl = _linkGenerator.GetPathByAction(nameof(HomeController.Privacy),homeControllerName),
                    //            Selected = actionName.Equals(nameof(HomeController.Privacy)),
                    //            IconClassName = bullerClassName
                    //        }
                    //    }
                    //},
                    //new MenuModel
                    //{
                    //    Title = "Main Content",
                    //    Selected = topMenuItem.Contains(controllerName),
                    //    SubMenuModel = new List<MenuModel>
                    //    {
                    //        new MenuModel
                    //        {
                    //            Title = "AllPages",
                    //            Selected = new[]
                    //            {
                    //                nameof(HomeController.Index),
                    //                nameof(HomeController.Privacy)
                    //            }.Contains(actionName),
                    //            IconPath = "Add-user.svg",
                    //            SubMenuModel = new List<MenuModel>
                    //            {
                    //                new MenuModel
                    //                {
                    //                    Title = "Dashboard",
                    //                    RedirectUrl = _linkGenerator.GetPathByAction(nameof(HomeController.Index), homeControllerName),
                    //                    Selected = actionName.Equals(nameof(HomeController.Index)),
                    //                    IconClassName = bullerClassName
                    //                },
                    //                new MenuModel
                    //                {
                    //                    Title = "Privacy",
                    //                    RedirectUrl = _linkGenerator.GetPathByAction(nameof(HomeController.Privacy),homeControllerName),
                    //                    Selected = actionName.Equals(nameof(HomeController.Privacy)),
                    //                    IconClassName = bullerClassName
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                }.ToList()
            };

            return model;
        }

        public SideMenuModel PrepareSideMenuModel(string controllerName)
        {
            var homeControllerName = nameof(HomeController).TrimController();
            var categoryMenu = new string[]{
                nameof(CategoryController).TrimController(),
                nameof(ProviderController).TrimController(),
                nameof(OfferController).TrimController(),
                nameof(OfferHistoryController).TrimController()
            };

            var accountContollerName = nameof(AccountController).TrimController();
            var userControllerName = nameof(UserController).TrimController();
            var reportControllerName = nameof(ReportController).TrimController();

            var sideMenuModel = new SideMenuModel
            {
                SideMenuItems = new List<SideMenuItem>
                {
                    new SideMenuItem
                    {
                        SideMenuItemPosition = SideMenuItemPosition.Top,
                        MenuPage = MenuPage.Home,
                        MenuModel = new MenuModel
                        {
                            Title = "Dashboard",
                            Selected = homeControllerName.Equals(controllerName),
                            IconClassName = "fas fa-home",
                            RedirectUrl = _linkGenerator.GetPathByAction(nameof(HomeController.Index), homeControllerName),
                        }
                    },
                    new SideMenuItem
                    {
                        SideMenuItemPosition = SideMenuItemPosition.Top,
                        MenuPage = MenuPage.Category,
                        MenuModel = new MenuModel
                        {
                            Title = "Service Management",
                            Selected = categoryMenu.Contains(controllerName),
                            IconClassName = "fas fa-list",
                            RedirectUrl = _linkGenerator.GetPathByAction(
                                nameof(CategoryController.Index),
                                nameof(CategoryController).TrimController(),
                                new { Area = Areas.ServiceManagement.AreaConstants.AreaName })
                        }
                    },
                    new SideMenuItem
                    {
                        SideMenuItemPosition = SideMenuItemPosition.Top,
                        MenuPage = MenuPage.Report,
                        MenuModel = new MenuModel
                        {
                            Title = "Report Management",
                            Selected = reportControllerName.Contains(controllerName),
                            IconClassName = "fas fa-list",
                            RedirectUrl = _linkGenerator.GetPathByAction(
                                nameof(ReportController.Index),
                                nameof(ReportController).TrimController(),
                                new { Area = Areas.ReportingManagement.AreaConstants.AreaName })
                        }
                    },
                    new SideMenuItem
                    {
                        SideMenuItemPosition = SideMenuItemPosition.Top,
                        MenuPage = MenuPage.User,
                        MenuModel = new MenuModel
                        {
                            Title = "Users",
                            Selected = userControllerName.Equals(controllerName),
                            IconClassName = "fas fa-user",
                            RedirectUrl = _linkGenerator.GetPathByAction(nameof(UserController.Index), userControllerName, new{ Area = Areas.UserManagement.AreaConstants.AreaName}),
                        }
                    },
                    new SideMenuItem
                    {
                        SideMenuItemPosition = SideMenuItemPosition.Bottom,
                        MenuPage = MenuPage.User,
                        MenuModel = new MenuModel
                        {
                            Title = "LogOut",
                            Selected = userControllerName.Equals(controllerName),
                            IconClassName = "fas fa-sign-out-alt",
                            RedirectUrl = _linkGenerator.GetPathByAction(nameof(AccountController.Logout), accountContollerName),
                        }
    }
    //new SideMenuItem
    //{
    //    SideMenuItemPosition = SideMenuItemPosition.Top,
    //    MenuPage = MenuPage.Category,
    //    MenuModel = new MenuModel
    //    {
    //        Title = "Service Management",
    //        Selected = categoryMenu.Contains(controllerName),
    //        IconClassName = "fas fa-list",
    //        RedirectUrl = _linkGenerator.GetPathByAction(
    //            nameof(OfferController.Index),
    //            nameof(OfferController).TrimController(),
    //            new { Area = Areas.ServiceManagement.AreaConstants.AreaName })
    //    }
    //},
    // new SideMenuItem
    //{
    //    SideMenuItemPosition = SideMenuItemPosition.Top,
    //    MenuPage = MenuPage.Location,
    //    MenuModel = new MenuModel
    //    {
    //        Title = "Location",
    //        Selected = locationControllerName.Equals(controllerName),
    //        IconClassName = "fas fa-map-marker-alt",
    //        RedirectUrl = _linkGenerator.GetPathByAction(nameof(LocationController.Index), locationControllerName),
    //    }
    //},

}
            };

            return sideMenuModel;
        }

        public TopMenuModel PrepareTopMenuModel(string controllerName, string actionName, string requestPathAndQuery, MenuPage menuPage)
        {
            var model = new TopMenuModel();

            switch (menuPage)
            {
                case MenuPage.Home:
                    model = PrepareHomePageTopMenuModel(controllerName, actionName);
                    break;
                case MenuPage.Category:
                    model = PrepareCategoryTopMenuModel(controllerName, actionName);
                    break;
                case MenuPage.Report:
                    model = PrepareReportTopMenuModel(controllerName, actionName);
                    break;
            }

            return model;
        }

        private TopMenuModel PrepareCategoryTopMenuModel(string controllerName, string actionName)
        {
            var model = new TopMenuModel
            {
                TopMenuItems = new List<MenuModel>
                {
                    new MenuModel
                    {
                        Selected = controllerName.ToLower() == nameof(CategoryController).TrimController(),
                        RedirectUrl = _linkGenerator.GetPathByAction(nameof(CategoryController.Index), nameof(CategoryController).TrimController(), new{ Area = Areas.ServiceManagement.AreaConstants.AreaName}),
                        Title = "Categories"
                    },
                    new MenuModel
                    {
                        Selected = controllerName.ToLower() == nameof(ProviderController).TrimController(),
                        RedirectUrl = _linkGenerator.GetPathByAction(nameof(ProviderController.Index), nameof(ProviderController).TrimController(), new{ Area = Areas.ServiceManagement.AreaConstants.AreaName}),
                        Title = "Providers"
                    },
                    new MenuModel
                    {
                        Selected = controllerName.ToLower() == nameof(OfferController).TrimController(),
                        RedirectUrl = _linkGenerator.GetPathByAction(nameof(OfferController.Index), nameof(OfferController).TrimController(), new{ Area = Areas.ServiceManagement.AreaConstants.AreaName}),
                        Title = "Offers"
                    },
                    new MenuModel
                    {
                        Selected = controllerName.ToLower() == nameof(OfferHistoryController).TrimController(),
                        RedirectUrl = _linkGenerator.GetPathByAction(nameof(OfferHistoryController.Index), nameof(OfferHistoryController).TrimController(), new{ Area = Areas.ServiceManagement.AreaConstants.AreaName}),
                        Title = "Offer History"
                    }
                }
            };

            return model;
        }

        private TopMenuModel PrepareReportTopMenuModel(string contollername, string actionName)
        {
            var model = new TopMenuModel
            {
                TopMenuItems = new List<MenuModel>
                {
                    new MenuModel
                    {
                        Selected = contollername.ToLower() == nameof(ReportController).TrimController(),
                        RedirectUrl = _linkGenerator.GetPathByAction(
                                nameof(ReportController.Index),
                                nameof(ReportController).TrimController(), new {
                                    Area = Areas.ReportingManagement.AreaConstants.AreaName,
                                    BeginDate = DateTime.Now.AddDays(-1).ToString("MM.dd.yyyy hh:mm"),
                                    EndDate = DateTime.Now.ToString("MM.dd.yyyy hh:mm") }),
                        Title = "24 Hour Report"
                    },
                    new MenuModel
                    {
                        Selected = contollername.ToLower() == nameof(ReportController).TrimController(),
                        RedirectUrl = _linkGenerator.GetPathByAction(
                            nameof(ReportController.Index),
                                nameof(ReportController).TrimController(), new {
                                    Area = Areas.ReportingManagement.AreaConstants.AreaName,
                                    BeginDate = DateTime.Now.AddDays(-7).ToString("MM.dd.yyyy hh:mm"),
                                    EndDate = DateTime.Now.ToString("MM.dd.yyyy hh:mm") }),
                        Title = "1 Week Report"
                    }
                }
            };

            return model;
        }
    }
}
