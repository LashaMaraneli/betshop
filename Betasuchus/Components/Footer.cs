﻿using Microsoft.AspNetCore.Mvc;

namespace Betasuchus.Admin.Components
{
    public class FooterViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
