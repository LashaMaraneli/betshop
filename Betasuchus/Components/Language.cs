﻿using Microsoft.AspNetCore.Mvc;

namespace Betasuchus.Admin.Components
{
    public class LanguageViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
