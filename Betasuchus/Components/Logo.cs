﻿using Betasuchus.Admin.Models.Shared.KtCommon;
using Microsoft.AspNetCore.Mvc;

namespace Betasuchus.Admin.Components
{
    public class LogoViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(bool isMobileViewLogo)
        {
            var model = new LogoModel
            {
                CompanyName = "Doggosaurus",
                IsMobileViewLogo = isMobileViewLogo
            };

            return View(model);
        }
    }
}
