﻿using Betasuchus.Admin.Factories.Menu;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Betasuchus.Admin.Components
{
    public class SideMenuViewComponent : ViewComponent
    {
        private readonly IMenuModelFactory _menuModelFactory;

        public SideMenuViewComponent(IMenuModelFactory menuModelFactory)
        {
            _menuModelFactory = menuModelFactory;
        }

        public IViewComponentResult Invoke()
        {
            string controllerName = RouteData.Values["controller"].ToString();
            string actionName = RouteData.Values["action"].ToString();

            var model = _menuModelFactory.PrepareSideMenuModel(controllerName);
            var selectedSideMenu = model.SideMenuItems.Where(x => x.MenuModel.Selected).FirstOrDefault();

            this.TempData["SelectedSideMenu"] = selectedSideMenu;

            return View(model);
        }
    }
}
