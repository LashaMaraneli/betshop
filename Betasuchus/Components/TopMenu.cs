﻿using Betasuchus.Admin.Factories.Menu;
using Betasuchus.Admin.Models.Shared.Menu;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Betasuchus.Admin.Components
{
    public class TopMenuViewComponent : ViewComponent
    {
        private readonly IMenuModelFactory _menuModelFactory;

        public TopMenuViewComponent(IMenuModelFactory menuModelFactory)
        {
            _menuModelFactory = menuModelFactory;
        }

        public IViewComponentResult Invoke()
        {
            var selectedSideMenuObject = this.TempData["SelectedSideMenu"];
            if (selectedSideMenuObject != null)
            {
                var selectedSideMenu = selectedSideMenuObject as SideMenuItem;

                string controllerName = RouteData.Values["controller"].ToString();
                string actionName = RouteData.Values["action"].ToString();

                var requestPathAndQuery = Request.GetEncodedPathAndQuery();

                var model = _menuModelFactory.PrepareTopMenuModel(controllerName, actionName, requestPathAndQuery, selectedSideMenu.MenuPage);

                if (model.TopMenuItems != null && model.TopMenuItems.Any())
                    return View(model);

            }

            return Content("");
        }
    }
}
