﻿using Betasuchus.Application.Commands.OrderManagement;
using Betasuchus.Application.Extensions;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.OperationManagement;
using Betasuchus.DI;
using Hangfire;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Betasuchus.Admin.BackgroundJobs
{
    public class OrderPaymentStatusProcessor : IDisposable
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;

        public OrderPaymentStatusProcessor(
            IWebHostEnvironment env,
            IConfiguration config)
        {
            _env = env;
            _config = config;
        }

        [DisableConcurrentExecution(30)]
        public async Task DoWork()
        {
            Log.Information("OrderPaymentStatusProcessor Service is working.");

            var serviceProvider = DependencyResolver.Resolve(new ServiceCollection(), _env, _config).BuildServiceProvider();

            using var scope = serviceProvider.CreateScope();

            var queryExecutor = scope.ServiceProvider.Get<QueryExecutor>();
            var commandExecutor = scope.ServiceProvider.Get<CommandExecutor>();

            Log.Information("Querying AwaitingPayment operations");

            var queryOperationsResult = await queryExecutor.ExecuteAsync<QueryOperationsByStatus, QueryOperationsByStatusResult>(new QueryOperationsByStatus
            {
                Status = Domain.OperationManagement.InternalStatus.AwaitingPayment
            });

            var operations = queryOperationsResult.Data.Operations;

            foreach (var operation in operations)
            {
                var command = new CheckPaymentStatusCommand()
                {
                    Id = operation.Id,
                };

                commandExecutor.ExecuteAsync(command).Wait();
            }
        }

        public void Dispose()
        {
        }
    }
}
