﻿using Betasuchus.Application.Commands.OperationManagement;
using Betasuchus.Application.Extensions;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.OperationManagement;
using Betasuchus.DI;
using Hangfire;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Betasuchus.Admin.BackgroundJobs
{
    public class OrderBonusHubSenderProcessor : IDisposable
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;

        public OrderBonusHubSenderProcessor(
            IWebHostEnvironment env,
            IConfiguration config)
        {
            _env = env;
            _config = config;
        }

        [DisableConcurrentExecution(30)]
        public async Task DoWork()
        {
            Log.Information("OrderBonusHubSenderProcessor Service is working.");

            var serviceProvider = DependencyResolver.Resolve(new ServiceCollection(), _env, _config).BuildServiceProvider();

            using var scope = serviceProvider.CreateScope();

            var queryExecutor = scope.ServiceProvider.Get<QueryExecutor>();
            var commandExecutor = scope.ServiceProvider.Get<CommandExecutor>();

            Log.Information("Querying operations awaiting sending to bonus hub");

            var queryOperationResult = await queryExecutor.ExecuteAsync<QueryOperationsByStatus, QueryOperationsByStatusResult>(new QueryOperationsByStatus
            {
                Status = Domain.OperationManagement.InternalStatus.AwaitingSending
            });

            var operations = queryOperationResult.Data.Operations;

            foreach (var operation in operations)
            {
                var command = new StartAwardingCommand
                {
                    Id = operation.Id
                };

                commandExecutor.ExecuteAsync(command).Wait(); ;
            }
        }

        public void Dispose()
        {
        }
    }
}
