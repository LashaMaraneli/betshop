﻿using Betasuchus.Admin.Infrastructure;
using Betasuchus.Application.Commands.OfferManagement;
using Betasuchus.Application.Infrastructure;
using System;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public class ActivateOfferViewModel : IViewModel
    {
        public int Id { get; set; }

        public DateTime? ExpireDate { get; set; }

        public Command ToCommand()
        {
            var command = new ActivateExpiredOfferCommand
            {
                Id = Id,
                ExpireDate = ExpireDate.Value
            };

            return command;
        }
    }
}
