﻿using Betasuchus.Admin.Infrastructure;
using Betasuchus.Application.Commands.CategoryManagement;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Categories
{
    public class AddCategoryViewModel : IViewModel
    {
        public long Id { get; set; }

        public long? ParentCategoryId { get; set; }

        public IEnumerable<SelectListItem> ParentCategories { get; set; }

        public MultilanguageString Name { get; set; }

        public IFormFile Thumbnail { get; set; }

        public Command ToCommand()
        {
            var command = new CreateCategoryCommand
            {
                Name = Name,
                ParentCategoryId = ParentCategoryId,
                Thumbnail = Thumbnail
            };

            return command;
        }
    }
}
