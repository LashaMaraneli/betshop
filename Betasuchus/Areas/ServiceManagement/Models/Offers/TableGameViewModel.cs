﻿using Betasuchus.Application.Commands.OfferManagement.TableGame;
using Betasuchus.Application.Infrastructure;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public class TableGameViewModel : ExtendedOfferViewModel
    {
        public override Command ToCommand()
        {
            var command = new CreateTableGameCommand()
            {
                Thumbnail = Thumbnail,
                Amount = Amount,
                CategoryId = CategoryId,
                Expire = Expire,
                ExpireDate = ExpireDate.Value,
                FreebetNominal = FreebetNominal,
                GameType = GameType,
                DiscountedPrice = DiscountedPrice,
                OfferText = OfferText,
                Price = Price,
                GameName = GameName
            };

            return command;
        }
    }
}
