﻿using Betasuchus.Application.Commands.OfferManagement.Sport;
using Betasuchus.Application.Infrastructure;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public class SportViewModel : ExtendedOfferViewModel
    {
        public override Command ToCommand()
        {
            var command = new CreateSportCommand()
            {
                Amount = Amount,
                OfferText = OfferText,
                CategoryId = CategoryId,
                ExpireDate = ExpireDate.Value,
                FreebetNominal = FreebetNominal,
                GameType = GameType,
                DiscountedPrice = DiscountedPrice,
                Price = Price,
                Thumbnail = Thumbnail,
                Expire = Expire,
                GameName = GameName
            };

            return command;
        }
    }
}
