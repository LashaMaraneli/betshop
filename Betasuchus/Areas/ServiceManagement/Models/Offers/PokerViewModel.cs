﻿using Betasuchus.Application.Commands.OfferManagement.Poker;
using Betasuchus.Application.Infrastructure;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public class PokerViewModel : ExtendedOfferViewModel
    {
        public string BonusCode { get; set; }

        public override Command ToCommand()
        {
            var command = new CreatePokerCommand()
            {
                Price = Price,
                OfferText = OfferText,
                Amount = Amount,
                BonusCode = BonusCode,
                CategoryId = CategoryId,
                Expire = Expire,
                ExpireDate = ExpireDate.Value,
                FreebetNominal = FreebetNominal,
                GameType = GameType,
                DiscountedPrice = DiscountedPrice,
                Thumbnail = Thumbnail,
                GameName = GameName
            };

            return command;
        }
    }
}
