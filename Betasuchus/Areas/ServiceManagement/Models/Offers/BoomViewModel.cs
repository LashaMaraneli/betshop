﻿using Betasuchus.Application.Commands.OfferManagement.Boom;
using Betasuchus.Application.Infrastructure;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public class BoomViewModel : ExtendedOfferViewModel
    {
        public override Command ToCommand()
        {
            var command = new CreateBoomCommand()
            {
                Amount = Amount,
                ExpireDate = ExpireDate.Value,
                CategoryId = CategoryId,
                Expire = Expire,
                FreebetNominal = FreebetNominal,
                GameType = GameType,
                DiscountedPrice = DiscountedPrice,
                OfferText = OfferText,
                Price = Price,
                Thumbnail = Thumbnail,
                GameName = GameName
            };

            return command;
        }
    }
}
