﻿using Betasuchus.Application.Commands.OfferManagement.PlayNGo;
using Betasuchus.Application.Infrastructure;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public class PlayNGoViewModel : SlotOfferViewModel
    {
        public string TriggerId { get; set; }

        public override Command ToCommand()
        {
            var command = new CreatePlayNGoCommand
            {
                Thumbnail = Thumbnail,
                ProviderId = ProviderId,
                Price = Price,
                Amount = Amount,
                CategoryId = CategoryId,
                Expire = Expire,
                ExpireDate = ExpireDate.Value,
                FreebetNominal = FreebetNominal,
                GameName = GameName,
                GameType = GameType,
                DiscountedPrice = DiscountedPrice,
                OfferText = OfferText,
                TriggerId = TriggerId
            };

            return command;
        }
    }
}
