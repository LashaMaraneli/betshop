﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public abstract class ExtendedOfferViewModel : OfferViewModel
    {
        public int Expire { get; set; }
    }
}
