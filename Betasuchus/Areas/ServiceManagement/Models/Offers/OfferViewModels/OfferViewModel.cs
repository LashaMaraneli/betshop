﻿using Betasuchus.Admin.Infrastructure;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Domain.OfferManagement;
using Betasuchus.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public abstract class OfferViewModel : IViewModel
    {
        public int Amount { get; set; }

        public DateTime? ExpireDate { get; set; }

        public decimal Price { get; set; }

        public decimal DiscountedPrice { get; set; }

        public long CategoryId { get; set; }

        public IFormFile Thumbnail { get; set; }

        public MultilanguageString OfferText { get; set; }

        public int FreebetNominal { get; set; }

        public string CampaignId { get; set; }

        public GameType GameType { get; set; }

        public IEnumerable<SelectListItem> GameTypes { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }

        public MultilanguageString GameName { get; set; }

        public abstract Command ToCommand();
    }
}
