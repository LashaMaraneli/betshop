﻿using Betasuchus.Shared;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public abstract class SlotOfferViewModel : ExtendedOfferViewModel
    {
        public long ProviderId { get; set; }

        public IEnumerable<SelectListItem> Providers { get; set; }
    }
}
