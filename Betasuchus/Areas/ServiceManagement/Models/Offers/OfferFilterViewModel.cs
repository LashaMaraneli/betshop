﻿using Betasuchus.Admin.Models.Shared.KtCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public class OfferFilterViewModel : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
    }
}
