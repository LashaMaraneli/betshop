﻿using Betasuchus.Application.Commands.OfferManagement.EGT;
using Betasuchus.Application.Infrastructure;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Offers
{
    public class EgtViewModel : SlotOfferViewModel
    {
        public override Command ToCommand()
        {
            var command = new CreateEGTCommand
            {
                Amount = Amount,
                CampaignId = CampaignId,
                CategoryId = CategoryId,
                Expire = Expire,
                ExpireDate = ExpireDate.Value,
                FreebetNominal = FreebetNominal,
                GameName = GameName,
                GameType = GameType,
                DiscountedPrice = DiscountedPrice,
                OfferText = OfferText,
                Price = Price,
                ProviderId = ProviderId,
                Thumbnail = Thumbnail
            };

            return command;
        }
    }
}
