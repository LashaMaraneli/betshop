﻿using Betasuchus.Admin.Infrastructure;
using Betasuchus.Application.Commands.ProviderManagement;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Shared;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Providers
{
    public class AddProviderViewModel : IViewModel
    {
        public MultilanguageString Name { get; set; }

        public Command ToCommand()
        {
            var command = new CreateProviderCommand
            {
                Name = Name
            };

            return command;
        }
    }
}
