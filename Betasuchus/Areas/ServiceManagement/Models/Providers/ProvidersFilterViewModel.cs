﻿using Betasuchus.Admin.Models.Shared.KtCommon;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Providers
{
    public class ProvidersFilterViewModel : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
    }
}
