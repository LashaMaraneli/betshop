﻿using Betasuchus.Admin.Infrastructure;
using Betasuchus.Application.Commands.ProviderManagement;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Shared;

namespace Betasuchus.Admin.Areas.ServiceManagement.Models.Providers
{
    public class EditProviderViewModel : IViewModel
    {
        public long Id { get; set; }

        public MultilanguageString Name { get; set; }

        public Command ToCommand()
        {
            var command = new UpdateProviderCommand
            {
                Id = Id,
                Name = Name
            };

            return command;
        }
    }
}
