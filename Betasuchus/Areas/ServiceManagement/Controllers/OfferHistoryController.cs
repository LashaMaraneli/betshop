﻿using Betasuchus.Admin.Areas.ServiceManagement.Models.OfferHistory;
using Betasuchus.Admin.Areas.ServiceManagement.Models.Offers;
using Betasuchus.Admin.Infrastructure;
using Betasuchus.Admin.Models.Shared.KtCommon;
using Betasuchus.Admin.Resources;
using Betasuchus.Application.Commands.OfferManagement;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Offers;
using Betasuchus.Infrastructure.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Threading.Tasks;
using Wandio.Web.Mvc;

namespace Betasuchus.Admin.Areas.ServiceManagement.Controllers
{
    [Area(AreaConstants.AreaName)]
    [Authorize(Roles = "editor,admin")]
    public class OfferHistoryController : Controller
    {
        private readonly CommandExecutor _commandExecutor;
        private readonly QueryExecutor _queryExecutor;
        private readonly IStringLocalizer<Translations> _localizer;

        public OfferHistoryController(
            CommandExecutor commandExecutor,
            QueryExecutor queryExecutor,
            IStringLocalizer<Translations> localizer)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            var model = new OfferHistoryFilterViewModel
            {
                KtCardModel = new KtCardModel
                {
                    KtCardActionModel = new KtCardActionModel()
                    {
                        ActionItems = null
                    },
                    KtCardInfoModel = new KtCardInfoModel
                    {
                        Title = _localizer["OffersHistory"]
                    },
                    KtDatatableModel = new KtDatatableModel
                    {
                        DatatableName = "Offers",
                        ReadUrl = Url.Action(nameof(Offers), nameof(OfferHistoryController).TrimController(), new { Area = AreaConstants.AreaName }),
                        ColumnFunctionName = "initOffersData",
                        DeleteAction = new KtActionModel
                        {
                            ActionUrl = Url.Action(nameof(Delete), nameof(OfferHistoryController).TrimController(), new { Area = AreaConstants.AreaName }),
                            IconClassName = "fa fa-ban",
                            Name = "Delete"
                        },
                        AdditionalActions = new List<KtActionModel>()
                        .AddIf(User.IsInRole("admin"),
                            new KtActionModel
                            {
                                ActionUrl = Url.Action(nameof(Activate), nameof(OfferHistoryController).TrimController(), new { Area = AreaConstants.AreaName }),
                                Name = "Activate",
                                IconClassName = "fa fa-check",
                                Hide = false,
                            })
                    }
                }
            };

            return View(model);
        }

        public async Task<IActionResult> Offers(OfferFilterViewModel query)
        {
            var offerQuery = new OffersQuery()
            {
                Page = query.Page,
                PageSize = query.PageSize,
                SortBy = query.SortBy ?? "Id",
                SortOrder = query.SortOrder,
                Active = false
            };

            var result = await _queryExecutor.ExecutePagedQueryAsync<OffersQuery, OffersQueryResult>(offerQuery);

            query.Total = result.Data.TotalCount;

            return query.ToDataSourceResult(result.Data.Offers);
        }

        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Activate(int id)
        {
            return await Task.FromResult(View(new ActivateOfferViewModel() { Id = id }));
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Activate(ActivateOfferViewModel command)
        {
            var response = await _commandExecutor.ExecuteAsync(command.ToCommand());
            if (response.Success)
            {
                this.SetSuccessMessage("Offer add success");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("Offer add error");
            return this.JsonFail();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(DeleteOfferCommand command)
        {
            var response = await _commandExecutor.ExecuteAsync(command);

            if (response.Success)
            {
                this.SetSuccessMessage("Offer deleted");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("Offer delete error");
            return this.JsonFail();
        }
    }
}
