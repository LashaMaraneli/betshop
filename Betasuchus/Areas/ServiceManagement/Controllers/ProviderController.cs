﻿using Betasuchus.Admin.Areas.ServiceManagement.Models.Providers;
using Betasuchus.Admin.Infrastructure;
using Betasuchus.Admin.Models.Shared.KtCommon;
using Betasuchus.Admin.Resources;
using Betasuchus.Application.Commands.ProviderManagement;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Categories;
using Betasuchus.Application.Queries.Providers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wandio.Web.Mvc;

namespace Betasuchus.Admin.Areas.ServiceManagement.Controllers
{
    [Area(AreaConstants.AreaName)]
    [Authorize(Roles = "editor,admin")]
    public class ProviderController : Controller
    {
        private readonly CommandExecutor _commandExecutor;
        private readonly QueryExecutor _queryExecutor;
        private readonly IStringLocalizer<Translations> _localizer;

        public ProviderController(CommandExecutor commandExecutor, QueryExecutor queryExecutor, IStringLocalizer<Translations> localizer)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            var model = new ProvidersFilterViewModel
            {
                KtCardModel = new KtCardModel
                {
                    KtCardActionModel = new KtCardActionModel()
                    {
                        ActionItems = new List<KtActionModel>()
                        {
                            new KtActionModel
                            {
                                ActionUrl = Url.Action(nameof(Add), nameof(ProviderController).TrimController(), new{Area=AreaConstants.AreaName }),
                                Name = nameof(Add),
                                CustomClassName = "btn-primary",
                                IconClassName = "fa fa-plus"
                            }
                        }
                    },
                    KtCardInfoModel = new KtCardInfoModel
                    {
                        Title = _localizer["Providers"]
                    },
                    KtDatatableModel = new KtDatatableModel
                    {
                        DatatableName = "Providers",
                        ReadUrl = Url.Action(nameof(Providers), nameof(ProviderController).TrimController(), new { Area = AreaConstants.AreaName }),
                        ColumnFunctionName = "initProviderDataColumns",
                        EditAction = new KtActionModel
                        {
                            ActionUrl = Url.Action(nameof(Edit), nameof(ProviderController).TrimController(), new { Area = AreaConstants.AreaName }),
                        },
                        DeleteAction = new KtActionModel
                        {
                            ActionUrl = Url.Action(nameof(Delete), nameof(ProviderController).TrimController(), new { @Area = AreaConstants.AreaName }),
                        }
                    }
                }
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Providers(ProvidersFilterViewModel query)
        {
            var providersQuery = new ProvidersQuery
            {
                Page = query.Page,
                PageSize = query.PageSize,
                SortBy = query.SortBy,
                SortOrder = query.SortOrder,
                Total = query.Total
            };

            var result = await _queryExecutor.ExecutePagedQueryAsync<ProvidersQuery, ProvidersQueryResult>(providersQuery);

            query.Total = result.Data.TotalCount;

            return query.ToDataSourceResult(result.Data.Providers);
        }

        public async Task<IActionResult> Add()
        {
            var categoryList = await _queryExecutor.ExecuteAsync<CategoriesSelectListQuery, CategoriesSelectListQueryResult>(new CategoriesSelectListQuery());

            var model = new AddProviderViewModel
            {
                Name = new Shared.MultilanguageString(),
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddProviderViewModel command)
        {
            var result = await _commandExecutor.ExecuteAsync(command.ToCommand());

            if (result.Success)
            {
                this.SetSuccessMessage(_localizer["Provider create success"]);
                return this.JsonSuccess();
            }

            this.SetErrorMessage(result.Error.Errors.JoinStrings());
            return this.JsonFail();
        }

        public async Task<IActionResult> Edit(long id)
        {
            var categoryList = await _queryExecutor.ExecuteAsync<CategoriesSelectListQuery, CategoriesSelectListQueryResult>(new CategoriesSelectListQuery());
            var result = await _queryExecutor.ExecuteAsync<GetProviderQuery, GetProviderQueryResult>(new GetProviderQuery() { Id = id });

            var model = new EditProviderViewModel()
            {
                Id = id,
                Name = result.Data.Provider.Name
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditProviderViewModel command)
        {
            var result = await _commandExecutor.ExecuteAsync(command.ToCommand());

            if (result.Success)
            {
                this.SetSuccessMessage(_localizer["Provider update success"]);
                return this.JsonSuccess();
            }

            this.SetErrorMessage(result.Error.Errors.JoinStrings());
            return this.JsonFail();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DeleteProviderCommand command)
        {
            var result = await _commandExecutor.ExecuteAsync(command);

            if (result.Success)
            {
                this.SetSuccessMessage(_localizer["Provider delete success"]);
                return this.JsonSuccess();
            }

            this.SetErrorMessage(result.Error.Errors.JoinStrings());
            return this.JsonFail();
        }
    }
}
