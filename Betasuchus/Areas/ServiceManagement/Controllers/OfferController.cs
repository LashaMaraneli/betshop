﻿using Betasuchus.Admin.Areas.ServiceManagement.Models.Offers;
using Betasuchus.Admin.Infrastructure;
using Betasuchus.Admin.Models.Shared.KtCommon;
using Betasuchus.Admin.Resources;
using Betasuchus.Application.Commands.OfferManagement;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Categories;
using Betasuchus.Application.Queries.Offers;
using Betasuchus.Application.Queries.Providers;
using Betasuchus.Domain.OfferManagement;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wandio.Web.Mvc;

namespace Betasuchus.Admin.Areas.ServiceManagement.Controllers
{
    [Area(AreaConstants.AreaName)]
    [Authorize(Roles = "editor,admin")]
    public class OfferController : Controller
    {
        private readonly CommandExecutor _commandExecutor;
        private readonly QueryExecutor _queryExecutor;
        private readonly IStringLocalizer<Translations> _localizer;

        public OfferController(
            CommandExecutor commandExecutor,
            QueryExecutor queryExecutor,
            IStringLocalizer<Translations> localizer)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            var model = new OfferFilterViewModel
            {
                KtCardModel = new KtCardModel
                {
                    KtCardActionModel = new KtCardActionModel()
                    {
                        ActionItems = new List<KtActionModel>()
                        {
                            new KtActionModel
                            {
                                Name = "Add",
                                SubActions = new List<KtActionModel>(){
                                    new KtActionModel{
                                        ActionUrl = Url.Action(nameof(AddEgt), nameof(OfferController).TrimController(), new{Area=AreaConstants.AreaName }),
                                        Name = nameof(AddEgt),
                                        CustomClassName = "btn-primary",
                                        IconClassName = "fa fa-plus"
                                    },
                                    new KtActionModel{
                                        ActionUrl = Url.Action(nameof(AddPlayNGo), nameof(OfferController).TrimController(), new{Area=AreaConstants.AreaName }),
                                        Name = nameof(AddPlayNGo),
                                        CustomClassName = "btn-primary",
                                        IconClassName = "fa fa-plus"
                                    },
                                    new KtActionModel{
                                        ActionUrl = Url.Action(nameof(AddPoker), nameof(OfferController).TrimController(), new{Area=AreaConstants.AreaName }),
                                        Name = nameof(AddPoker),
                                        CustomClassName = "btn-primary",
                                        IconClassName = "fa fa-plus"
                                    },
                                    new KtActionModel{
                                        ActionUrl = Url.Action(nameof(AddTableGames), nameof(OfferController).TrimController(), new{Area=AreaConstants.AreaName }),
                                        Name = nameof(AddTableGames),
                                        CustomClassName = "btn-primary",
                                        IconClassName = "fa fa-plus"
                                    },
                                    new KtActionModel{
                                        ActionUrl = Url.Action(nameof(AddSport), nameof(OfferController).TrimController(), new{Area=AreaConstants.AreaName }),
                                        Name = nameof(AddSport),
                                        CustomClassName = "btn-primary",
                                        IconClassName = "fa fa-plus"
                                    },
                                    new KtActionModel{
                                        ActionUrl = Url.Action(nameof(AddBoom), nameof(OfferController).TrimController(), new{Area=AreaConstants.AreaName }),
                                        Name = nameof(AddBoom),
                                        CustomClassName = "btn-primary",
                                        IconClassName = "fa fa-plus",
                                    },
                                }
                            }
                        }
                    },
                    KtCardInfoModel = new KtCardInfoModel
                    {
                        Title = _localizer["Offers"]
                    },
                    KtDatatableModel = new KtDatatableModel
                    {
                        DatatableName = "Offers",
                        ReadUrl = Url.Action(nameof(Offers), nameof(OfferController).TrimController(), new { Area = AreaConstants.AreaName }),
                        ColumnFunctionName = "initOffersData",
                        DeleteAction = new KtActionModel
                        {
                            ActionUrl = Url.Action(nameof(Delete), nameof(OfferController).TrimController(), new { Area = AreaConstants.AreaName }),
                            IconClassName = "fa fa-ban",
                            Name = "Delete"
                        }
                    }
                }
            };

            return View(model);
        }

        public async Task<IActionResult> Offers(OfferFilterViewModel query)
        {
            var offerQuery = new OffersQuery()
            {
                Page = query.Page,
                PageSize = query.PageSize,
                SortBy = query.SortBy ?? "Id",
                SortOrder = query.SortOrder,
                Active = true
            };

            var result = await _queryExecutor.ExecutePagedQueryAsync<OffersQuery, OffersQueryResult>(offerQuery);

            query.Total = result.Data.TotalCount;

            return query.ToDataSourceResult(result.Data.Offers);
        }

        public async Task<IActionResult> AddEgt()
        {
            var model = new EgtViewModel();
            await SetSelectLists(model);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddEgt(EgtViewModel command)
        {
            var response = await _commandExecutor.ExecuteAsync(command.ToCommand());
            if (response.Success)
            {
                this.SetSuccessMessage("Offer add success");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("Offer add error");
            return this.JsonFail();
        }

        public async Task<IActionResult> AddPlayNGo()
        {
            var model = new PlayNGoViewModel();
            await SetSelectLists(model);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddPlayNGo(PlayNGoViewModel command)
        {
            var response = await _commandExecutor.ExecuteAsync(command.ToCommand());
            if (response.Success)
            {
                this.SetSuccessMessage("Offer add success");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("Offer add error");
            return this.JsonFail();
        }

        public async Task<IActionResult> AddBoom()
        {
            var model = new BoomViewModel();
            await SetSelectLists(model);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddBoom(BoomViewModel command)
        {
            var response = await _commandExecutor.ExecuteAsync(command.ToCommand());
            if (response.Success)
            {
                this.SetSuccessMessage("Offer add success");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("Offer add error");
            return this.JsonFail();
        }

        public async Task<IActionResult> AddSport()
        {
            var model = new SportViewModel();
            await SetSelectLists(model);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddSport(SportViewModel command)
        {
            var response = await _commandExecutor.ExecuteAsync(command.ToCommand());
            if (response.Success)
            {
                this.SetSuccessMessage("Offer add success");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("Offer add error");
            return this.JsonFail();
        }

        public async Task<IActionResult> AddTableGames()
        {
            var model = new TableGameViewModel();
            await SetSelectLists(model);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddTableGames(TableGameViewModel command)
        {
            var response = await _commandExecutor.ExecuteAsync(command.ToCommand());
            if (response.Success)
            {
                this.SetSuccessMessage("Offer add success");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("Offer add error");
            return this.JsonFail();
        }

        public async Task<IActionResult> AddPoker()
        {
            var model = new PokerViewModel();
            await SetSelectLists(model);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddPoker(PokerViewModel command)
        {
            var response = await _commandExecutor.ExecuteAsync(command.ToCommand());
            if (response.Success)
            {
                this.SetSuccessMessage("Offer add success");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("Offer add error");
            return this.JsonFail();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(DeleteOfferCommand command)
        {
            var response = await _commandExecutor.ExecuteAsync(command);

            if (response.Success)
            {
                this.SetSuccessMessage("Offer deleted");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("Offer delete error");
            return this.JsonFail();
        }

        private async Task SetSelectLists(OfferViewModel model)
        {
            var categoryList = await _queryExecutor.ExecuteAsync<CategoriesSelectListQuery, CategoriesSelectListQueryResult>(new CategoriesSelectListQuery());

            model.Categories = categoryList.Data.Categories.Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
            {
                Text = x.Name.Georgian,
                Value = x.Id.ToString()
            });

            if (model is SlotOfferViewModel)
            {
                var query = await _queryExecutor.ExecuteAsync<ProvidersSelectListQuery, ProvidersSelectListQueryResult>(new ProvidersSelectListQuery());
                (model as SlotOfferViewModel).Providers =
                    query.Data.Providers.Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                    {
                        Text = x.Name.Georgian,
                        Value = x.Id.ToString()
                    });
            }

            model.GameTypes = this.EnumToSelectList<GameType>();
        }
    }
}
