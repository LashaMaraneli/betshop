﻿using Betasuchus.Admin.Areas.ServiceManagement.Models.Categories;
using Betasuchus.Admin.Areas.ServiceManagement.Models.Offers;
using Betasuchus.Admin.Factories;
using Betasuchus.Admin.Infrastructure;
using Betasuchus.Admin.Models.Shared.KtCommon;
using Betasuchus.Admin.Resources;
using Betasuchus.Application.Commands.CategoryManagement;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Categories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wandio.Web.Mvc;

namespace Betasuchus.Admin.Areas.ServiceManagement.Controllers
{
    [Area(AreaConstants.AreaName)]
    [Authorize(Roles = "editor,admin")]
    public class CategoryController : Controller
    {
        private readonly CommandExecutor _commandExecutor;
        private readonly QueryExecutor _queryExecutor;
        private readonly IStringLocalizer<Translations> _localizer;

        public CategoryController(
            CommandExecutor commandExecutor,
            QueryExecutor queryExecutor,
            IStringLocalizer<Translations> localizer)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            var model = new CategoriesFilterViewModel
            {
                KtCardModel = new KtCardModel
                {
                    KtCardActionModel = new KtCardActionModel()
                    {
                        ActionItems = new List<KtActionModel>()
                        {
                            new KtActionModel
                            {
                                ActionUrl = Url.Action(nameof(Add), nameof(CategoryController).TrimController(), new{Area=AreaConstants.AreaName }),
                                Name = nameof(Add),
                                CustomClassName = "btn-primary",
                                IconClassName = "fa fa-plus"
                            }
                        }
                    },
                    KtCardInfoModel = new KtCardInfoModel
                    {
                        Title = _localizer["Categories"]
                    },
                    KtDatatableModel = new KtDatatableModel
                    {
                        DatatableName = "Categories",
                        ReadUrl = Url.Action(nameof(Categories), nameof(CategoryController).TrimController(), new { Area = AreaConstants.AreaName }),
                        ColumnFunctionName = "initCategoryDataColumns",
                        EditAction = new KtActionModel
                        {
                            ActionUrl = Url.Action(nameof(Edit), nameof(CategoryController).TrimController(), new { Area = AreaConstants.AreaName }),
                        },
                        DeleteAction = new KtActionModel
                        {
                            ActionUrl = Url.Action(nameof(Delete), nameof(CategoryController).TrimController(), new { @Area = AreaConstants.AreaName }),
                        }
                    }
                }
            };

            return View(model);
        }

        public async Task<IActionResult> Categories(CategoriesFilterViewModel query)
        {
            var categoriesQuery = new CategoriesQuery
            {
                Page = query.Page,
                PageSize = query.PageSize,
                SortBy = query.SortBy,
                SortOrder = query.SortOrder,
                Total = query.Total
            };

            var result = await _queryExecutor.ExecutePagedQueryAsync<CategoriesQuery, CategoriesQueryResult>(categoriesQuery);

            query.Total = result.Data.TotalCount;

            return query.ToDataSourceResult(result.Data.Categories);
        }

        public async Task<IActionResult> Add()
        {
            var result = await _queryExecutor.ExecuteAsync<CategoriesSelectListQuery, CategoriesSelectListQueryResult>(new CategoriesSelectListQuery());

            var model = new AddCategoryViewModel
            {
                Name = new Shared.MultilanguageString(),
                ParentCategories = result.Data.Categories?.Select(x =>
                    new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name.Georgian
                    }).Prepend(new SelectListItem())
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddCategoryViewModel command)
        {
            var result = await _commandExecutor.ExecuteAsync(command.ToCommand());
            if (result.Success)
            {
                this.SetSuccessMessage(_localizer["Category create success"]);

                return this.JsonSuccess();
            }

            this.SetErrorMessage(result.Error.Errors.JoinStrings());

            return RedirectToAction(nameof(Index), nameof(CategoryController).TrimController(), new { Area = AreaConstants.AreaName });
        }

        public async Task<IActionResult> Edit(int id)
        {
            var result = await _queryExecutor.ExecuteAsync<GetCategoryQuery, GetCategoryQueryResult>(new GetCategoryQuery { Id = id });

            var selectListQuery = await _queryExecutor.ExecuteAsync<CategoriesSelectListQuery, CategoriesSelectListQueryResult>(new CategoriesSelectListQuery());

            var model = new EditCategoryViewModel()
            {
                Name = result.Data.CategoryItem.Name,
                ParentCategoryId = result.Data.CategoryItem.ParentCategoryId,
                ParentCategories = selectListQuery.Data.Categories?.Select(x =>
                    new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name.Georgian
                    }).Prepend(new SelectListItem()),
                Id = result.Data.CategoryItem.Id
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditCategoryViewModel command)
        {
            var result = await _commandExecutor.ExecuteAsync(command.ToCommand());
            if (result.Success)
            {
                this.SetSuccessMessage(_localizer["Category edit success"]);

                return this.JsonSuccess();
            }

            this.SetErrorMessage(result.Error.Errors.JoinStrings());

            return RedirectToAction(nameof(Index), nameof(CategoryController).TrimController(), new { Area = AreaConstants.AreaName });
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DeleteCategoryCommand command)
        {
            var result = await _commandExecutor.ExecuteAsync(command);

            if (result.Success)
            {
                this.SetSuccessMessage(_localizer["Category delete success"]);

                return this.JsonSuccess();
            }

            this.SetErrorMessage(result.Error.Errors.JoinStrings());

            return RedirectToAction(nameof(Index), nameof(CategoryController).TrimController(), new { Area = AreaConstants.AreaName });
        }
    }
}
