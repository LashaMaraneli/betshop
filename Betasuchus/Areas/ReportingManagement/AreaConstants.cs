﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Admin.Areas.ReportingManagement
{
    public static class AreaConstants
    {
        public const string AreaName = "ReportingManagement";
    }
}
