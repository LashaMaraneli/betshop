﻿using Betasuchus.Admin.Models.Shared.KtCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Betasuchus.Admin.Areas.ReportingManagement.Models.Reports
{
    public class ReportFilterViewModel : SortedAndPagedListRequestBase
    {
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
