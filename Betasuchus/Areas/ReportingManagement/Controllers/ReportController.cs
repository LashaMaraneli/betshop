﻿using Betasuchus.Admin.Areas.ReportingManagement.Models.Reports;
using Betasuchus.Admin.Infrastructure;
using Betasuchus.Admin.Models.Shared.KtCommon;
using Betasuchus.Admin.Resources;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Reports;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Threading.Tasks;
using Wandio.Web.Mvc;

namespace Betasuchus.Admin.Areas.ReportingManagement.Controllers
{
    [Area(AreaConstants.AreaName)]
    public class ReportController : Controller
    {
        private readonly QueryExecutor _queryExecutor;
        private readonly IStringLocalizer<Translations> _localizer;

        public ReportController(QueryExecutor queryExecutor, IStringLocalizer<Translations> localizer)
        {
            _queryExecutor = queryExecutor;
            _localizer = localizer;
        }

        public IActionResult Index(DateTime? beginDate, DateTime? endDate)
        {
            var model = new ReportFilterViewModel
            {
                SortBy = "Id",
                BeginDate = beginDate ?? DateTime.Now.AddDays(-1),
                EndDate = endDate ?? DateTime.Now,
                KtCardModel = new KtCardModel
                {
                    KtCardActionModel = new KtCardActionModel(),
                    KtCardInfoModel = new KtCardInfoModel
                    {
                        Title = _localizer["Report"]
                    },
                    KtDatatableModel = new KtDatatableModel
                    {
                        DatatableName = "Report",
                        ReadUrl = Url.Action(nameof(Report), nameof(ReportController).TrimController(), new { Area = AreaConstants.AreaName }),
                        ColumnFunctionName = "initReportDataColumns"
                    }
                }
            };

            return View(model);
        }

        public async Task<IActionResult> Report(ReportFilterViewModel query)
        {
            var reportQuery = new OperationReportQuery
            {
                Page = query.Page,
                BeginDate = query.BeginDate,
                EndDate = query.EndDate,
                PageSize = query.PageSize,
                SortBy = query.SortBy ?? "Id",
                SortOrder = query.SortOrder,
                Total = query.Total
            };

            var result = await _queryExecutor.ExecutePagedQueryAsync<OperationReportQuery, OperationReportResultQuery>(reportQuery);

            query.Total = result.Data.TotalCount;

            return query.ToDataSourceResult(result.Data.Operations);
        }

        [HttpGet]
        public async Task<IActionResult> Export(ReportFilterViewModel query)
        {
            var reportQuery = new ExportOperationReportQuery
            {
                BeginDate = query.BeginDate,
                EndDate = query.EndDate
            };

            var result = await _queryExecutor.ExecuteAsync<ExportOperationReportQuery, ExportOperationReportQueryResult>(reportQuery);

            return await this.ToExcelFile(
                new ExportToExcelRequest<ExportOperationReportQueryResult.Operation>(
                    "Order report.xls",
                    result.Data.Operations));
        }
    }
}
