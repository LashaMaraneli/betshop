﻿using Betasuchus.Admin.Areas.UserManagement.Models.Users;
using Betasuchus.Admin.Infrastructure;
using Betasuchus.Admin.Models.Shared.KtCommon;
using Betasuchus.Admin.Resources;
using Betasuchus.Application.Commands.UserCommands;
using Betasuchus.Application.Infrastructure;
using Betasuchus.Application.Queries.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Threading.Tasks;
using Wandio.Web.Mvc;

namespace Betasuchus.Admin.Areas.UserManagement.Controllers
{
    [Area(AreaConstants.AreaName)]
    [Authorize(Roles = "admin")]
    public class UserController : Controller
    {
        private readonly QueryExecutor _queryExecutor;
        private readonly CommandExecutor _commandExecutor;
        private readonly IStringLocalizer<Translations> _localizer;

        public UserController(CommandExecutor commandExecutor, QueryExecutor queryExecutor, IStringLocalizer<Translations> localizer)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            var model = new UsersFilterViewModel
            {
                KtCardModel = new KtCardModel
                {
                    KtCardActionModel = new KtCardActionModel()
                    {
                        ActionItems = new List<KtActionModel>()
                        {
                            new KtActionModel
                            {
                                ActionUrl = Url.Action(nameof(Add), nameof(UserController).TrimController(), new{Area=AreaConstants.AreaName }),
                                Name = nameof(Add),
                                CustomClassName = "btn-primary",
                                IconClassName = "fa fa-plus"
                            }
                        }
                    },
                    KtCardInfoModel = new KtCardInfoModel
                    {
                        Title = _localizer["Users"]
                    },
                    KtDatatableModel = new KtDatatableModel
                    {
                        DatatableName = "Categories",
                        ReadUrl = Url.Action(nameof(Users), nameof(UserController).TrimController(), new { Area = AreaConstants.AreaName }),
                        ColumnFunctionName = "initUserDataColumns",
                        EditAction = new KtActionModel
                        {
                            ActionUrl = Url.Action(nameof(Edit), nameof(UserController).TrimController(), new { Area = AreaConstants.AreaName }),
                        },
                        DeleteAction = new KtActionModel
                        {
                            ActionUrl = Url.Action(nameof(Delete), nameof(UserController).TrimController(), new { @Area = AreaConstants.AreaName }),
                        }
                    }
                }
            };

            return View(model);
        }

        public async Task<IActionResult> Users(UsersFilterViewModel query)
        {
            var usersQuery = new UsersQuery()
            {
                Page = query.Page,
                PageSize = query.PageSize,
                SortBy = query.SortBy ?? "Id",
                SortOrder = query.SortOrder,
            };

            var result = await _queryExecutor.ExecutePagedQueryAsync<UsersQuery, UsersQueryResult>(usersQuery);

            query.Total = result.Data.TotalCount;

            return query.ToDataSourceResult(result.Data.Users);
        }

        public async Task<IActionResult> Add()
        {
            return View(new AddUserViewModel
            {
                Roles = this.ToSelectList(
                    (await _queryExecutor.ExecuteAsync<RolesQuery, RolesQueryResult>(
                        new RolesQuery())).Data.Roles,
                     t => t,
                     v => v)
            });
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddUserViewModel command)
        {
            var result = await _commandExecutor.ExecuteAsync(command.ToCommand());

            if (result.Success)
            {
                this.SetSuccessMessage("User create succeeded");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("User create failed");
            return this.JsonFail(result.Error);
        }

        public async Task<IActionResult> Edit(long id)
        {
            var result = await _queryExecutor.ExecuteAsync<UserQuery, UserQueryResult>(new UserQuery { Id = id });

            if (!result.Success)
            {
                return BadRequest(result.Error);
            }

            var model = new EditUserViewModel
            {
                Id = result.Data.Id,
                BirthDate = result.Data.BirthDate,
                Email = result.Data.Email,
                FirstName = result.Data.FirstName,
                LastName = result.Data.LastName,
                PhoneNumber = result.Data.PhoneNumber,
                Role = result.Data.Role,
                Roles = this.ToSelectList(
                    (await _queryExecutor.ExecuteAsync<RolesQuery, RolesQueryResult>(
                        new RolesQuery())).Data.Roles,
                     t => t,
                     v => v)
            };

            return View(model);
        }

        [HttpPost]

        public async Task<IActionResult> Edit(EditUserViewModel command)
        {
            var result = await _commandExecutor.ExecuteAsync(command.ToCommand());

            if (result.Success)
            {
                this.SetSuccessMessage("User update succeeded");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("User update failed");
            return this.JsonFail(result.Error);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DeleteUserCommand command)
        {
            var result = await _commandExecutor.ExecuteAsync(command);

            if (result.Success)
            {
                this.SetSuccessMessage("User delete succeeded");
                return this.JsonSuccess();
            }

            this.SetErrorMessage("User delete failed");
            return this.JsonFail(result.Error);
        }
    }
}
