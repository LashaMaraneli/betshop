﻿using Betasuchus.Admin.Infrastructure;
using Betasuchus.Application.Commands.UserCommands;
using Betasuchus.Application.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace Betasuchus.Admin.Areas.UserManagement.Models.Users
{
    public class EditUserViewModel : IViewModel
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime? BirthDate { get; set; }

        public string Role { get; set; }

        public IEnumerable<SelectListItem> Roles { get; set; }

        public Command ToCommand()
        {
            var command = new EditUserCommand()
            {
                BirthDate = BirthDate,
                Email = Email,
                FirstName = FirstName,
                Id = Id,
                LastName = LastName,
                PhoneNumber = PhoneNumber,
                Role = Role
            };

            return command;
        }
    }
}
