﻿namespace Betasuchus.Admin.Models.Shared.Menu
{
    public enum MenuPage
    {
        Home,
        Category,
        User,
        Report
    }
}
