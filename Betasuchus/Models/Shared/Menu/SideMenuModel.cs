﻿using System.Collections.Generic;

namespace Betasuchus.Admin.Models.Shared.Menu
{
    public enum SideMenuItemPosition
    {
        Top,
        Bottom
    }

    public class SideMenuModel
    {
        public IList<SideMenuItem> SideMenuItems { get; set; }
    }

    public class SideMenuItem
    {
        public SideMenuItemPosition SideMenuItemPosition { get; set; }

        public MenuPage MenuPage { get; set; }

        public MenuModel MenuModel { get; set; }
    }
}
