﻿using System.Collections.Generic;

namespace Betasuchus.Admin.Models.Shared.Menu
{
    public class TopMenuModel
    {
        public IList<MenuModel> TopMenuItems { get; set; }
    }
}
