﻿using System.Collections.Generic;

namespace Betasuchus.Admin.Models.Shared.Menu
{
    public class MenuModel
    {
        public MenuModel()
        {
            SubMenuModel = new List<MenuModel>();
        }

        public string Title { get; set; }

        public string RedirectUrl { get; set; }

        public string ClassName { get; set; }

        public bool Selected { get; set; }

        public string IconPath { get; set; }

        public string IconClassName { get; set; }

        public IList<MenuModel> SubMenuModel { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}
