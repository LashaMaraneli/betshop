﻿using Betasuchus.Admin.Infrastructure;
using Betasuchus.Shared;
using Microsoft.AspNetCore.Mvc;

namespace Betasuchus.Admin.Models.Shared.KtCommon
{
    public abstract class SortedAndPagedListRequestBase : ISortedAndPagedListRequestBase
    {
        [ModelBinder(BinderType = typeof(SortedAndPagedListRequestBaseModelBinder))]
        public int PageSize { get; set; }

        [ModelBinder(BinderType = typeof(SortedAndPagedListRequestBaseModelBinder))]
        public int Page { get; set; }

        public int Total { get; set; }

        [ModelBinder(BinderType = typeof(SortedAndPagedListRequestBaseModelBinder))]
        public string SortBy { get; set; }

        [ModelBinder(BinderType = typeof(SortedAndPagedListRequestBaseModelBinder))]
        public SortOrder SortOrder { get; set; }

        public KtCardModel KtCardModel { get; set; }
    }
}
