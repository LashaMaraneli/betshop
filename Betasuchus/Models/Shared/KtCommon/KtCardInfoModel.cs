﻿namespace Betasuchus.Admin.Models.Shared.KtCommon
{
    public class KtCardInfoModel
    {
        public string Title { get; set; }

        public string SubTitle { get; set; }
    }
}
