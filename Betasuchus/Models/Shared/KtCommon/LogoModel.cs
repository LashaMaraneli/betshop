﻿
namespace Betasuchus.Admin.Models.Shared.KtCommon
{
    public class LogoModel
    {
        public string CompanyName { get; set; }

        public bool IsMobileViewLogo { get; set; }
    }
}
