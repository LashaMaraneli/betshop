﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Betasuchus.Admin.Models.Shared.KtCommon
{
    public class KtDatatableResult
    {
        [JsonPropertyName("data")]
        public object Data { get; set; }

        [JsonPropertyName("meta")]
        public Meta Meta { get; set; }
    }

    public class Meta
    {
        private int _pageSize;

        [JsonPropertyName("page")]
        public int Page { get; set; }

        [JsonPropertyName("perpage")]
        public int PageSize { get; set; }

        [JsonPropertyName("total")]
        [Display(Name = "total")]
        public int Total { get; set; }

        [JsonPropertyName("sort")]
        public string SortOrder { get; set; }

        [JsonPropertyName("field")]
        public string SortBy { get; set; }

        [JsonPropertyName("pages")]
        public int Pages
        {
            get => this._pageSize;
            private set { this._pageSize = (Total + PageSize - 1) / PageSize; }
        }
    }
}
