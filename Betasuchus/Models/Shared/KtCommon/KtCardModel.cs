﻿

namespace Betasuchus.Admin.Models.Shared.KtCommon
{
    public class KtCardModel
    {
        public KtCardInfoModel KtCardInfoModel { get; set; }

        public KtCardActionModel KtCardActionModel { get; set; }

        public KtDatatableModel KtDatatableModel { get; set; }

        public KtComponentModel KtComponentModel { get; set; }
    }
}
