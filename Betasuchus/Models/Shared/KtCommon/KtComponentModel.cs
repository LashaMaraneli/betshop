﻿namespace Betasuchus.Admin.Models.Shared.KtCommon
{
    public class KtComponentModel
    {
        public string ComponentName { get; set; }

        public object Data { get; set; }
    }
}
