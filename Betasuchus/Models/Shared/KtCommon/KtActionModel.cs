﻿using System.Collections.Generic;

namespace Betasuchus.Admin.Models.Shared.KtCommon
{
    public class KtActionModel
    {
        public string Name { get; set; }

        public string ActionUrl { get; set; }

        public string CustomClassName { get; set; }

        public string IconClassName { get; set; }

        public string FunctionName { get; set; }

        public bool Hide { get; set; }

        public bool IsCustomAction { get; set; }

        public IList<KtActionModel> SubActions { get; set; }
    }
}
