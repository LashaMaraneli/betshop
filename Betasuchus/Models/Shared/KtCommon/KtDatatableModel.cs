﻿using System.Collections.Generic;

namespace Betasuchus.Admin.Models.Shared.KtCommon
{
    public class KtDatatableModel
    {
        // required
        public string DatatableName { get; set; }

        // required
        public string ReadUrl { get; set; }

        // required
        public string ColumnFunctionName { get; set; }

        public string QueryParamsFunctionName { get; set; }

        public int PageSize { get; set; } = 25;

        public int[] PageSizes { get; set; } = new int[] { 10, 25, 50, 100 };

        public bool Scrollable { get; set; } = true;

        public KtActionModel EditAction { get; set; }

        public KtActionModel DeleteAction { get; set; }

        public IList<KtActionModel> AdditionalActions { get; set; }

        public KtDatatableModel ChildTable { get; set; }
    }
}
