﻿using System.Collections.Generic;

namespace Betasuchus.Admin.Models.Shared.KtCommon
{
    public class KtCardActionModel
    {
        public IList<KtActionModel> ActionItems { get; set; }
    }
}
